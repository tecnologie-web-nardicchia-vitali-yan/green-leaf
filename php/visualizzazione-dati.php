<?php
require_once 'bootstrap.php';



if(isUserLoggedIn()){
    $templateParams["titolo"] = "I miei dati - Admin";
    $templateParams["nome"] = "visualizzazione-dati.php";
    $templateParams["stile"] = "style_gestisci_dati.css";
    $templateParams["datispedizione"] = $dbh->getDatiSpedizioneByUserEmail($_SESSION["email"]);
    $templateParams["datipagamento"] = $dbh->getDatiPagamentoByUserEmail($_SESSION["email"]);
    $templateParams["datipersonali"] = $dbh->getDatiPersonaliByUserEmail($_SESSION["email"]);
    
    if(isset($_GET["formmsg"])){
        $templateParams["formmsg"] = $_GET["formmsg"];
    }

}else{
    header("location: login.php");
}


require 'template/base.php';
?>