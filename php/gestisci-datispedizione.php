<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn() || !isset($_GET["action"]) || ($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3 && $_GET["action"]!=5)){
    header("location: login.php");
}


if ($_GET["action"] != 1 && $_GET["action"] != 5) {
    $risultato = $dbh->getDatiSpedizioneByCodiceandEmail($_GET["codice"], $_SESSION["email"]);

    if (count($risultato) == 0) {
        $templateParams["spedizione"] = null;
    } else {
        $templateParams["spedizione"] = $risultato[0];
    }
}else{
    $templateParams["spedizione"] = getEmptyDatiSpedizione();
}


$templateParams["titolo"] = "Green Leaf - Gestisci dati spedizione";
$templateParams["nome"] = "form-datispedizione.php";

$templateParams["js"] = "controllo_formspedizione.js";


$templateParams["stile"] = "style_gestisci_dati.css";

$templateParams["azione"] = $_GET["action"];

require 'template/base.php';
?>