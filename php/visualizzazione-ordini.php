<?php
require_once 'bootstrap.php';



if(isUserLoggedIn()){
    $templateParams["titolo"] = "I miei ordini - Admin";
    $templateParams["nome"] = "visualizzazione-ordini.php";
    $templateParams["stile"] = "style_gestisci_dati.css";
    $templateParams["js"] = "storico_ordini.js";
    $templateParams["ordini"] = $dbh->getOrdiniPagatiByUserEmail($_SESSION["email"]);
    
    if(isset($_GET["formmsg"])){
        $templateParams["formmsg"] = $_GET["formmsg"];
    }

}else{
    header("location: login.php");
}


require 'template/base.php';
?>