<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn() || $dbh->isUserRole($_SESSION["email"]) || !isset($_GET["action"]) || ($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3)){
    header("location: login.php");
}

if ($_GET["action"] != 1) {
    $risultato = $dbh->getTreeByName($_GET["nome_albero"]);

    if (count($risultato) == 0) {
        $templateParams["albero"] = null;
    } else {
        $templateParams["albero"] = $risultato[0];
        $templateParams["immagini"] = $dbh->getTreeImgByName($_GET["nome_albero"]);
        
        
    }
} else {
    $templateParams["albero"] = getEmptyAlbero();
}



$templateParams["titolo"] = "Green Leaf - Gestisci alberi";
$templateParams["nome"] = "form-albero.php";

$templateParams["js"] = "controllo_formalbero.js";

$templateParams["stile"] = "style_gestisci_dati.css";

$templateParams["azione"] = $_GET["action"];

require 'template/base.php';
?>