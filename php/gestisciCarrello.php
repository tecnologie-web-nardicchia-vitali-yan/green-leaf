<?php
require_once 'bootstrap.php';

if (!isset($_GET["action"]) || ($_GET["action"] != 1 && $_GET["action"] != 2) || ($_GET["action"] != 1 && !isset($_GET["id"]) && !isset($_GET["order"]))) {
    header("location: carrello.php");
}

if (isset($_GET["id"]) && isset($_GET["order"])) {

    $orderId = $_GET["order"];
    $codice = $_GET["id"];

    if (isset($_POST["update"])) {
        $treeName = $_POST["nome"];
        $quantity = $_POST["quantità"];
        $isGift = isset($_POST["regala"]) ? 1 : 0;
        $isToPlant = isset($_POST["piantare"]) ? 1 : 0;
        $shippingCode = isset($_POST["indirizzo"]) ? $_POST["indirizzo"] : null;
        $email = isset($_POST["email"]) ? $_POST["email"] : null; 
        $presente = 0;

        if (!isUserLoggedIn()) {

            //controllo se è già presente l'articolo
            if(isset($_COOKIE["dettaglioordine"])) {
                foreach ($_COOKIE["dettaglioordine"] as $key => $value) {
                    $albero = unserialize($value);
                    if ($codice != $key && $albero["nome_albero"] == $treeName && $albero["regalo"] == $isGift && $albero["piantare"]==$isToPlant && 
                    $albero["codice_spedizione"] == $shippingCode && $email == $albero["email_regalo"]) {
                        $quantity = $albero["quantità"] + $quantity; 
                        $shippingCode = $albero["codice_spedizione"];
                        deleteOrderDetails($codice);
                        $codice = $key;
                        $presente = 1;
                    }
                }
            }

            updateOrderDetails($codice, $treeName, $quantity, $isGift, $isToPlant, $shippingCode, $email);
        } else {

            $oldOrderDetails = $dbh->checkIsPresentOrderDetails($treeName, $orderId, $isToPlant, $isGift, $email);
            if (isset($oldOrderDetails) && count($oldOrderDetails) >= 1 && $codice != $oldOrderDetails[0]["codice"] && $email == $oldOrderDetails[0]["email_regalo"]) {

                $quantità = $oldOrderDetails[0]["quantità"] + $quantity; 
                $dbh->updateOrderDetails($oldOrderDetails[0]["codice"], $oldOrderDetails[0]["nome_albero"], 
                                         $quantità, $oldOrderDetails[0]["regalo"], $oldOrderDetails[0]["piantare"], 
                                         $oldOrderDetails[0]["codice_spedizione"], $oldOrderDetails[0]["email_regalo"]);
                $dbh->deleteOrderDetails($codice);
            } else {
                $dbh->updateOrderDetails($codice, $treeName, $quantity, $isGift, $isToPlant, $shippingCode, $email);
            }
        }
        header("location: carrello.php");
    }

    //modifica
    if ($_GET["action"] == 1) {

        $templateParams["titolo"] = "Green Leaf - Modifica Albero";
        $templateParams["nome"] = "gestisciCarrello.php";
        $templateParams["stile"] = "style_carrello.css";
        $templateParams["js"] = "gestisciCarrello.js";

        if (isUserLoggedIn()) {
            $templateParams["dettaglioordine"] = $dbh->getOrderDetailsByCode($codice)[0];

            if (isset($templateParams["dettaglioordine"][0]["codice_spedizione"])) {
                $codiceordine = $dettaglioordine["codice_spedizione"];
                $templateParams["indirizzospedizione"] = $dbh->getShippingAddressByCode($codiceordine);
            }
        } else {

            if (isset($_COOKIE["dettaglioordine"])) {

                foreach ($_COOKIE["dettaglioordine"] as $key => $value) {
                    $dettaglioordine[$key] = unserialize($value);
                }

                $templateParams["dettaglioordine"] = $dettaglioordine[$codice];
            }
        }
    }
}
$templateParams["azione"] = $_GET["action"];

require 'template/base.php';
?>