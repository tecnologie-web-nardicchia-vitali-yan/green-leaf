<?php
require_once 'bootstrap.php';



if(isUserLoggedIn()){
    $templateParams["titolo"] = "Le mie piante - Admin";
    $templateParams["nome"] = "visualizzazione-lemiepiante.php";
    $templateParams["stile"] = "style_gestisci_dati.css";
    $templateParams["alberi_piantati"] = $dbh->getAlberiPiantatiNonRegalatiByUserEmail($_SESSION["email"]);
    $templateParams["alberi_regalati"] = $dbh->getAlberiPiantatiRegalatiByUserEmail($_SESSION["email"]);
    
}else{  
    header("location: login.php");
}


require 'template/base.php';
?>