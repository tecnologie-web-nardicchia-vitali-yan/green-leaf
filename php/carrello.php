<?php
require_once 'bootstrap.php';

if (isset($_GET["id"])) {
    $codice = $_GET["id"];


    //eliminazione
    if (isset($_GET["action"]) && $_GET["action"] == 1) {
        if (isUserLoggedIn()) {
            $dbh->deleteOrderDetails($codice);
        } else {
            deleteOrderDetails($codice);
            header("location: carrello.php");
        }
    }
}

if (isUserLoggedIn()) {
    $templateParams["ordine"] = $dbh->getNotPairOrderByUser($_SESSION["email"]);

    if (count($templateParams["ordine"]) == 0) {
        //se carrello vuoto, aggiunge nuovo ordine
        $templateParams["orderId"] = $dbh->addNewOrder(0, 0, floatval(0), $_SESSION["email"], null);
        $templateParams["ordine"] = $dbh->getOrderById($templateParams["orderId"]);
    } else {
        $templateParams["orderId"] = $templateParams["ordine"][0]["numero"];
    }

    //se appena loggato
    if (isset($_GET["action"]) && isset($_COOKIE["dettaglioordine"])) {
        foreach ($_COOKIE["dettaglioordine"] as $key => $value) {
            $albero = unserialize($value);
            $nomeAlbero = $albero["nome_albero"];
            $quantità = $albero["quantità"];
            $regalo = $albero["regalo"];
            $piantare = $albero["piantare"];
            $codicespedizione = $albero["codice_spedizione"];
            $email = $albero["email_regalo"];

            //controllo se ordine già presente, se si aumenta quantità di 1
            $oldOrderDetails = $dbh->checkIsPresentOrderDetails($nomeAlbero, $templateParams["orderId"], $piantare, $regalo, $email);
            if (isset($oldOrderDetails) && count($oldOrderDetails) == 1) {
                $quantità = $oldOrderDetails[0]["quantità"] + $quantità;
                $code = $oldOrderDetails[0]["codice"];
                $dbh->updateOrderDetails(
                    $code,
                    $nomeAlbero,
                    $quantità,
                    $oldOrderDetails[0]["regalo"],
                    $oldOrderDetails[0]["piantare"],
                    $oldOrderDetails[0]["codice_spedizione"],
                    $oldOrderDetails[0]["email_regalo"] 
                );
            } else {
                $code = $dbh->addOrderDetails($nomeAlbero, $quantità, $regalo, $piantare, $templateParams["orderId"], $codicespedizione, $email);
            }
            deleteOrderDetails($key);
        }
        if ($_GET["action"] == 1) {
            header("location: carrello.php?email=".$email);
        } else {
            header("location: index.php");
        }
    }

    //prendo tutti i dettagli ordine
    if (count($templateParams["ordine"]) == 1) {
        $templateParams["dettaglioordine"] = $dbh->getOrderDetailsByOrderNumber($templateParams["ordine"][0]["numero"]);

        foreach ($templateParams["dettaglioordine"] as $dettaglioordine) {
            if (isset($dettaglioordine["codice_spedizione"])) {
                $codiceordine = $dettaglioordine["codice_spedizione"];
                $templateParams["indirizzospedizione"][$codiceordine] = $dbh->getShippingAddressByCode($codiceordine);
            }
        }
        $templateParams["action"] = "id=" . $templateParams["ordine"][0]["numero"];
    }
} else {
    $templateParams["orderId"] = -1;
    //se non è loggato
    if (isset($_COOKIE["dettaglioordine"])) {
        foreach ($_COOKIE["dettaglioordine"] as $key => $value) {
            $dettaglioordine[$key] = unserialize($value);
        }
        $templateParams["dettaglioordine"] = $dettaglioordine;
    }
    $templateParams["action"] = "action=1";
}


$templateParams["titolo"] = "Green Leaf - Carrello";
$templateParams["nome"] = "carrello.php";
$templateParams["stile"] = "style_carrello.css";


require 'template/base.php';

?>
