<?php
require_once 'bootstrap.php';

if (isset($_POST["add"])) {
    $name_place = $_POST["name_place"];
    $src = $_POST["src"];
    $dbh->addPlace($name_place, $src);
    header("location: gestisci-ordini.php");
}

if(!isUserLoggedIn()){
    header("location: login.php");
} else{
    $templateParams["titolo"] = "Green Leaf - Aggiungi posizione";
    $templateParams["nome"] = "form-posizione.php";
    $templateParams["stile"] = "style_gestisci_dati.css";
}

require 'template/base.php';
?>