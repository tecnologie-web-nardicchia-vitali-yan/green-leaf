<?php
require_once 'bootstrap.php';

$nomeAlbero = -1;
if(isset($_GET["nome"])){
    $nomeAlbero = $_GET["nome"];
}

$templateParams["albero"] = $dbh->getTreeByName($nomeAlbero);
$templateParams["immagini"] = $dbh->getTreeImgByName($nomeAlbero);

if(isUserLoggedIn()) {
    $templateParams["ordine"] = $dbh->getNotPairOrderByUser($_SESSION["email"]);

    if (count($templateParams["ordine"]) == 0) {
        $orderId =$dbh->addNewOrder(0, 0, floatval(0), $_SESSION["email"], null);
        $templateParams["ordine"] = $dbh->getOrderById($orderId);
    } else {
        $orderId=$templateParams["ordine"][0]["numero"];
    }


    if(isset($_POST["submit"])) {
        $oldOrderDetails = $dbh->checkIsPresentOrderDetails($nomeAlbero, $orderId, 0, 0, null);
        if (isset($oldOrderDetails) && count($oldOrderDetails) >= 1) {
            $presente = 1;
            $quantità = $oldOrderDetails[0]["quantità"]+1;
            $result= $dbh->updateOrderDetails($oldOrderDetails[0]["codice"], $nomeAlbero, 
                                     $quantità, $oldOrderDetails[0]["regalo"], 
                                     $oldOrderDetails[0]["piantare"], $oldOrderDetails[0]["codice_spedizione"], $oldOrderDetails["email"]);
        } else {
            $presente =0;
            $result=$dbh->addOrderDetails($nomeAlbero, 1, 0, 0, $orderId, null, null);
        }
        $templateParams["msg"] = "Aggiunto al carrello!";
    }

} else {

    if(isset($_POST["submit"])) {
        $quantità = 0;
        $regalo = 0;
        $piantare = 0;
        $codicespedizione = null; 
        $email = NULL;
        $presente = 0;

        if(isset($_COOKIE["dettaglioordine"])) {
            foreach ($_COOKIE["dettaglioordine"] as $key => $value) {
                $albero = unserialize($value);
                if ($albero["nome_albero"] == $nomeAlbero && $albero["regalo"] == 0 && $albero["piantare"]==0) {
                    $codice = $key;
                    $quantità = $albero["quantità"]; 
                    $codicespedizione = $albero["codice_spedizione"];
                    $presente = 1;
                    $email = $albero["email_regalo"];
                }
            }
        } 
        if ($presente) {
            updateOrderDetails($codice, $nomeAlbero, $quantità + 1, $regalo, $piantare, $codicespedizione, $email);
        } else {
            registerOrderDetails($nomeAlbero, $quantità + 1, $regalo, $piantare, -1, $codicespedizione, $templateParams["albero"][0]["costo"], $templateParams["immagini"][0]["nome_immagine"], $email);
        }
        $templateParams["msg"] = "Aggiunto al carrello!";
    }
}

$templateParams["titolo"] = "Green Leaf - Albero";
$templateParams["nome"] = "alberoDescrizione.php";
$templateParams["stile"] = "style_descrizioneAlbero.css";

require 'template/base.php';

?>
