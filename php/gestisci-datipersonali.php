<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn()){
    header("location: login.php");
}

$templateParams["titolo"] = "Green Leaf - Gestisci dati personali";
$templateParams["nome"] = "form-datipersonali.php";
$templateParams["js"] = "controllo_formdatipersonali.js";
$templateParams["stile"] = "style_gestisci_dati.css";

$templateParams["datipersonali"] = $dbh->getDatiPersonaliByUserEmail($_SESSION["email"]);



require 'template/base.php';
?>