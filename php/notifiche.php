<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn()) {
    header("location: login.php");
} else {
    $templateParams["titolo"] = "GreenLeaf - Notifiche";
    $templateParams["nome"] = "notifiche.php";
    $templateParams["notifiche"] = $dbh->getUserNotifications($_SESSION["email"]);
    $templateParams["stile"] = "notifiche.css";
    $templateParams["js"] = "notifiche.js";
}

require 'template/base.php';
?>