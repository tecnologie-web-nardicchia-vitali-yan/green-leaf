<?php
require_once 'bootstrap.php';

if(!isUserLoggedIn() || !isset($_GET["action"]) || ($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3 && $_GET["action"]!=5)){
    header("location: login.php");
}


if ($_GET["action"] != 1 && $_GET["action"] != 5) {
    $risultato = $dbh->getDatiPagamentoByNumeroAndEmail($_GET["numero"], $_SESSION["email"]);

    if (count($risultato) == 0) {
        $templateParams["pagamento"] = null;
    } else {
        $templateParams["pagamento"] = $risultato[0];
    }
}else{
    $templateParams["pagamento"] = getEmptyDatiPagamento();
}


$templateParams["titolo"] = "Green Leaf - Gestisci dati pagamento";
$templateParams["nome"] = "form-datipagamento.php";

$templateParams["js"] = "controllo_formpagamento.js";


$templateParams["stile"] = "style_gestisci_dati.css";

$templateParams["azione"] = $_GET["action"];

require 'template/base.php';
?>