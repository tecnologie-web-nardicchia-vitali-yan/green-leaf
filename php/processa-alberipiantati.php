<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !isset($_GET["action"])) {
    header("location: login.php");
}


if ($_GET["action"] == 3) {
    //cancello
    $codice = $_GET["codice"];

    $dettagli_crescita = $dbh->getDettaglioCrescitaAlbero($codice);

    if ($dettagli_crescita != null) {
        foreach (array_slice($dettagli_crescita, 0) as $dettaglio){
            $codice = $dbh->deleteImmagineCrescita($dettaglio["codice"]);
            if(!$codice){
                $msg = "Errore cancellazione immagine di crescita!";
                header("location: visualizzazione-venditorealberipiantati.php?formmsg=" . $msg);
            }
        }

        foreach (array_slice($dettagli_crescita, 0) as $dettaglio){
            $codice = $dbh->deleteDettaglioCrescita($dettaglio["codice"]);
            if(!$codice){
                $msg = "Errore cancellazione dettaglio crescita!";
                header("location: visualizzazione-venditorealberipiantati.php?formmsg=" . $msg);
            }
        }

    }


    $codice = $dbh->deleteAlberoPiantato($codice);

    if ($codice) {
        $msg = "Cancellazione completata correttamente!";
    } else {
        $msg = "Errore nella cancellazione!";
    }

    header("location: visualizzazione-venditorealberipiantati.php?formmsg=" . $msg);
}

?>