<?php
if (count($templateParams["ordini"]) == 0) :
?>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3 text-center">Ordini</h2>
                <h3 class="text-center text-danger">Non hai ancora fatto ordini</h3>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php else : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Ordini</h2>
                <table class="mt-4 table table-hover">
                    <tr class="bg-white">
                        <th>Numero:</th>
                        <th>Spedito:</th>
                        <th>Prezzo totale:</th>
                    </tr>
                    <?php foreach ($templateParams["ordini"] as $ordine) : ?>
                        <tr>
                            <td><?php echo $ordine["numero"] ?></td>
                            <td><?php if ($ordine["spedito"]) {
                                    echo "Si";
                                } else {
                                    echo "No";
                                } ?></td>
                            <td><?php echo $ordine["prezzo_totale"] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif;
