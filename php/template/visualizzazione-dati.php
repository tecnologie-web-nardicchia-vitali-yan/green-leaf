<?php if (isset($templateParams["formmsg"])) : ?>
    <div class="row mt-5 mx-0">
        <div class="col-12 text-center">
            <p><?php echo $templateParams["formmsg"]; ?></p>
        </div>
    </div>
<?php endif; 
$utente = $templateParams["datipersonali"][0];
?>
<div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Dati personali</h2>
                    <table class="table table-borderless">
                        <tr>
                            <th class="row">Nome:</th>
                            <td><?php echo $utente["nome"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Cognome:</th>
                            <td><?php echo $utente["cognome"]?></td>
                        </tr>
                        <tr>
                            <th class="row">Telefono:</th>
                            <td><?php echo $utente["telefono"] ?></td>
                        </tr>
                    </table>
                    <ul class="mt-2 nav">
                        <li class="col-6"></li>
                        <li class="col-6"><a class="nav-link text-center rounded-pill" href="gestisci-datipersonali.php">Modifica</a></li>
                    </ul>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php
if ($templateParams["datispedizione"] == null) :
?>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3 text-center">Dati spedizione</h2>
                <a class="mt-2 nav-link text-center mx-auto rounded-pill text-center" href="gestisci-datispedizione.php?action=1">Inserisci dati Spedizione</a>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php else : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Dati spedizione</h2>
                <a class="mt-2 nav-link text-center rounded-pill" href="gestisci-datispedizione.php?action=1">Inserisci dati Spedizione</a>
                <?php foreach ($templateParams["datispedizione"] as $spedizione) : ?>
                    <table class="table table-borderless">
                        <tr>
                            <th class="row">Via:</th>
                            <td><?php echo $spedizione["via"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Numero:</th>
                            <td><?php echo $spedizione["numero"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Città:</th>
                            <td><?php echo $spedizione["città"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Provincia:</th>
                            <td><?php echo $spedizione["provincia"] ?></td>
                        </tr>
                        <tr>
                            <th class="row"> CAP:</th>
                            <td><?php echo $spedizione["CAP"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Nome destinatario:</th>
                            <td><?php echo $spedizione["nome_destinatario"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Cognome destinatario:</th>
                            <td><?php echo $spedizione["cognome_destinatario"] ?></td>
                        </tr>
                    </table>
                    <ul class="mt-2 nav">
                        <li class="col-6"><a class="nav-link text-center rounded-pill" href="gestisci-datispedizione.php?action=2&codice=<?php echo $spedizione["codice"] ?>">Modifica</a></li>
                        <li class="col-6"><a class="nav-link text-center rounded-pill" href="gestisci-datispedizione.php?action=3&codice=<?php echo $spedizione["codice"] ?>">Cancella</a></li>
                    </ul>
                <?php endforeach; ?>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif ?>
<?php
if ($templateParams["datipagamento"] == null) :
?>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3 text-center">Dati Pagamento</h2>
                <a class="mt-2 nav-link mx-auto text-center rounded-pill text-center" href="gestisci-datipagamento.php?action=1">Inserisci dati Pagamento</a>
            </section>
            <div class="col-1"></div>
        </div>
    </div> 
<?php else : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Modalità di pagamento</h2>
                <a class="mt-2 nav-link text-center rounded-pill" href="gestisci-datipagamento.php?action=1">Inserisci modalità di pagamento</a>
                <?php foreach ($templateParams["datipagamento"] as $pagamento) : ?>
                    <table class="table table-borderless">
                        <tr>
                            <th class="row">Numero Carta:</th>
                            <td><?php echo $pagamento["numero"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Numero:</th>
                            <td><?php echo $pagamento["tipo_carta"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Nome titolare:</th>
                            <td><?php echo $pagamento["nome_titolare"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Cognome titolare:</th>
                            <td><?php echo $pagamento["cognome_titolare"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Data scadenza:</th>
                            <td><?php echo $pagamento["data_scadenza"] ?></td>
                        </tr>
                    </table>
                    <ul class="mt-2 nav">
                        <li class="col-6"><a class="nav-link text-center rounded-pill" href="gestisci-datiPagamento.php?action=2&numero=<?php echo $pagamento["numero"] ?>">Modifica</a></li>
                        <li class="col-6"><a class="nav-link text-center rounded-pill" href="gestisci-datiPagamento.php?action=3&numero=<?php echo $pagamento["numero"] ?>">Cancella</a></li>
                    </ul>
                <?php endforeach; ?>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif; ?>