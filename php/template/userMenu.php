<ul class="dropdown-menu p-2" aria-labelledby="dropdownMenu">
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="visualizzazione-dati.php">I miei dati</a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "utente.png"; ?>" alt="" />
    </li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="visualizzazione-ordini.php">I miei ordini</a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "albero2.png"; ?>" alt="" />
    </li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="visualizzazione-lemiepiante.php">Le mie piante</a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "pianta.png"; ?>" alt="" />
    </li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="notifiche.php">Notifiche<span class="ml-2 badge"><?php echo $notifiche; ?></span></a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "notifiche.png"; ?>" alt="" />
    </li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="carrello.php">Carrello</a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "carrello.png"; ?>" alt="" />
    </li>
    <li class="dropdown-divider"></li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-12 col-md-12 col-lg-12" href="logout.php">Logout</a>
    </li>
</ul>