<div class="container-fluid text-left">
    <form action="#" class="p-5" method="POST">
        <fieldset>
            <legend class="text-left my-5">Aggiungi Luogo</legend>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nome luogo:</label>
                <div class="col-sm-10 col-md-6 col-lg-5">
                    <input type="text" class="form-control" id="name_place" name="name_place" placeholder="Nome luogo" required />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">src:</label>
                <div class="col-sm-10 col-md-6 col-lg-5">
                    <input type="text" class="form-control" id="src" name="src" placeholder="Inserire l'src" required />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-12 col-lg-12 text-center">
                    <input type="submit" name="add" class="btn rounded-pill" value="Aggiungi" />
                </div>
            </div>
        </fieldset>
    </form>
</div>