<?php
if (isset($templateParams["nome"])) : ?>
    <h2 class="text-center mb-5">Riepilogo</h2>
<?php endif; ?>

<div class="container-fluid">
    <div class="row d-flex align-items-center">
        <div class="col-1 col-lg-2"></div>
        <div class="col-10 col-lg-8">
            <nav>
                <ul class="list-group list-group-horizontal mb-5 text-center nav">
                    <li class="list-group-item flex-fill nav-item">
                        <a class="nav-link" href="carrello.php">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart3" viewBox="0 0 16 16">
                                <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                            </svg>
                            Carrello
                        </a>
                    </li>
                    <li class="list-group-item flex-fill active nav-item" aria-current="true">
                        <a class="nav-link" href="riepilogoOrdine.php?id=<?php echo $codice; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                                <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
                            </svg>
                            Dati di spedizione
                        </a>
                    </li>
                    <li class="list-group-item flex-fill nav-item">
                        <a class="nav-link disabled" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-credit-card" viewBox="0 0 16 16">
                                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v1h14V4a1 1 0 0 0-1-1H2zm13 4H1v5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V7z" />
                                <path d="M2 10a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1z" />
                            </svg>
                            Pagamento
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-1 col-lg-2"></div>
        <form action="#" method="POST" class="col-12">
            <div class="row">

                <?php if (isset($templateParams["ordininormali"]) && count($templateParams["ordininormali"]) >= 1) : ?>
                    <div class="col-lg-2"></div>
                    <article class="col-12 col-lg-8">

                        <header>
                            <h3>Alberi scelti</h3>
                        </header>
                        <section>
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Albero</th>
                                            <th scope="col">Spedisci a</th>
                                            <th scope="col">Totale</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($templateParams["ordininormali"] as $ordinedettaglio) : ?>
                                            <tr>
                                                <td><?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></td>
                                                <td>
                                                    <label for="indirizzo_ordinenormale<?php echo $ordinedettaglio["codice"] ?>">Indirizzo di consegna:</label>
                                                    <select class="custom-select mr-sm-2" id="indirizzo_ordinenormale<?php echo $ordinedettaglio["codice"] ?>" name="indirizzo_ordinenormale<?php echo $ordinedettaglio["codice"] ?>" required>
                                                        <?php if (isset($ordinedettaglio["codice_spedizione"])) :
                                                            $codice_spedizione = $ordinedettaglio["codice_spedizione"];
                                                        else :
                                                            $codice_spedizione  = -1; ?>
                                                            <option value="" disabled selected>Scegli l'indirizzo di spedizione</option>
                                                        <?php endif; ?>
                                                        <?php foreach ($templateParams["datispedizione"] as $spedizione) : ?>
                                                            <option value="<?php echo $spedizione["codice"] ?>" <?php if ($codice_spedizione  == $spedizione["codice"]) echo "selected"; ?>>
                                                                <?php echo $spedizione["nome_destinatario"] . " " . $spedizione["cognome_destinatario"] .
                                                                    ", Via " . $spedizione["via"] . " " . $spedizione["numero"] . ", " . $spedizione["CAP"] . " " . $spedizione["città"]; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                        <option value="-1">Aggiungi un nuovo indirizzo di spedizione...</option>
                                                    </select>
                                                    <?php if ($ordinedettaglio["quantità"] > 1) : ?>
                                                        <a href="riepilogoOrdine.php?id=<?php echo $templateParams["ordine"][0]["numero"]; ?>&codice=<?php echo $ordinedettaglio["codice"] ?>&action=2">Spedisci a più indirizzi</a>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                                            </tr>

                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </section>

                        <footer>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-square" viewBox="0 0 16 16">
                                <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                            </svg>
                            <p>Puoi inviare l'albero che ordini a diversi indirizzi di spedizione selezionando il nome del destinatario e
                                l'indirizzo di spedizione per ciascun albero oppure aggiungere un nuovo indirizzo.
                            </p>
                        </footer>
                    </article>
                    <div class="col-lg-2"></div>
                <?php endif; ?>
            </div>
            <div class="row">
                <?php if (isset($templateParams["ordiniregala"]) && count($templateParams["ordiniregala"]) >= 1) : ?>
                    <div class="col-lg-2"></div>
                    <article class="col-12 col-lg-8 mb-3 mt-3">
                        <header>
                            <h3>Alberi scelti come regalo</h3>
                        </header>
                        <section>
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Albero</th>
                                            <th scope="col">Spedisci a </th>
                                            <th scope="col">Totale</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($templateParams["ordiniregala"] as $ordinedettaglio) :
                                            $codice = isset($ordinedettaglio["codice_spedizione"]) ? $ordinedettaglio["codice_spedizione"] : 0; ?>

                                            <tr>
                                                <td><?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></td>
                                                <td>
                                                    <label for="indirizzo_ordinenormale<?php echo $ordinedettaglio["codice"] ?>">Indirizzo di consegna:</label>
                                                    <select class="custom-select mr-sm-2" name="indirizzo_ordineregala<?php echo $ordinedettaglio["codice"] ?>" required>
                                                        <?php if (isset($ordinedettaglio["codice_spedizione"])) :
                                                            $codice_spedizione = $ordinedettaglio["codice_spedizione"];
                                                        else :
                                                            $codice_spedizione  = -1; ?>
                                                            <option value="" disabled selected>Scegli l'indirizzo di spedizione</option>
                                                        <?php endif; ?>
                                                        <?php foreach ($templateParams["datispedizione"] as $spedizione) : ?>
                                                            <option value="<?php echo $spedizione["codice"] ?>" <?php if ($codice_spedizione  == $spedizione["codice"]) echo "selected"; ?>>
                                                                <?php echo $spedizione["nome_destinatario"] . " " . $spedizione["cognome_destinatario"] .
                                                                    ", Via " . $spedizione["via"] . " " . $spedizione["numero"] . ", " . $spedizione["CAP"] . " " . $spedizione["città"]; ?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                        <option value="-1">Aggiungi un nuovo indirizzo di spedizione...</option>
                                                    </select>
                                                    <?php if ($ordinedettaglio["quantità"] > 1) : ?>
                                                        <a href="riepilogoOrdine.php?id=<?php echo $templateParams["ordine"][0]["numero"]; ?>&codice=<?php echo $ordinedettaglio["codice"] ?>&action=2">Spedisci a più indirizzi</a>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                                            </tr>

                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </section>
                        <footer>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-square" viewBox="0 0 16 16">
                                <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                            </svg>
                            <p>Puoi inviare l'albero che ordini a diversi destinatari selezionando il nome e l'indirizzo
                                di spedizione di ciascun destinatario.
                            </p>
                        </footer>
                    </article>
                    <div class="col-lg-2"></div>
                <?php endif; ?>
            </div>
            <div class="row">
                <?php if (isset($templateParams["ordinipianta"]) && count($templateParams["ordinipianta"]) >= 1) : ?>
                    <div class="col-lg-2"></div>
                    <article class="col-12 col-lg-8 mb-3 mt-3">
                        <header>
                            <h3>Alberi scelti da piantare</h3>
                        </header>
                        <section>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Albero</th>
                                        <th scope="col">Totale</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($templateParams["ordinipianta"] as $ordinedettaglio) : ?>

                                        <tr>
                                            <td><?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></td>
                                            <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                                        </tr>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </section>
                        <footer>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-square" viewBox="0 0 16 16">
                                <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                            </svg>
                            <p>Un contadino pianterà i tuoi alberi scelti
                                e potrai seguire la loro crescita passo passo a distanza.
                                I nuovi alberi compariranno nella sezione "Le mie piante"
                                che trovi nella tua area personale e potrai vedere i dati del tuo albero
                                e ricevere aggiornamenti fotografici dei tuoi alberi. </p>
                        </footer>
                    </article>
                    <div class="col-lg-2"></div>
                <?php endif; ?>
            </div>
            <div class="row">
                <?php if (isset($templateParams["ordinipiantaregala"]) && count($templateParams["ordinipiantaregala"]) >= 1) : ?>
                    <div class="col-lg-2"></div>
                    <article class="col-12 col-lg-8 mb-3 mt-3">
                        <header>
                            <h3>Alberi scelti da piantare per regalo</h3>
                        </header>
                        <section>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Albero</th>
                                        <th scope="col">Regala a</th>
                                        <th scope="col">Totale</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($templateParams["ordinipiantaregala"] as $ordinedettaglio) : ?>

                                        <tr>
                                            <td><?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></td>
                                            <td><?php echo $ordinedettaglio["email_regalo"] ?></td>
                                            <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                                        </tr>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </section>
                        <footer>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-square" viewBox="0 0 16 16">
                                <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                            </svg>
                            <p> Una volta che il destinatario si iscrive (o effettua il login nel caso in cui sia già iscritto)
                                potrà seguire l'albero dalla sezione "Le mie piante" nella propria area personale e ricevere gli aggiornamenti fotografici.</p>
                        </footer>
                    </article>
                    <div class="col-lg-2"></div>

                <?php endif; ?>
            </div>
            <div class="row">

                <section class="col-12 text-center">


                    <h4 class="col-12 mb-5 mt-5">Totale carrello: <?php echo $templateParams["ordine"][0]["prezzo_totale"]; ?> €</h4>
                    <div class="row">
                        <div class="col-4"></div>
                        <button type="submit" name="procedi" class="col-4 rounded-pill">Procedi con l'ordine</button>
                        <div class="col-4"></div>
                    </div>

                </section>

            </div>
        </form>
    </div>
</div>