<?php if (isset($templateParams["formmsg"])) : ?>
  <div class="row mt-5 mx-0">
    <div class="col-12 text-center">
      <p><?php echo $templateParams["formmsg"]; ?></p>
    </div>
  </div>
<?php endif; ?>
<?php
$dettaglio = $templateParams["dettaglio"];
$azione = getAction($templateParams["azione"]);

if ($templateParams["azione"] != 1) {
  $immagine = $templateParams["immagine"];
}

?>
<form action="processa-dettagliocrescita.php" class="p-5" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend class="text-left my-5">Dati dettaglo di crescita</legend>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Data rilevazione:</label>
      <div class="col-sm-10 col-md-4 col-lg-4">
        <input type="date" class="form-control" id="data_rilevazione" name="data_rilevazione" value="<?php echo $dettaglio["data_rilevazione"] ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Altezza:</label>
      <div class="col-sm-10 col-md-4 col-lg-4">
        <input type="number" step="0.01" class="form-control" id="altezza" name="altezza" value="<?php echo $dettaglio["altezza"] ?>">
      </div>
    </div>
    <?php if ($templateParams["azione"] == 1) : ?>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Inserisci immagine:</label>
        <div class="col-sm-10 col-md-4 col-lg-4">
          <label class="text-center rounded-pill" accept="image/*" for="file_immagine">Scegli file</label>
          <input type="file" name="immagine" id="file_immagine">
        </div>
      </div>
    <?php endif;
    if ($templateParams["azione"] == 2) : ?>
      <div class="form-group row">
        <label class="col-sm-12 mb-4">Immagine:</label>
        <div class="row col-12">
          <div class="col-6">
            <img class="img-fluid" name="<?php echo $immagine["nome"] ?>" src="<?php echo UPLOAD_DIR . $immagine["nome"] ?>" alt="<?php echo "Dettaglio di crescita dell'albero " . $templateParams["albero_piantato"] ?>">
          </div>
          <div class="col-6 my-auto">
            <label class="text-center rounded-pill" for="file_immagine_modifica">Sostituisci</label>
            <input type="file" name="immagine" accept="image/*" id="file_immagine_modifica">
          </div>
        </div>
      </div>
    <?php endif;
    if ($templateParams["azione"] == 3) : ?>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Immagine:</label>
        <div class="col-sm-10 col-md-4 col-lg-4">
          <img class="img-fluid mx-auto" name="<?php echo $immagine["nome"] ?>" src="<?php echo UPLOAD_DIR . $immagine["nome"] ?>" alt="<?php echo "Dettaglio di crescita dell'albero " . $templateParams["albero_piantato"] ?>">
        </div>
      </div>
    <?php endif; ?>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <input type="submit" value="<?php echo $azione; ?> dettagli di crescita" class="btn rounded-pill mr-5">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <input type="hidden" name="action" id="azione" value="<?php echo $templateParams["azione"]; ?>" />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 col-md-6 col-lg-6">
        <input type="hidden" class="form-control" id="albero_piantato" name="albero_piantato" value="<?php echo $templateParams["albero_piantato"] ?>">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 col-md-6 col-lg-6">
        <input type="hidden" class="form-control" id="codice" name="codice" value="<?php echo $dettaglio["codice"] ?>">
      </div>
    </div>
  </fieldset>
</form>