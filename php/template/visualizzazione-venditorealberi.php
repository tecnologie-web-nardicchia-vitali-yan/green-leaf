<?php if (isset($templateParams["formmsg"])) : ?>
    <div class="row mt-5 mx-0">
        <div class="col-12 text-center">
            <p><?php echo $templateParams["formmsg"]; ?></p>
        </div>
    </div>
<?php endif; ?>
<?php
if (count($templateParams["albero"]) == 0) :
?>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3 text-center">Alberi</h2>
                <a class="mt-2 nav-link text-center mx-auto rounded-pill text-center" href="gestisci-alberi.php?action=1">Inserisci nuovo albero</a>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php else : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Alberi</h2>
                <a class="mt-2 mb-3 nav-link text-center rounded-pill" href="gestisci-alberi.php?action=1">Inserisci nuovo albero</a>
                <?php foreach ($templateParams["albero"] as $albero) : ?>
                    <table class="table">
                        <tr>
                            <th class="row">Nome:</th>
                            <td><?php echo $albero["nome_albero"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Descrizione:</th>
                            <td><?php echo $albero["descrizione"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Costo:</th>
                            <td><?php echo $albero["costo"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Consumo CO2:</th>
                            <td><?php echo $albero["consumoCO2"] ?></td>
                        </tr>
                    </table>
                    <?php if (($albero["immagini"]) != null) :
                        $immagini = explode(',', $albero["immagini"]);
                        $size = count($immagini);
                    ?>
                        <div class="row mx-auto d-flex flex-wrap align-item-center container-fluid">
                            <?php for ($i = 0; $i < $size; $i++) : ?>
                                <div class="col-sm-12 col-md-4 col-lg-4">
                                    <img class="img-fluid" src="<?php echo UPLOAD_DIR . $immagini[$i] ?>" alt="<?php echo "Dettaglio dell'albero " . $albero["nome_albero"] ?>">
                                </div>
                            <?php endfor; ?>
                        </div>
                    <?php endif; ?>
                    <div class="container mx-auto">
                        <ul class="mt-2 mb-5 nav">
                            <li class="col-6"><a class="p-2 nav-link text-center rounded-pill" href="gestisci-alberi.php?action=2&nome_albero=<?php echo $albero["nome_albero"]?>">Modifica</a></li>
                            <li class="col-6"><a class="p-2 nav-link text-center rounded-pill" href="gestisci-alberi.php?action=3&nome_albero=<?php echo $albero["nome_albero"]?>">Cancella</a></li>
                        </ul>
                    </div>
                <?php endforeach; ?>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif; ?>