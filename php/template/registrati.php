<?php if (isset($templateParams["nome"])) : ?>
    <h2 class="text-center mb-4">Register</h2>
<?php endif; ?>
<div class="container-fluid text-right">
    <form action="#" class="p-5" method="POST">
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <label for="name" class="col-12 col-md-2 col-lg-2 col-form-label">Nome</label>
            <input type="text" id="name" class="form-control col-12 col-md-5 col-lg-4" name="name" placeholder="Nome" required />
            <div class="col-md-3 col-lg-4"></div>
        </div>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <label for="surname" class="col-12 col-md-2 col-lg-2 col-form-label">Cognome</label>
            <input type="text" id="surname" class="form-control col-12 col-md-5 col-lg-4" name="surname" placeholder="Cognome" required />
            <div class="col-md-3 col-lg-4"></div>
        </div>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <label for="phone" class="col-12 col-md-2 col-lg-2 col-form-label">Telefono</label>
            <input type="text" id="phone" class="form-control col-12 col-md-5 col-lg-4" name="phone" placeholder="Telefono" required />
            <div class="col-md-3 col-lg-4"></div>
        </div>
        <?php if (isset($templateParams["errore"])) : ?>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <div class="col-12 col-md-8 col-lg-8 text-center">
                <p class="text-danger"><?php echo $templateParams["errore"]; ?></p>
            </div>
            <div class="col-md-2 col-lg-2"></div>
        </div>
        <?php endif; ?>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <label for="email" class="col-12 col-md-2 col-lg-2 col-form-label">E-mail</label>
            <input type="email" id="email" class="form-control col-12 col-md-5 col-lg-4" name="email" placeholder="E-mail" required />
            <div class="col-md-3 col-lg-4"></div>
        </div>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <label for="password" class="col-12 col-md-2 col-lg-2 col-form-label">Password</label>
            <input type="password" id="password" class="form-control col-12 col-md-5 col-lg-4" name="password" placeholder="Password" required />
            <div class="col-md-3 col-lg-4"></div>
        </div>
        <?php if (isset($templateParams["registrato"])) : ?>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <div class="col-12 col-md-8 col-lg-8 text-center">
                <p class="text-danger"><?php echo $templateParams["registrato"]; ?></p>
            </div>
            <div class="col-md-2 col-lg-2"></div>
        </div>
        <?php endif; ?>
        <div class="form-group row">
            <div class="col-12 col-md-12 col-lg-12 text-center">
                <input type="submit" name="submit" class="btn" value="NEXT" />
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <div class="col-12 col-md-8 col-lg-8 text-center">
                <p class="mb-0" >Possiedi già un account?</p>
                <p><a href="login.php">Accedi</a> subito!</p>
            </div>
            <div class="col-md-2 col-lg-2"></div>
        </div>
    </form>
</div>