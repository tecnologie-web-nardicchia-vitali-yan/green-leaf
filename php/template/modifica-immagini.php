<?php if (isset($templateParams["formmsg"])) : ?>
    <div class="row mt-5 mx-0">
        <div class="col-12 text-center">
            <p><?php echo $templateParams["formmsg"]; ?></p>
        </div>
    </div>
<?php endif; ?>
<div class="container">
    <div class="row">
        <div class="col-1"></div>
        <section class="col-10">
            <h2 class="mt-4 mb-3">Immagini Albero: <?php echo $templateParams["nome_albero"] ?></h2>
            <a class="mt-2 nav-link text-center rounded-pill" href="#inserisci-immagine" data-toggle="collapse">Inserisci nuova immagine</a>
            <form action="processa-immagini.php?action=1" class="ml-0 collapse" id="inserisci-immagine" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-12">
                        <div class="row mt-5">
                            <div class="col-4">
                                <label class="text-center rounded-pill" for="file_immagine" >Scegli file</label>
                                <input type="file" name="immagine" accept="image/*" id="file_immagine">
                            </div>
                            <div class="col-4">
                                <p id="nome-file">Nessun file selezionato</p>
                            </div>
                            <div class="col-4">
                                <input type="submit" class="p-2 text-center btn rounded-pill" id="conferma" value="Conferma">
                                <input type="hidden" name="nome_albero" value ="<?php echo $templateParams["nome_albero"] ?>" >
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php foreach (array_slice($templateParams["immagini"], 0) as $img) : ?>
                <div class="row d-flex align-items-center">
                    <div class="col-6 ">
                        <img class="modifica img-fluid" src="<?php echo UPLOAD_DIR . $img["nome_immagine"] ?>" alt="<?php echo "Dettaglio dell'albero " . $templateParams["nome_albero"] ?>">
                    </div>
                    <div class="col-6 text-left">
                        <a class="btn border-dark rounded-pill" href="processa-immagini.php?action=3&nome_immagine=<?php echo $img["nome_immagine"] ?>&nome_albero=<?php echo $templateParams["nome_albero"] ?>">Elimina</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </section>
        <div class="col-1"></div>
    </div>
</div>