<?php
$pagamento = $templateParams["pagamento"];
$azione = getAction($templateParams["azione"]);
?>
<form action="processa-datipagamento.php" class="p-5" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend class="text-left my-5">Dati pagamento</legend>
    <?php if ($templateParams["azione"] == 1 || $templateParams["azione"] == 5) : ?>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Numero Carta:</label>
        <div class="col-sm-10 col-md-6 col-lg-5">
          <input type="text" class="form-control" id="numero" name="numero" value="<?php echo $pagamento["numero"]; ?>">
        </div>
      </div>
    <?php endif; ?>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Tipo Carta:</label>
      <div class="col-sm-10 col-md-4 col-lg-3">
        <select class="form-control" id="tipo_carta" name="tipo_carta">
          <option <?php if ($pagamento["tipo_carta"] == 'Visa') : ?> selected="selected" <?php endif; ?>>Visa</option>
          <option <?php if ($pagamento["tipo_carta"] == 'MasterCard') : ?> selected="selected" <?php endif; ?>>MasterCard</option>
          <option <?php if ($pagamento["tipo_carta"] == 'Maestro') : ?> selected="selected" <?php endif; ?>>Maestro</option>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-lg-2 col-form-label">CVV:</label>
      <div class="col-sm-10 col-md-3 col-lg-2">
        <input type="text" class="form-control" id="cvv" name="CVV" value="<?php echo $pagamento["CVV"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Nome titolare:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="nome_titolare" name="nome_titolare" value="<?php echo $pagamento["nome_titolare"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Cognome titolare:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="cognome_titolare" name="cognome_titolare" value="<?php echo $pagamento["cognome_titolare"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Data scadenza:</label>
      <div class="col-sm-10 col-md-4 col-lg-3">
        <input type="date" class="form-control" id="data_scadenza" name="data_scadenza" value="<?php echo $pagamento["data_scadenza"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <input type="submit" value="<?php echo $azione; ?> dati pagamento" class="btn rounded-pill">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <input type="hidden" name="action" id="action" value="<?php echo $templateParams["azione"]; ?>" />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <?php if ($templateParams["azione"] != 1 && $templateParams["azione"] != 5) : ?>
          <input type="hidden" name="numero" value="<?php echo $pagamento["numero"]; ?>" />
        <?php endif; ?>
      </div>
    </div>
  </fieldset>
</form>