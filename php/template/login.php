<?php if (isset($templateParams["nome"])) : ?>
    <h2 class="text-center mb-4">Log in</h2>
<?php endif; ?>
<div class="container-fluid text-right">
    <form action="#" class="p-5" method="POST">
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <label for="email" class="col-12 col-md-2 col-lg-2 col-form-label">E-mail</label>
            <input type="email" id="email" class="form-control col-12 col-md-5 col-lg-4" name="email" placeholder="E-mail" required />
            <div class="col-md-3 col-lg-4"></div>
        </div>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <label for="password" class="col-12 col-md-2 col-lg-2 col-form-label">Password</label>
            <input type="password" id="password" class="form-control col-12 col-md-5 col-lg-4" name="password" placeholder="Password" required />
            <div class="col-md-3 col-lg-4"></div>
        </div>
        <?php if (isset($templateParams["errore"])) : ?>
        <div class="form-group row">
            <div class="col-md-2 col-lg-2"></div>
            <div class="col-12 col-md-8 col-lg-8 text-center">
                <p class="text-danger"><?php echo $templateParams["errore"]; ?></p>
            </div>
            <div class="col-md-2 col-lg-2"></div>
        </div>
        <?php endif; ?>
        <div class="form-group row">
            <div class="col-12 col-md-12 col-lg-12 text-center">
                <input type="submit" name="submit" class="btn" value="LOG IN" />
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12 col-md-12 col-lg-12 text-center">
                <p class="mb-0" >Non possiedi ancora un account?</p>
                <p><a href="registrati.php<?php if(isset($templateParams["action"])) echo "?action=".$templateParams["action"];?>">Registrati</a> subito!</p>
            </div>
        </div>
    </form>
</div>