<?php
if (isset($templateParams["nome"])) :
    $totale = 0; ?>
    <h2 class="text-center mb-5">Riepilogo</h2>
<?php endif;?>

<div class="container-fluid">
    <div class="row d-flex align-items-center">

        <article class="col-12 mb-5">
            <div class="col-1 col-lg-1"></div>
            <header class="col-12">
                <h3>Alberi scelti</h3>
            </header>

            <table class="table col-10 col-lg-4">
                <thead>
                    <tr>
                        <th scope="col">Albero</th>
                        <th scope="col">Totale</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($templateParams["ordinedettaglio"] as $ordinedettaglio) : ?>

                        <tr>
                            <th scope="row">
                                <input class="mr-2" id="pianta" type="checkbox" name="piantare" <?php if ($ordinedettaglio["piantare"]) echo 'checked="checked"' ?> />
                                <label for="pianta"> <?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></label>
                            <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="col-1 col-lg-1"></div>
        </article>



        <section class="col-12 text-center">
            <form action="pagamento.php?id=<?php echo $templateParams["ordine"][0]["numero"]; ?>" method="POST">

                <h4 class="col-12 mb-5">Totale carrello: <?php echo $totale ?> €</h4>
                <input type="hidden" name="totale" value="<?php echo $totale ?>" />
                <div class="row">
                    <div class="col-4"></div>
                    <button type="submit" name="update" class="col-4 rounded-pill">Procedi con l'ordine</button>
                    <div class="col-4"></div>
                </div>

            </form>
        </section>

        <div class="col-1 col-lg-1"></div>
    </div>
</div>