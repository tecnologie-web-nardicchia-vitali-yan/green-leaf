<?php
$albero = $templateParams["albero"];
$azione = getAction($templateParams["azione"]);

if ($templateParams["azione"] != 1) {
  $immagini = $templateParams["immagini"];
}

?>
<form action="processa-datialbero.php" class="p-5" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend class="text-left my-5">Dati Albero</legend>
    <?php if ($templateParams["azione"] != 2) : ?>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Nome:</label>
        <div class="col-sm-10 col-md-6 col-lg-6">
          <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $albero["nome"]; ?>">
        </div>
      </div>
    <?php endif; ?>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Descrizione:</label>
      <div class="col-sm-10 col-md-6 col-lg-6">
        <textarea class="form-control" id="descrizione" name="descrizione">
      <?php echo $albero["descrizione"]; ?>
      </textarea>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Costo:</label>
      <div class="col-sm-10 col-md-3 col-lg-2">
        <input type="text" class="form-control" id="costo" name="costo" value="<?php echo $albero["costo"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Consumo CO2:</label>
      <div class="col-sm-10 col-md-3 col-lg-2">
        <input type="text" class="form-control" id="consumoCO2" name="consumoCO2" value="<?php echo $albero["consumoCO2"]; ?>">
      </div>
    </div>
    <?php if ($templateParams["azione"] == 1) : ?>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Inserisci immagine:</label>
        <div class="col-sm-10 col-md-4 col-lg-4">
          <label class="text-center rounded-pill" accept="image/*" for="file_immagine">Scegli file</label>
          <input type="file" name="immagine" id="file_immagine">
        </div>
      </div>
    <?php endif; ?>
    <?php if ($templateParams["azione"] == 3) : ?>
      <div class="form-group row">
        <label class="col-sm-12 mb-4">Immagini:</label>
        <?php foreach (array_slice($immagini, 0) as $img) : ?>
          <div class="col-sm-12 col-md-4 col-lg-4">
            <img class="img-fluid" src="<?php echo UPLOAD_DIR . $img["nome_immagine"] ?>" alt="<?php echo "Dettaglio dell'albero " . $albero["nome"] ?>">
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
    <?php if ($templateParams["azione"] == 2) : ?>
      <div class="form-group row">
        <div class="col-12 col-md-6 col-lg-6">
          <a class="btn rounded-pill" href="modifica-immagini.php?nome_albero=<?php echo $albero["nome"] ?>"><?php echo $azione; ?> immagini</a>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
          <input type="submit" value="<?php echo $azione; ?> dati albero" class="btn rounded-pill m-0">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-10 col-md-6 col-lg-6">
          <input type="hidden" class="form-control" id="nome" name="nome" value="<?php echo $albero["nome"]; ?>">
        </div>
      </div>
    <?php else : ?>
      <div class="form-group row">
        <div class="col-sm-10 text-right">
          <input type="submit" value="<?php echo $azione; ?> dati albero" class="btn rounded-pill mr-5">
        </div>
      </div>
    <?php endif; ?>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <input type="hidden" name="action" id="azione" value="<?php echo $templateParams["azione"]; ?>" />
      </div>
    </div>
  </fieldset>
</form>