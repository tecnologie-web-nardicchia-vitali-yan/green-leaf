<?php if (count($templateParams["albero"]) == 0) : ?>
    <article>
        <p>Articolo non presente</p>
    </article>
<?php
else :
    $albero = $templateParams["albero"][0];
    $immagini = $templateParams["immagini"];
    $imgDetail = count($immagini) - 1;
    $col = 4;
    switch ($imgDetail) {
        case $imgDetail % 4 == 0:
            $col = 3;
            break;
        case $imgDetail % 3 == 0:
            $col = 4;
            break;
        case $imgDetail % 2 == 0:
            $col = 6;
            break;
    }
?>
    <?php if (isset($templateParams["msg"])) : ?>
        <p class="text-center mt-5">
            <?php echo $templateParams["msg"] ?>
            <a href="carrello.php">Visualizza</a>
        </p>
    <?php endif; ?>
    <?php if (isset($templateParams["nome"])) : ?>
        <h2 class="text-center mb-5"><?php echo $albero["nome"] ?></h2>
    <?php endif; ?>

    <div class="container-fluid">
        <article class="row d-flex align-items-center">

            <div class="col-12 col-lg-6 text-center">
                <img class="img-fluid" src="<?php echo UPLOAD_DIR . $immagini[0]["nome_immagine"] ?>" alt="<?php echo $albero["nome"] ?>">
            </div>

            <section class="col-12 col-lg-6">
                <div>
                    <h3>Descrizione</h3>
                    <p><?php echo $albero["descrizione"] ?></p>
                </div>

                <div>
                    <h4>Costo</h4>
                    <p><?php echo $albero["costo"] . "€" ?></p>
                </div>

                <div>
                    <h4>Quanta CO2 compensa?</h4>
                    <p><?php echo "Piantando questo albero di " . $albero["nome"] . " assobirai " . $albero["consumoCO2"] . "Kg/CO2" ?></p>
                </div>
            </section>

            <section class="col-12 col-lg-12 text-center">
                <div class="row d-flex align-items-center">

                    <?php foreach (array_slice($immagini, 1) as $img) : ?>
                        <div class="col-12 col-lg-<?php echo $col ?>">
                            <img class="img-fluid" src="<?php echo UPLOAD_DIR . $img["nome_immagine"] ?>" alt="<?php echo "Dettaglio dell'albero " . $albero["nome"] ?>">
                        </div>
                    <?php endforeach; ?>
                </div>
            </section>


            <footer class="col-12 col-lg-12 text center">
                <div class="row">


                    <div class="col-4"></div>
                    <form action="#" method="POST" class="col-4">
                        <button type="submit" name="submit" class="btn nav-link text-center rounded-pill"> Aggiungi al carrello
                            <img class="ml-3" src="<?php echo UPLOAD_DIR . "/carrello.png"; ?>" alt="" />
                        </button>
                    </form>
                    <div class="col-4"></div>
                </div>
            </footer>


        </article>
    </div>

<?php endif; ?>