<?php
$spedizione = $templateParams["spedizione"];
$azione = getAction($templateParams["azione"]);
?>
<form action="processa-datispedizione.php" class="p-5" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend class="text-left my-5">Dati spedizione</legend>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Via:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="via" name="via" value="<?php echo $spedizione["via"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Numero civico:</label>
      <div class="col-sm-10 col-md-3 col-lg-2">
        <input type="number" class="form-control" id="numcivico" name="numero" value="<?php echo $spedizione["numero"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Città:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="città" name="città" value="<?php echo $spedizione["città"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Provincia:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="prov" name="provincia" value="<?php echo $spedizione["provincia"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">CAP:</label>
      <div class="col-sm-10 col-md-4 col-lg-3">
        <input type="text" class="form-control" id="cap" name="cap" value="<?php echo $spedizione["CAP"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Nome destinatario:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="nome-dest" name="nome_destinatario" value="<?php echo $spedizione["nome_destinatario"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Cognome destinatario:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="cognome-dest" name="cognome_destinatario" value="<?php echo $spedizione["cognome_destinatario"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <input type="submit" value="<?php echo $azione; ?> dati spedizione" class="btn rounded-pill">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <input type="hidden" name="action" value="<?php echo $templateParams["azione"]; ?>" />
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <?php if ($templateParams["azione"] != 1) : ?>
          <input type="hidden" name="codice" value="<?php echo $spedizione["codice"]; ?>" />
        <?php endif; ?>
      </div>
    </div>
  </fieldset>
</form>