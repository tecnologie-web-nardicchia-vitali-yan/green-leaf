<?php
if (isset($templateParams["nome"])) :
    $totale = 0; ?>
    <h2 class="text-center mb-5">Cassa</h2>
<?php endif;

if (isset($templateParams["formmsg"])) : ?>
    <div class="row mt-5 mx-0">
        <div class="col-12 text-center">
            <p><?php echo $templateParams["formmsg"]; ?></p>
        </div>
    </div>
<?php endif; ?>

<div class="container-fluid">
    <div class="row d-flex align-items-center">
        <div class="col-1 col-lg-2"></div>
        <div class="col-10 col-lg-8">
            <nav>

                <ul class="list-group list-group-horizontal mb-5 text-center nav">
                    <li class="list-group-item flex-fill nav-item">
                        <a class="nav-link" href="carrello.php">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart3" viewBox="0 0 16 16">
                                <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                            </svg>
                            Carrello
                        </a>
                    </li>
                    <li class="list-group-item flex-fill nav-item">
                        <a class="nav-link" href="riepilogoOrdine.php?id=<?php echo $templateParams["ordine"][0]["numero"]; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                                <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
                            </svg>
                            Dati di spedizione
                        </a>
                    </li>
                    <li class="list-group-item flex-fill active nav-item" aria-current="true">
                        <a class="nav-link" href="pagamento.php?id=<?php echo $templateParams["ordine"][0]["numero"]; ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-credit-card" viewBox="0 0 16 16">
                                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v1h14V4a1 1 0 0 0-1-1H2zm13 4H1v5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V7z" />
                                <path d="M2 10a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1z" />
                            </svg>
                            Pagamento
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-1 col-lg-2"></div>

        <form action="#" method="POST" class="col-12">
            <div class="row">

                <div class="col-lg-2"></div>
                <article class="col-12 col-lg-8">

                    <header>
                        <h3>Alberi scelti</h3>
                    </header>
                    <section>
                        <div class="table-responsive-sm">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Albero</th>
                                        <th scope="col">Spedisci a</th>
                                        <th scope="col">Totale</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($templateParams["ordininormali"] as $ordinedettaglio) : ?>
                                        <tr>
                                            <td><?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></td>
                                            <td>
                                                <?php $codice_spedizione = $ordinedettaglio["codice_spedizione"];
                                                foreach ($templateParams["datispedizione"] as $datispedizione) {
                                                    if ($datispedizione["codice"] == $codice_spedizione) {
                                                        $spedizione = $datispedizione;
                                                    }
                                                } ?>
                                                <p>
                                                    <?php echo $spedizione["nome_destinatario"] . " " . $spedizione["cognome_destinatario"] .
                                                        ", Via " . $spedizione["via"] . " " . $spedizione["numero"] . ", " . $spedizione["CAP"] . " " . $spedizione["città"]; ?>
                                                </p>
                                            </td>
                                            <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                                        </tr>

                                    <?php endforeach; ?>
                                    <?php foreach ($templateParams["ordiniregala"] as $ordinedettaglio) : ?>
                                        <tr>
                                            <td><?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></td>
                                            <td>
                                                <?php $codice_spedizione = $ordinedettaglio["codice_spedizione"];
                                                foreach ($templateParams["datispedizione"] as $datispedizione) {
                                                    if ($datispedizione["codice"] == $codice_spedizione) {
                                                        $spedizione = $datispedizione;
                                                    }
                                                } ?>
                                                <p>
                                                    Regala a <?php echo $spedizione["nome_destinatario"] . " " . $spedizione["cognome_destinatario"] .
                                                        ", Via " . $spedizione["via"] . " " . $spedizione["numero"] . ", " . $spedizione["CAP"] . " " . $spedizione["città"]; ?>
                                                </p>
                                            </td>
                                            <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                                        </tr>

                                    <?php endforeach; ?>
                                    <?php foreach ($templateParams["ordinipianta"] as $ordinedettaglio) : ?>

                                        <tr>
                                            <td><?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></td>
                                            <td>Albero scelto da piantare</td>
                                            <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                                        </tr>

                                    <?php endforeach; ?>
                                    <?php foreach ($templateParams["ordinipiantaregala"] as $ordinedettaglio) : ?>

                                        <tr>
                                            <td><?php echo $ordinedettaglio["nome_albero"] . " × " . $ordinedettaglio["quantità"]; ?></td>
                                            <td>Albero scelto da piantare per regalo a <?php echo $ordinedettaglio["email_regalo"] ?></td>
                                            <td><?php echo ($ordinedettaglio["quantità"] * $ordinedettaglio["costo"]) ?> €</td>
                                        </tr>

                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <footer>
                        <h4 class="col-12 mb-5 mt-5 text-center">Totale carrello: <?php echo $templateParams["ordine"][0]["prezzo_totale"]; ?> €</h4>
                    </footer>
                </article>
                <div class="col-lg-2"></div>
            </div>

            <div class="row">
                <div class="col-lg-2"></div>
                <article class="col-12 col-lg-8">
                    <div>

                        <header class="mb-3 text-left">
                            <h3>Paga con: </h3>
                        </header>

                        <section class="text-left">
                            <div class="form-check row">
                                <?php foreach ($templateParams["datipagamento"] as $datipagamento) : ?>
                                    <div class="form-group row form-check ml-1 mt-1">
                                        <input class="form-check-input" type="radio" name="datipagamento" id="dato<?php echo $datipagamento["numero"] ?>" value=<?php echo $datipagamento["numero"] ?> checked>
                                        <label class="form-check-label" for="dato<?php echo $datipagamento["numero"] ?>">
                                            <?php echo $datipagamento["tipo_carta"] . " " . $datipagamento["numero"] . " " . $datipagamento["nome_titolare"] . " " . $datipagamento["cognome_titolare"] ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <a class="nav-link rounded-pill col-8 col-lg-4 text-center" href="gestisci-datipagamento.php?action=5">Aggiungi una carta</a>
                        </section>
                        <?php if (isset($templateParams["errore"])) : ?>
                            <section class="text-left mt-3">
                                <p class="text-danger"><?php echo $templateParams["errore"]; ?></p>
                            </section>
                        <?php endif; ?>

                        <div class="col-7"></div>
                    </div>

                    <footer class="text-center">
                        <div class="row">
                            <div class="col-2 col-lg-4"></div>
                            <button type="submit" name="paga" class="col-8 col-lg-4 rounded-pill">Paga</button>
                            <div class="col-2 col-lg-4"></div>
                        </div>
                    </footer>

                </article>

                <div class="col-lg-2"></div>

            </div>
        </form>
    </div>
</div>