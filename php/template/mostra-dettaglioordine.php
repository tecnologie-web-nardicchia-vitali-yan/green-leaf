<?php if ($templateParams["dettagli_ordine_spediti"] != null) : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Alberi acquistati</h2>
                <table class="mt-4 table table-striped">
                    <tr class="bg-white">
                        <th>Albero</th>
                        <th>Quantità</th>
                        <th>E' un regalo?</th>
                        <th>Indirizzo di consegna</th>
                        <th>Destinatario</th>
                    </tr>
                    <?php foreach ($templateParams["dettagli_ordine_spediti"] as $dettaglio) : ?>
                        <tr>

                            <td><?php echo $dettaglio["nome_albero"] ?></td>
                            <td><?php echo $dettaglio["quantità"] ?></td>
                            <td><?php if ($dettaglio["regalo"]) {
                                    echo "Si";
                                } else {
                                    echo "No";
                                } ?></td>
                            <td><?php echo $dettaglio["via"] . ", " .  $dettaglio["numero"] . ", " . $dettaglio["città"] ?></td>
                            <td><?php echo $dettaglio["nome_destinatario"] . " " .  $dettaglio["cognome_destinatario"] ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif;
if ($templateParams["dettagli_ordine_piantare"] != null) : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Alberi da piantare</h2>
                <table class="mt-4 table table-striped">
                    <tr class="bg-white">
                        <th>Albero</th>
                        <th>Quantità</th>
                        <th>E' un regalo?</th>
                    </tr>
                    <?php foreach ($templateParams["dettagli_ordine_piantare"] as $dettaglio) : ?>
                        <tr>

                            <td><?php echo $dettaglio["nome_albero"] ?></td>
                            <td><?php echo $dettaglio["quantità"] ?></td>
                            <td><?php if ($dettaglio["regalo"]) {
                                    echo "Si";
                                } else {
                                    echo "No";
                                } ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif; ?>