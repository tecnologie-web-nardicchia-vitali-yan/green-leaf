    <?php if (isset($templateParams["nome"])) : ?>
        <h2 class="text-center mb-5">Come regalare un albero</h2>
    <?php endif; ?>

    <div class="container-fluid">

        <div class="row d-flex align-items-top text-center">
                <div class="col-lg-1"></div>

                <article class="col-lg-4">
                <div class="row">           
                        <div class="col-12 col-lg-6">
                            <header>
                                <div>
                                    <img src="../img/AlberoSfondo.png" alt="Albero" />
                                </div>

                                <h3>Scegli</h3>
                            </header>
                            <section>
                                <p>Scegli fra tante piante a disposizione disponibili nell'area alberi</p>
                            </section>
                        </div>

                        <div class="col-12 col-lg-6">
                            <footer>
                                <div>
                                    <img src="../img/Arrow.png" alt="Freccia" />
                                </div>
                            </footer>
                        </div>
                        </div>
                </article>


  
                <article class="col-lg-4">
                <div class="row">
                        <div class="col-12 col-lg-6">
                            <header>
                                <div>
                                    <img src="../img/Paga.png" alt="Acquista" />
                                </div>

                                <h3>Acquista</h3>
                            </header>
                            <section>
                                <p>Scegli l’opzione regalo al momento dell’acquisto<br/></p>
                            </section>
                        </div>
                        <div class="col-12 col-lg-6">
                            <footer>
                                <div>
                                    <img src="../img/Arrow.png" alt="Freccia" />
                                </div>
                            </footer>
                        </div>
                        </div>
                </article>
     

            
                <article class="col-12 col-lg-2">
                        <header>
                            <div>
                                <img src="../img/RegaloSfondo.png" alt="Regala" />
                            </div>

                            <h3>Regala</h3>
                        </header>
                        <section>
                            <p>Scegli il nome del destinatario, scrivi una dedica e spedisci via email</p>
                        </section>              
                </article>

                <div class="col-lg-1"></div>
            
        </div>
    </div>