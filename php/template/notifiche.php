<div class="container">
    <div class="row">
        <div class="col-lg-1"></div>
        <section class="col-lg-10">
            <?php if (count($templateParams["notifiche"])==0) : ?>
            <h2 class="mt-4 mb-3 text-center">Notifiche</h2>
            <p class="text-center">Non hai ricevuto notifiche</p>
            <?php else : ?>
            <h2 class="mt-4 mb-3">Notifiche</h2>
            <table class="mt-4 table table-striped">
                <tr class="bg-white">
                    <th>Tipologia</th>
                    <th>Data</th>
                    <th>Messaggio di notifica</th>
                </tr>
                <?php foreach ($templateParams["notifiche"] as $notifica) : ?>
                    <tr class="alert alert-dismissible fade show" role="alert">
                        <td>
                            <p class="d-none"><?php echo $notifica["numero"]; ?></p>
                            <?php if ($notifica["codice"]==1) : ?>
                                <img src="<?php echo UPLOAD_DIR . "/notifiche.png"; ?>" alt="" />
                            <?php elseif ($notifica["codice"]==2) : ?>
                                <img  src="<?php echo UPLOAD_DIR . "/pagaIcon.png"; ?>" alt="" />
                            <?php elseif ($notifica["codice"]==3 || $notifica["codice"]==8) : ?>
                                <img  src="<?php echo UPLOAD_DIR . "/box.png"; ?>" alt="" />
                            <?php elseif ($notifica["codice"]==5 || $notifica["codice"]==7) : ?>
                                    <img  src="<?php echo UPLOAD_DIR . "/regalo.png"; ?>" alt="" />
                            <?php elseif ($notifica["codice"]==4 || $notifica["codice"]==6 || $notifica["codice"]==7) : ?>
                                <img  src="<?php echo UPLOAD_DIR . "/pianta.png"; ?>" alt="" />
                            <?php endif; ?>
                        </td>
                        <td>
                            <p><?php echo $notifica["data"]; ?></p>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-10">
                                    <p class="mb-0"><?php echo $notifica["messaggio"]; ?></p>
                                    <?php if ($notifica["codice"]==2) : ?>
                                        <p>Visualizza gli ordini effettuati: <a href="visualizzazione-ordini.php">I miei ordini</a></p>
                                    <?php elseif ($notifica["codice"]==7) : ?>
                                        <p>Scopri da chi andando in: <a href="visualizzazione-lemiepiante.php">Le mie piante</a></p>
                                    <?php elseif ($notifica["codice"]==4 || $notifica["codice"]==7) : ?>
                                        <p>Visualizza gli aggiornamenti: <a href="visualizzazione-lemiepiante.php">Le mie piante</a></p>
                                    <?php elseif ($notifica["codice"]==6) : ?>
                                        <p>Visualizza gli ordini: <a href="gestisci-ordini.php">Gestisci ordini</a></p>
                                    <?php elseif ($notifica["codice"]==8) : ?>
                                        <p>Visualizza gli ordini effettuati: <a href="gestisci-ordini.php">Gestisci ordini</a></p>
                                    <?php endif; ?>
                                </div>
                                <button type="button" class="close col-2" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <?php endif; ?>  
        </section>
        <div class="col-lg-1"></div>
    </div>
</div>