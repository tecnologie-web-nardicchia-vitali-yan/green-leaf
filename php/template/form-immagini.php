<div id="immagini">
  <div class="form-group row">
    <div class="col-sm-10 text- center">
      <label for="immagine_albero" class="my-auto  p-2 rounded-pill text-center"> Inserisci immagine</label>
      <input type="file" id="immagine_albero" name="nome_immagine" accept="image/">
    </div>
  </div>
  <?php
  foreach ($immagini as $img) :
  ?>
    <div class="form-gorup row">
      <div class="col-1"></div>
      <div class="col-5 mt-4">
        <img class="img-fluid" src="<?php echo UPLOAD_DIR . $img["nome_immagine"] ?>" alt="<?php echo "Dettaglio dell'albero " . $albero["nome"] ?>">
      </div>
      <div class="col-5 mt-4 ">
        <div class="row">
          <div class="col-12 d-flex justify-content-center">
            <input type="submit" value="Rinomina" id="modifica_img" class="btn rounded-pill mr-5">
          </div>
        </div>
        <div class="row">
          <div class="col-12 d-flex justify-content-center">
            <input type="submit" value="Elimina" id="modifica_img" class="btn rounded-pill mr-5">
          </div>
        </div>
      </div>
    <?php endforeach; ?>
    </div>
</div>