<?php if (count($templateParams["dettaglio_crescita"]) == 0) : ?>
    <div class="row align-items-center">
        <div class="col-1"></div>
        <section class="col-10">
            <h2 class="mt-4 mb-3 text-center text-danger">Non sono ancora stati caricati dettagli di crescita</h2>
        </section>
        <div class="col-1"></div>
    </div>
<?php else :
    $dettagli_crescita = $templateParams["dettaglio_crescita"];
    $ultima_rilevazione = $templateParams["ultima_rilevazione"][0];
?>

    <section>
        <h2 class="mt-3 text-center">Ecco il tuo albero!</h2>
    </section>


    <div class="mt-4 container-fluid">
        <article class="row d-flex align-items-center">

            <div class="col-12 col-lg-6 text-center">
                <img class="img-fluid" src="<?php echo UPLOAD_DIR . $ultima_rilevazione["nome"] ?>" alt="ultima immagine di crescita">
            </div>

            <section class="col-12 col-lg-6">
                <div class="bg-white text-center">
                    <h3>Quanto è cresciuto il tuo albero?</h3>
                </div>

                <div>
                    <h4>Data ultima rilevazione</h4>
                    <p><?php echo $ultima_rilevazione["data_rilevazione"] ?></p>
                </div>

                <div>
                    <h4>Altezza rilevata</h4>
                    <p><?php echo $ultima_rilevazione["altezza"] ?> m</p>
                </div>
            </section>

            <?php if (count($dettagli_crescita) > 1) : ?>
                <section class="bg-white col-12 col-lg-12 text-center">
                    <div class="text-center">
                        <h3>Le immagini del tuo albero</h3>
                    </div>

                    <div class="bg-white container text-center">
                        <div id="myCarousel" class="carousel slide mx-auto" data-ride="carousel">

                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <?php for ($i = 1; $i < count($dettagli_crescita); $i++) : ?>
                                    <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>"></li>
                                <?php endfor; ?>
                            </ol>


                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="img-fluid" src="<?php echo UPLOAD_DIR . $ultima_rilevazione["nome"] ?>" alt="#">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h4>Data rilevazione</h4>
                                        <p><?php echo $ultima_rilevazione["data_rilevazione"] ?></p>
                                    </div>
                                </div>

                                <?php foreach (array_slice($dettagli_crescita, 1) as $dettaglio) : ?>
                                    <div class="carousel-item">
                                        <img class="img-fluid" src="<?php echo UPLOAD_DIR . $dettaglio["nome"] ?>" alt="#">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h4>Data rilevazione</h4>
                                            <p><?php echo $dettaglio["data_rilevazione"] ?></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>


                            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

            <?php if (count($dettagli_crescita) > 1) : ?>
                <footer class="col-12 col-lg-12 text center">
                    <div class="row">
                        <div class="col-sm-1 col-md-4 col-lg-4"></div>
                        <div class="col-sm-10 col-md-4 col-lg-4">
                            <a class="mt-1 nav-link text-center rounded-pill" href="#dettaglicrescita" data-toggle="collapse">Mostra precedenti rilevazioni</a>
                        </div>
                        <div class="col-sm-2 col-md-4 col-lg-4"></div>
                    </div>
                    <div class="mt-5 container mx-auto collapse" id="dettaglicrescita">
                        <div class="row">
                            <div class="col-1"></div>
                            <table class="table col-10">
                                <tr>
                                    <th>Data rilevazione</th>
                                    <th>Altezza</th>
                                </tr>
                                <?php foreach (array_slice($dettagli_crescita, 1) as $dettaglio) : ?>
                                    <tr>
                                        <td><?php echo $dettaglio["data_rilevazione"] ?></td>
                                        <td><?php echo $dettaglio["altezza"] ?> m</td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                            <div class="col-1"></div>
                        </div>
                    </div>
                </footer>
            <?php endif; ?>


        </article>
    </div>
<?php endif; ?>