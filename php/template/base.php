<!DOCTYPE html>
<html lang="it">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta charset="UTF-8" />
        <title><?php echo $templateParams["titolo"]; ?></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="./css/style.css" />
        <?php if (isset($templateParams["stile"])) : ?>
            <link rel="stylesheet" type="text/css" href="<?php echo UPLOAD_STYLE . $templateParams["stile"]; ?>" />
        <?php endif; ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <?php if (isset($templateParams["js"])) : ?>
            <script src="<?php echo UPLOAD_JS . $templateParams["js"]; ?>"></script>
        <?php endif; ?>
        <script src="<?php echo UPLOAD_JS; ?>menu.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>

    <body>
        <header class="row justify-content-center px-0 m-0">
            <div class="col-10 col-md-11 col-lg-11">
                <img src="<?php echo UPLOAD_DIR . "corniceSuperiore.png"; ?>" alt="" />
                <h1 class="text-center">Green Leaf</h1>
                <img src="<?php echo UPLOAD_DIR . "corniceInferiore.png"; ?>" alt="" />
            </div>
            <div class="dropdown col-2 col-md-1 col-lg-1">
                <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenu" data-bs-toggle="dropdown" aria-expanded="false">
                    <img class="justify-content-start" src="<?php echo UPLOAD_DIR . "utente2.png"; ?>" alt="utente" />
                </a>
                <?php
                if (isset($templateParams["menu"])) {
                    require($templateParams["menu"]);
                }
                ?>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light focusable col-12 col-md-12 col-lg-12">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                    <img class="navbar-toggler-icon clickable focusable justify-content-start" src="<?php echo UPLOAD_DIR . "utente2.png"; ?>" alt="utente" />
                </button>

                <div class="collapse navbar-collapse" id="navbarToggler">
                    <?php
                    if (isset($templateParams["menu"])) {
                        require($templateParams["menu"]);
                    }
                    ?>
                </div>
            </nav>
        </header>

        <div class="container-fluid">
            <div class="row justify-content-center">
                <nav class="col-12">
                    <ul class="nav">
                        <li class="nav-item col-12 col-md-6 col-lg-3">
                            <a class="nav-link text-center rounded-pill <?php isActive("index.php"); ?>" href="index.php">Alberi
                                <img class="ml-3" src="<?php echo UPLOAD_DIR . "albero2.png"; ?>" alt="" />
                            </a>
                        </li>
                        <li class="nav-item col-12 col-md-6 col-lg-3">
                            <a class="nav-link text-center rounded-pill <?php isActive("regala.php"); ?>" href="regala.php">Regala
                                <img class="ml-3" src="<?php echo UPLOAD_DIR . "regalo.png"; ?>" alt="" />
                            </a>
                        </li>
                        <li class="nav-item col-12 col-md-6 col-lg-3">
                            <a class="nav-link text-center rounded-pill <?php isActive("pianta.php"); ?>" href="pianta.php">Pianta
                                <img class="ml-3" src="<?php echo UPLOAD_DIR . "pianta.png"; ?>" alt="" />
                            </a>
                        </li>
                        <li class="nav-item col-12 col-md-6 col-lg-3">
                            <a class="nav-link text-center rounded-pill <?php isActive("carrello.php"); ?>" href="carrello.php">Carrello
                                <img class="ml-3" src="<?php echo UPLOAD_DIR . "carrello.png"; ?>" alt="" />
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <main>
            <?php
            if (isset($templateParams["nome"])) {
                require($templateParams["nome"]);
            }
            ?>
        </main>
        <footer>
            <p class="font-weight-bold text-center mt-3">Social</p>
            <div class="container-fluid">
                <ul class="row justify-content-center px-0">
                    <li class="col-3 col-lg-3 text-center"><a href="https://www.instagram.com"><img src="<?php echo UPLOAD_DIR . "instagram.png"; ?>" alt="instagram" /></a></li>
                    <li class="col-3 col-lg-3 text-center"><a href="https://twitter.com"><img src="<?php echo UPLOAD_DIR . "twitter.png"; ?>" alt="twitter" /></a></li>
                    <li class="col-3 col-lg-3 text-center"><a href="https://facebook.com"><img src="<?php echo UPLOAD_DIR . "facebook.png"; ?>" alt="facebook" /></a></li>
                    <li class="col-3 col-lg-3 text-center"><a href="https://web.telegram.org"><img src="<?php echo UPLOAD_DIR . "telegram.png"; ?>" alt="telegram" /></a></li>
                </ul>
            </div>

        </footer>
    </body>

</html>