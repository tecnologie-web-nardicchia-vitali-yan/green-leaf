<?php if (isset($templateParams["formmsg"])) : ?>
    <div class="row mt-5 mx-0">
        <div class="col-12 text-center">
            <p><?php echo $templateParams["formmsg"]; ?></p>
        </div>
    </div>
<?php endif; ?>
<?php
if ($templateParams["dettaglio_crescita"] == null) :
?>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3 text-center">Dettagli di crescita dell'albero: <?php echo $templateParams["codice_albero"]; ?></h2>
                <a class="mt-2 nav-link text-center mx-auto rounded-pill text-center" href="gestisci-dettagliocrescita.php?action=1&albero_piantato=<?php echo $templateParams["codice_albero"]; ?>">Inserisci nuova rilevazione</a>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php else : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Dettagli di crescita dell'albero: <?php echo $templateParams["codice_albero"]; ?></h2>
                <a class="mt-2 mb-3 nav-link text-center rounded-pill" href="gestisci-dettagliocrescita.php?action=1&albero_piantato=<?php echo $templateParams["codice_albero"]; ?> ">Inserisci nuova rilevazione</a>
                <?php foreach ($templateParams["dettaglio_crescita"] as $dettaglio) : ?>
                    <table class="table">
                        <tr>
                            <th>Data rilevazione</th>
                            <th>Altezza</th>
                            <th>Immagine</th>
                        </tr>
                        <tr>
                            <td><?php echo $dettaglio["data_rilevazione"] ?></td>
                            <td><?php echo $dettaglio["altezza"] ?> m</td>
                            <td class="w-25"><img class="img-fluid" src="<?php echo UPLOAD_DIR . $dettaglio["nome"] ?>" alt=" <?php echo "Dettaglio di crescita dell'albero" . $dettaglio["codice"] ?>"></td>
                            
                        </tr>
                    </table>
                    <div class="container mx-auto">
                        <ul class="mt-2 mb-5 nav">
                            <li class="col-6"><a class="p-2 nav-link text-center rounded-pill" href="gestisci-dettagliocrescita.php?action=2&codice=<?php echo $dettaglio["codice"]?>&albero_piantato=<?php echo $templateParams["codice_albero"]; ?>">Modifica</a></li>
                            <li class="col-6"><a class="p-2 nav-link text-center rounded-pill" href="gestisci-dettagliocrescita.php?action=3&codice=<?php echo $dettaglio["codice"]?>&albero_piantato=<?php echo $templateParams["codice_albero"]; ?>">Cancella</a></li>
                        </ul>
                    </div>
                <?php endforeach; ?>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif; ?>