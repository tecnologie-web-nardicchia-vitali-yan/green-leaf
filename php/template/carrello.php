<?php if (!isset($templateParams["dettaglioordine"]) || count($templateParams["dettaglioordine"]) == 0) : ?>
    <article class="text-center">
        <div>
            <img class="img-fluid mb-5" src="<?php echo UPLOAD_DIR . "/Groot.png"; ?>" alt="">
        </div>
        <h2>Il tuo carrello è vuoto...</h2>
    </article>

    <?php
else :
    if (isset($templateParams["nome"])) :
        $totale = 0; ?>
        <h2 class="text-center mb-5">Carrello</h2>
    <?php endif; ?>


    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-1 col-lg-2"></div>
            <div class="col-10 col-lg-8">
                <nav>

                    <ul class="list-group list-group-horizontal mb-5 text-center nav">
                        <li class="list-group-item flex-fill active nav-item" aria-current="true">
                            <a class="nav-link" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart3" viewBox="0 0 16 16">
                                    <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                                </svg>
                                Carrello
                            </a>
                        </li>
                        <li class="list-group-item flex-fill nav-item">
                            <a class="nav-link disabled" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                                    <path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
                                </svg>
                                Dati di spedizione
                            </a>
                        </li>
                        <li class="list-group-item flex-fill nav-item">
                            <a class="nav-link disabled" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-credit-card" viewBox="0 0 16 16">
                                    <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v1h14V4a1 1 0 0 0-1-1H2zm13 4H1v5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V7z" />
                                    <path d="M2 10a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1z" />
                                </svg>
                                Pagamento
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-1 col-lg-2"></div>
            <?php foreach ($templateParams["dettaglioordine"] as $ordinedettaglio) : ?>

                <div class="col-1 col-lg-2"></div>

                <article class="col-10 col-lg-8">
                    <div class="row text-center mb-3">
                        <div class="col-1 col-lg-1"></div>
                        <header class="col-10 col-lg-10">
                            <h3><?php echo $ordinedettaglio["nome_albero"]; ?></h3>
                        </header>
                        <div class="col-1 col-lg-1"></div>
                    </div>

                    <section class="row">
                        <div class="col-lg-1"></div>

                        <div class="col-12 col-lg-5 text-center">
                            <img class="img-fluid" src="<?php echo UPLOAD_DIR . $ordinedettaglio["nome_immagine"]; ?>" alt="<?php echo $ordinedettaglio["nome_albero"]; ?>" />
                        </div>

                        <div class="col-12 col-lg-5">
                            <div class="row">

                                <p class="col-6">Quantità:</p>
                                <p class="col-6"><?php echo $ordinedettaglio["quantità"] ?></p>

                                <p class="col-6">Prezzo unitario: </p>
                                <p class="col-6"><?php echo $ordinedettaglio["costo"] ?> €</p>

                                <p class="col-6">Prezzo totale: </p>
                                <p class="col-6"><?php $costo = $ordinedettaglio["quantità"] * $ordinedettaglio["costo"];
                                                    echo $costo ?> €</p>

                                <?php $totale = $totale + $costo; ?>

                                <p class="col-6">Lo vuoi piantare?</p>
                                <p class="col-6"><?php echo $ordinedettaglio["piantare"] ? "Sì" : "No" ?></p>

                                <p class="col-6">Lo vuoi regalare?</p>
                                <p class="col-6"><?php echo $ordinedettaglio["regalo"] ? "Sì" : "No" ?></p>

                                <?php if ($ordinedettaglio["piantare"] && $ordinedettaglio["regalo"]) : ?>
                                    <div class="col-12">
                                        <p>Email dell'amico da regalare l'albero piantato:</p>
                                        <p><?php echo $ordinedettaglio["email_regalo"]; ?></p>
                                    </div>
                                <?php endif; ?>

                                <?php if (isset($templateParams["indirizzospedizione"][$ordinedettaglio["codice_spedizione"]])) :
                                    $indirizzo = $templateParams["indirizzospedizione"][$ordinedettaglio["codice_spedizione"]][0]; ?>
                                    <div class="col-12">
                                        <p>Indirizzo di spedizione:</p>
                                        <p><?php echo $indirizzo["nome_destinatario"] . " " . $indirizzo["cognome_destinatario"] ?></p>
                                        <p>Via <?php echo $indirizzo["via"] . " " . $indirizzo["numero"] . " " . $indirizzo["città"] . " " . $indirizzo["provincia"] . " " . $indirizzo["CAP"] ?></p>
                                    </div>
                                <?php endif; ?>

                                <a class="col-4 nav-link text-center rounded-pill ml-3" href="gestisciCarrello.php?action=1&order=<?php echo $templateParams["orderId"]; ?>&id=<?php echo $ordinedettaglio["codice"]; ?>">Modifica</a>
                                <div class="col-2"></div>
                                <a class="col-4 nav-link text-center rounded-pill" href="carrello.php?action=1&id=<?php echo $ordinedettaglio["codice"]; ?>">Elimina</a>

                            </div>
                        </div>

                        <div class="col-lg-1"></div>
                    </section>
                </article>
                <div class="col-1 col-lg-2"></div>

            <?php endforeach; ?>
            <section class="col-12 text-center">
                <form action="riepilogoOrdine.php?<?php echo $templateParams["action"] ?>" method="POST">

                    <h4 class="col-12 mb-5">Totale carrello: <?php echo $totale ?> €</h4>
                    <input type="hidden" name="totale" value="<?php echo $totale ?>" />
                    <div class="row">
                        <div class="col-4"></div>
                        <button type="submit" name="update" class="col-4 rounded-pill">Procedi con l'ordine</button>
                        <div class="col-4"></div>
                    </div>

                </form>
            </section>


        </div>
    </div>
<?php endif; ?>