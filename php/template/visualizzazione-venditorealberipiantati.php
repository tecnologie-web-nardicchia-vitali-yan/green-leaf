<?php if (isset($templateParams["formmsg"])) : ?>
    <div class="row mt-5 mx-0">
        <div class="col-12 text-center">
            <p><?php echo $templateParams["formmsg"]; ?></p>
        </div>
    </div>
<?php endif; ?>
<?php
if (count($templateParams["albero"]) == 0) :
?>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3 text-center">Alberi piantati</h2>
                <h3 class="text-center text-danger">Non è ancora stato piantato nessun albero</h3>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php else : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Alberi piantati</h2>
                <?php foreach ($templateParams["albero"] as $albero) : ?>
                    <table class="table">
                        <tr>
                            <th class="row">Codice:</th>
                            <td><?php echo $albero["codice"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Data piantagione:</th>
                            <td><?php echo $albero["data_piantagione"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Email propretario:</th>
                            <td><?php echo $albero["email_utente"] ?></td>
                        </tr>
                        <tr>
                            <th class="row">Albero:</th>
                            <td><?php echo $albero["nome_albero"] ?></td>
                        </tr>
                    </table>
                    <div class="container mx-auto">
                        <ul class="mt-2 mb-5 nav">
                            <li class="col-sm-12 col-md-6 col-lg-6 dettaglio"><a class="p-2 nav-link text-center rounded-pill" href="dettaglio-crescitavenditore.php?codice=<?php echo $albero["codice"] ?>">Mostra dettagli crescita</a></li>
                            <li class="col-sm-12 col-md-6 col-lg-6 dettaglio"><a class="p-2 nav-link text-center rounded-pill" href="processa-alberipiantati.php?action=3&codice=<?php echo $albero["codice"] ?>">Cancella</a></li>
                        </ul>
                    </div>
                <?php endforeach; ?>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif; ?>