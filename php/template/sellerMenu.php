<ul class="dropdown-menu" aria-labelledby="dropdownMenu">
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="gestisci-ordini.php">Gestisci ordini</a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "box.png"; ?>" alt="" />
    </li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="visualizzazione-venditorealberipiantati.php">Alberi piantati</a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "pianta.png"; ?>" alt="" />
    </li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="visualizzazione-venditorealberi.php">Gestisci albero</a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "albero2.png"; ?>" alt="" />
    </li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="notifiche.php">Notifiche<span class="ml-2 badge"><?php echo $notifiche; ?></span></a>
        <img class="col-3 col-md-4 col-lg-4" src="<?php echo UPLOAD_DIR . "notifiche.png"; ?>" alt="" />
    </li>
    <li class="dropdown-divider"></li>
    <li class="row m-0 py-1">
        <a class="dropdown-item col-12 col-md-12 col-lg-12" href="logout.php">Logout</a>
    </li>
</ul>