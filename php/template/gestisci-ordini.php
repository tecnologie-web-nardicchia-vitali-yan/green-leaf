<?php if (isset($templateParams["nome"])) : ?>
    <h2 class="text-center mb-5">Gestisci ordini</h2>
<?php endif; ?>

<div class="container-fluid">
    <div class="row d-flex align-items-center">

        <?php if (count($orders_not_send) != 0) : ?>
            <?php foreach ($orders_not_send as $order_number) :
                $order_number = $order_number["numero"];
                $templateParams["dettagli_ordine"] = $dbh->getOrderDetails($order_number);
                $templateParams["ordine"] = $dbh->getOrderById($order_number); ?>

                <form action="gestisci-ordini.php" method="POST" class="col-12" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-1 col-lg-2"></div>
                        <article class="col-10 col-lg-8">
                            <header data-toggle="collapse" data-target="#collapse<?php echo $order_number; ?>" aria-expanded="false" aria-controls="collapse<?php echo $order_number; ?>">
                                <h3 class="m-1">Ordine <?php echo $order_number; ?></h3>
                            </header>
                            <div class="collapse" id="collapse<?php echo $order_number; ?>">
                                <section>
                                    <div class="table-responsive-sm">
                                        <table class="table table-striped">
                                            <tr class="bg-white">
                                                <th>Albero</th>
                                                <th>Quantità</th>
                                                <th>E' un regalo?</th>
                                                <th>E' da piantare?</th>
                                                <th>Indirizzo di consegna<br/>/ Luogo piantagione</th>
                                                <th>Destinatario</th>
                                                <th>Regalo dedicato a</th>
                                            </tr>
                                            <?php foreach ($templateParams["dettagli_ordine"] as $dettaglio) : ?>
                                            <tr>
                                                <td><?php echo $dettaglio["nome_albero"]; ?></td>
                                                <td><?php echo $dettaglio["quantità"]; ?></td>
                                                <td>
                                                    <?php if ($dettaglio["regalo"]) {
                                                        echo "Si";
                                                    } else {
                                                        echo "No";
                                                    } ?>
                                                </td>
                                                <td>
                                                    <?php if ($dettaglio["piantare"]) {
                                                        echo "Si";
                                                    } else {
                                                        echo "No";
                                                    } ?>
                                                </td>
                                                <td>
                                                    <?php if (!$dettaglio["piantare"]) :
                                                        echo $dettaglio["via"] . ", " .  $dettaglio["numero"] . ", " . $dettaglio["città"];
                                                    else : ?>
                                                        <div class="row">
                                                            <label class="col-form-label d-none" for="place">Luogo</label>
                                                            <select class="form-control col-lg-10 p-0" id="place" name="place<?php echo $dettaglio["codice"]; ?>">
                                                                <?php foreach ($templateParams["nome_luoghi"] as $nome_luogo) : ?>
                                                                    <option value="<?php echo $nome_luogo["codice"]; ?>" selected="selected"><?php echo $nome_luogo["nome"] ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                            <div class="dropdown col-lg-2 p-0">
                                                                <a class="btn dropdown-toggle pr-0" href="#" role="button" id="dropdownoption" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                                                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                                                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                                                </svg>
                                                                </a>
                                                                <ul class="dropdown-menu p-2" aria-labelledby="dropdownoption">
                                                                    <li class="row m-0 py-1">
                                                                        <a class="dropdown-item col-9 col-md-8 col-lg-8" href="form-posizione.php">Aggiungi luogo</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?php if (!$dettaglio["piantare"]) :
                                                        echo $dettaglio["nome_destinatario"] . " " .  $dettaglio["cognome_destinatario"];
                                                    else :
                                                        echo "--------------------";
                                                    endif; ?>
                                                </td>
                                                <td>
                                                    <?php if ($dettaglio["regalo"]) :
                                                        echo $dettaglio["email_regalo"];
                                                    else :
                                                        echo "--------------------";
                                                    endif; ?>
                                                    <input type="hidden" name="order_number" value="<?php echo $order_number; ?>" />
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </table>
                                    </div>
                                </section>
                                <footer class="mb-5">
                                    <div class="row">
                                        <div class="col-2 col-lg-2"></div>
                                        <h4 class="col-4 col-lg-4 mt-1 text-center">Totale carrello: <?php echo $templateParams["ordine"][0]["prezzo_totale"]; ?> €</h4>
                                        <input type="submit" name="processa" class="col-4 col-lg-4 p-1 my-2 rounded-pill" value="Processa ordine" />
                                        <div class="col-2 col-lg-2"></div>
                                    </div>
                                </footer>
                            </div>
                        </article>
                        <div class="col-1 col-lg-2"></div>
                    </div>
                </form>
            <?php endforeach; ?>
        <?php else : ?>
            <div class="col-12 text-center">
                <p>Non hai ricevuto ordini</p>
            </div>
        <?php endif; ?>

    </div>
</div>