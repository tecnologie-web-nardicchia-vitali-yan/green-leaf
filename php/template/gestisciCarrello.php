<?php if (isset($templateParams["nome"])) :
    $ordinedettaglio = $templateParams["dettaglioordine"]; ?>
    <h2 class="text-center mb-5">Modifica</h2>
<?php endif; ?>

<div class="container-fluid">
    <div class="row d-flex align-items-center">
        <div class="col-1 col-lg-1"></div>

        <article class="col-10 col-lg-10">

            <form action="#" method="POST">
                <div class="row text-center mb-5">
                    <div class="col-1 col-lg-1"></div>
                    <header class="col-10 col-lg-10">
                        <h3><?php echo $ordinedettaglio["nome_albero"]; ?></h3>
                    </header>
                    <div class="col-1 col-lg-1"></div>
                </div>

                <section class="row">
                    <div class="col-lg-1"></div>

                    <div class="col-12 col-lg-5 text-center">
                        <img class="img-fluid" src="<?php echo UPLOAD_DIR . $ordinedettaglio["nome_immagine"]; ?>" alt="<?php echo $ordinedettaglio["nome_albero"]; ?>" />
                    </div>

                    <div class="col-12 col-lg-5">


                        <div class="form-group row">
                            <label class="col-6" for="quantità">Quantità: </label>
                            <input class="col-5" id="quantità" type="number" name="quantità" min="1" value="<?php echo $ordinedettaglio["quantità"] ?>" />
                            <div class="col-1"></div>
                        </div>

                        <div class="form-group row">
                            <p class="col-6">Prezzo unitario: </p>
                            <p class="col-6"><?php echo $ordinedettaglio["costo"] ?> €</p>
                        </div>

                        <div class="form-group row form-check">
                            <input class="mr-2" id="pianta" type="checkbox" name="piantare" <?php if ($ordinedettaglio["piantare"]) echo 'checked="checked"' ?> />
                            <label for="pianta"> Pianta l'albero!</label><br/>
                        </div>

                        <div class="form-group row form-check">
                            <input class="mr-2" id="regala" type="checkbox" name="regala" <?php if ($ordinedettaglio["regalo"]) echo 'checked="checked"' ?> />
                            <label for="regala"> Regala l'albero!</label><br/>
                            
                        </div>

                        <div class="form-group row" id="regala-email">
                            <label for="email" class="col-12 col-form-label">Inserisci l'email dell'amico da regalare l'albero piantato!</label>
                            <input type="email" id="email" class="form-control col-11 ml-3" name="email" placeholder="E-mail" value="<?php if (isset($ordinedettaglio["email_regalo"])) { echo $ordinedettaglio["email_regalo"]; }?>"/>
                            <div class="col-1"></div>
                        </div>

                        <?php if (isset($templateParams["indirizzospedizione"][$ordinedettaglio["codice_spedizione"]])) :
                            $indirizzo = $templateParams["indirizzospedizione"][$ordinedettaglio["codice_spedizione"]][0]; ?>
                            <div class="col-12">
                                <p>Indirizzo di spedizione:</p>
                                <p><?php echo $indirizzo["nome_destinatario"] . " " . $indirizzo["cognome_destinatario"] ?></p>
                                <p>Via <?php echo $indirizzo["via"] . " " . $indirizzo["numero"] ?></p>
                                <p><?php echo $indirizzo["città"] . " " . $indirizzo["provincia"] . " " . $indirizzo["CAP"] ?></p>
                                <input type="hidden" name="indirizzo" value="<?php echo $indirizzo["codice"] ?> €" />
                            </div>
                        <?php endif; ?>

                        <input type="hidden" name="prezzo" value="<?php echo $ordinedettaglio["costo"] ?> €" />
                        <input type="hidden" name="nome" value="<?php echo $ordinedettaglio["nome_albero"] ?>" />

                        <div class="row">
                            <button type="submit" name="update" class="nav-link text-center col-4 rounded-pill ml-3">Aggiorna</button>
                            <div class="col-2"></div>
                            <a class="col-4 nav-link text-center rounded-pill" href="carrello.php?action=1&id=<?php echo $ordinedettaglio["codice"]; ?>">Elimina</a>

                        </div>

                    </div>

                    <div class="col-lg-1"></div>
                </section>
            </form>
        </article>


        <div class="col-1 col-lg-1"></div>
    </div>
</div>