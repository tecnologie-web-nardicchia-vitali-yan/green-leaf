<?php
$utente = $templateParams["datipersonali"][0];
?>
<form action="processa-datipersonali.php" class="p-5" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend class="text-left my-5">Dati persoanli</legend>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Nome:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $utente["nome"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Cognome:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="cognome" name="cognome" value="<?php echo $utente["cognome"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-2 col-form-label">Telefono:</label>
      <div class="col-sm-10 col-md-6 col-lg-5">
        <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $utente["telefono"]; ?>">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-10 text-right">
        <input type="submit" value="Modifica dati persoanli" class="btn rounded-pill">
      </div>
    </div>
  </fieldset>
</form>