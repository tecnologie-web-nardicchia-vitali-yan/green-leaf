    <div class="container-fluid">

        <div class="row d-flex align-items-top text-center">
            <div class="col-1 col-lg-2"></div>

            <div class="col-10 col-lg-8">
                <section>
                    <header>
                        <h2>Il pagamento è stato effettuato con successo!</h2>
                    </header>
                    <p>Ora puoi visualizzare lo stato del tuo ordine nella sezione</p>
                    <a href="visualizzazione-ordini.php">I miei ordini
                        <img src="<?php echo UPLOAD_DIR . "/albero2.png"; ?>" alt="" />
                    </a>
                </section>
            </div>

            <div class="col-1 col-lg-2"></div>

        </div>
    </div>