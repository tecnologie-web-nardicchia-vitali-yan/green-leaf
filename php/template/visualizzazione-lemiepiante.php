<?php
if (count($templateParams["alberi_piantati"]) == 0 || $templateParams["alberi_regalati"] == 0) :
?>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3 text-center">Alberi piantati</h2>
                <h3 class="text-center text-danger">Non hai ancora piantato nessun albero</h3>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif;
if (count($templateParams["alberi_piantati"]) != 0) : ?>
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-lg-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Alberi piantati</h2>
                <?php foreach ($templateParams["alberi_piantati"] as $albero) : ?>
                    <table class="mt-4 table">
                        <tr>
                            <th>Immagine</th>
                            <th>Nome albero</th>
                            <th>Data piantagione</th>
                            <th>Luogo piantagione</th>
                        </tr>
                        <tr>
                            <td class="w-25"><img class="img-fluid" src="<?php echo UPLOAD_DIR . $albero["nome"] ?>" alt="#"></td>
                            <td><?php echo $albero["nome_albero"] ?></td>
                            <td><?php echo $albero["data_piantagione"] ?></td>
                            <td>
                                <div class="google-maps">
                                    <iframe src="<?php echo $albero["luogo"]; ?>" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="container text-right">
                        <ul class="mt-2 mb-5 nav">
                            <li class="col-sm-12 col-md-6 col-lg-6 dettaglio"></li>
                            <li class="col-sm-12 col-md-6 col-lg-6 dettaglio"><a class="p-2 nav-link text-center rounded-pill" href="dettaglio-crescitautente.php?codice=<?php echo $albero["codice"] ?>">Mostra dettagli di crescita</a></li>
                        </ul>
                    </div>
                <?php endforeach; ?>
            </section>
            <div class="col-md-1 col-lg-1"></div>
        </div>
    </div>
<?php endif;
if (count($templateParams["alberi_regalati"]) != 0) : ?>
    <div class="container">
        <div class="row">
            <div class="col-1"></div>
            <section class="col-10">
                <h2 class="mt-4 mb-3">Alberi che ti sono stati regalati</h2>
                <?php foreach ($templateParams["alberi_regalati"] as $albero) : ?>
                    <table class="mt-4 table">
                        <tr>
                            <th>Immagine:</th>
                            <th>Nome albero:</th>
                            <th>Data piantagione:</th>
                            <th>Da parte di:</th>
                            <th>Luogo piantagione</th>
                        </tr>
                        <tr>
                            <td class="w-25"><img class="img-fluid" src="<?php echo UPLOAD_DIR . $albero["nome"] ?>" alt="#"></td>
                            <td><?php echo $albero["nome_albero"] ?></td>
                            <td><?php echo $albero["data_piantagione"] ?></td>
                            <td><?php echo $albero["email_utente"] ?></td>
                            <td>
                                <div class="google-maps">
                                    <iframe src="<?php echo $albero["luogo"]; ?>" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="container text-right">
                        <ul class="mt-2 mb-5 nav">
                            <li class="col-sm-12 col-md-6 col-lg-6 dettaglio"></li>
                            <li class="col-sm-12 col-md-6 col-lg-6 dettaglio"><a class="p-2 nav-link text-center rounded-pill" href="dettaglio-crescitautente.php?codice=<?php echo $albero["codice"] ?>">Mostra dettagli di crescita</a></li>
                        </ul>
                    </div>
                <?php endforeach; ?>
            </section>
            <div class="col-1"></div>
        </div>
    </div>
<?php endif; ?>