<div class="container-fluid">
    <article class="row d-flex align-items-center mt-5">
  
        <div class="col-12 col-lg-6 text-center ">
            <img src="../img/PiantaInfo.png" alt="Piantare un albero" class="img-fluid">
        </div>

        <div class="col-12 col-lg-6">
            <h2 class="mt-5 mb-3">Come piantare un albero?</h2>
            <p>
                Se decidi di piantare un albero, un contandino lo pianterà e tu potrai seguire la sua crescita passo passo a distanza. <br/>
                Aiuta a piantare alberi in zone che ne hanno necessità causa: <br/>
                <ul>
                    <li><p>disboscamento</p></li>
                    <li><p>incendi</p></li>
                    <li><p>calamità naturali</p></li>
                </ul>
            </p>
        </div>

    </article>



    <div class="row d-flex align-items-top text-center mt-5">
        <div class="col-lg-1"></div>

        <article class="col-lg-4">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <header>
                        <div>
                            <img src="../img/AlberoSfondo.png" alt="Albero" />
                        </div>

                        <h3>Scegli</h3>
                    </header>
                    <section>
                        <p>Scegli fra tante piante a disposizione disponibili nell'area alberi</p>
                    </section>
                </div>

                <div class="col-12 col-lg-6">
                    <footer>
                        <div>
                            <img src="../img/Arrow.png" alt="Freccia" />
                        </div>
                    </footer>
                </div>
            </div>
        </article>

        <article class="col-lg-4">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <header>
                        <div>
                            <img src="../img/Paga.png" alt="Acquista" />
                        </div>

                        <h3>Acquista</h3>
                    </header>
                    <section>
                        <p>Scegli l’opzione pianta al momento dell’acquisto<br/></p>
                    </section>
                </div>
                <div class="col-12 col-lg-6">
                    <footer>
                        <div>
                            <img src="../img/Arrow.png" alt="Freccia" />
                        </div>
                    </footer>
                </div>
            </div>
        </article>

        <article class="col-12 col-lg-2">

            <header>
                <div>
                    <img src="../img/PiantaSfondo.png" alt="Pianta" />
                </div>

                <h3>Pianta</h3>
            </header>
            <section>
                <p>Potrai vedere la crescita degli alberi che hai piantato dal tuo profilo</p>
            </section>

        </article>

        <div class="col-lg-1"></div>

    </div>
</div>