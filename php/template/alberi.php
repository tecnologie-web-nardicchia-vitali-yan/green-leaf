<?php if (isset($templateParams["nome"])) : ?>
    <h2 class="text-center mb-4">Alberi</h2>
<?php endif; ?>
<div class="container-fluid">
    <div class="row">
        <?php foreach ($templateParams["randomTrees"] as $randomTree) : ?>
            <div class="col-12 col-md-6 col-lg-4 px-2">
                <a href="alberoDescrizione.php?nome=<?php echo $randomTree["nome_albero"]; ?>">
                    <article class="m-1 p-3 row">
                        <div class="col-6 col-lg-6 d-flex align-items-center">
                            <header>
                                <h3><?php echo $randomTree["nome_albero"]; ?></h3>
                                <?php echo $randomTree["costo"] . "€"; ?>
                            </header>
                        </div>
                        <div class="col-6 col-lg-6 d-flex align-items-right">
                            <section>
                                <img src="<?php echo UPLOAD_DIR . $randomTree["nome_immagine"]; ?>" alt="" />
                            </section>
                        </div>
                    </article>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>