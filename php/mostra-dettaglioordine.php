<?php
require_once 'bootstrap.php';



if(isUserLoggedIn() && isset($_GET["numero_ordine"])){
    $result = $dbh->checkIfOrderReferToUser($_GET["numero_ordine"], $_SESSION["email"]);

    if(count($result) != 0){
        $templateParams["titolo"] = "Dettaglio ordine - Admin";
        $templateParams["nome"] = "mostra-dettaglioordine.php";
        $templateParams["stile"] = "style_gestisci_dati.css";
        $templateParams["dettagli_ordine_spediti"] = $dbh->getDettagliOrdineWithDatiSpedizione($_GET["numero_ordine"]);
        $templateParams["dettagli_ordine_piantare"] = $dbh->getDettagliOrdineWithOutDatiSpedizione($_GET["numero_ordine"]);
    
    }else{
        header("location: visualizzazione-ordini.php");
    }

}else{
    header("location: login.php");
}


require 'template/base.php';
?>