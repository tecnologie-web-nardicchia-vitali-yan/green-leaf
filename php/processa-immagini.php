<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !isset($_GET["action"]) || !isset($_GET["nome_albero"]) || $dbh->isUserRole($_SESSION["email"])) {
    header("location: login.php");
}

if ($_GET["action"] == 1) {
    //Inserisco
    $nome_albero = $_POST["nome_albero"];

    list($result, $nome_immagine) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);
    $nome = basename($_FILES["immagine"]);
    if ($result) {
        $codice = $dbh->insertDatiImmagine($nome_immagine);

        if (!$codice) {
            $msg = "Errore inserimento immagine!";
        } else {
            $codice = $dbh->insertImmagineAlberoForeignKey($nome_immagine, $nome_albero);

            if ($codice) {
                $msg = "Inserimento completato correttamente!";
            } else {
                $dbh->deleteImmagine($nome_immagine); //toglie l'immagine inserita precedentemente
                $msg = "Errore in inserimento!";
            }
        }
    } else {
        $msg = "Errore caricamento immagine!";
    }

    header("location: modifica-immagini.php?nome_albero=" . $nome_albero . "&formmsg=" . $msg);
}

if ($_GET["action"] == 3) {
    //cancello
    $nome_albero = $_GET["nome_albero"];
    $nome_immagine = $_GET["nome_immagine"];

    echo $nome_albero;

    $risultato = $dbh->countImageOfaTree($nome_albero);

    if ($risultato[0]["numero_immagini"] > 1) {

        $codice = $dbh->deleteDatiAlberoImmagine($nome_albero, $nome_immagine);

        if ($codice) {
            $codice = $dbh->deleteImmagine($nome_immagine);

            if ($codice) {
                $msg = "Cancellazione completata correttamente!";
            } else {
                $msg = "Errore nella cancellazione!";
            }
        } else {
            $msg = "Errore nella cancellazione dell'immagine!";
        }
    }else{
        $msg = "Un albero deve possedere almeno un'immagine!" ;
    }


    header("location: modifica-immagini.php?nome_albero=" . $nome_albero . "&formmsg=" . $msg);
}
?>