<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !isset($_POST["action"])) {
    header("location: login.php");
}

if ($_POST["action"] == 1 || $_POST["action"] == 5) {
    //Inserisco
    $numero = $_POST["numero"];
    $tipo_carta = $_POST["tipo_carta"];
    $cvv = $_POST["CVV"];
    $nome_titolare = $_POST["nome_titolare"];
    $cognome_titolare = $_POST["cognome_titolare"];
    $data_scadenza = $_POST["data_scadenza"];

    $email = $_SESSION['email'];

    $codice = $dbh->insertDatiPagamento($numero, $tipo_carta, $cvv, $nome_titolare, $cognome_titolare, $data_scadenza, $email);
    if ($codice) {
        $msg = "Inserimento completato correttamente! ";
    } else {
        $msg = "Errore in inserimento! ";
    }
    if ($_POST["action"] == 1) {
        header("location: visualizzazione-dati.php?formmsg=" . $msg);
    } else {
        header("location: pagamento.php?action=5&formmsg=" . $msg);
    }
}

if ($_POST["action"] == 2) {
    //modifico
    $numero = $_POST["numero"];
    $tipo_carta = $_POST["tipo_carta"];
    $cvv = $_POST["CVV"];
    $nome_titolare = $_POST["nome_titolare"];
    $cognome_titolare = $_POST["cognome_titolare"];
    $data_scadenza  = $_POST["data_scadenza"];

    $email = $_SESSION["email"];

    $codice = $dbh->updateDatiPagamento($numero, $tipo_carta, $cvv, $nome_titolare, $cognome_titolare, $data_scadenza, $email);

    if ($codice) {
        $msg = "Modifica completata correttamente!";
    } else {
        $msg = "Errore di modifica!";
    }


    header("location: visualizzazione-dati.php?formmsg=" . $msg);
}

if ($_POST["action"] == 3) {
    //cancello
    $numero = $_POST["numero"];
    $email =  $_SESSION["email"];

    $result = $dbh->checkifExistOrderWithCard($numero);

    if ($result != null) {
        foreach (array_slice($result, 0) as $ordine) {
            $codice = $dbh->setNullCartainOrdine($ordine["numero"]);
            if (!$codice) {
                $msg = "Errore nella cancellazione ordini collegati a questa carta!";
                break;
            }
        }
    }

    $codice = $dbh->deleteDatiPagamentoOfUtente($email, $numero);

    if ($codice) {
        $msg = "Cancellazione completata correttamente!";
    } else {
        $msg = "Errore nella cancellazione!";
    }

    header("location: visualizzazione-dati.php?formmsg=" . $msg);
}
?>