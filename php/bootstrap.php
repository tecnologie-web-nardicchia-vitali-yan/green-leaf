<?php
session_start();
define("UPLOAD_DIR", "../img/");
define("UPLOAD_STYLE", "./css/");
define("UPLOAD_JS", "../js/");
require_once("utils/functions.php");
require_once("db/database.php");
$dbh = new DatabaseHelper("localhost", "root", "", "greenleaf", 3306);

if (isUserLoggedIn()) {
    if ($dbh->isUserRole($_SESSION['email'])) {
        $templateParams["menu"] = "userMenu.php";
    } else {
        $templateParams["menu"] = "sellerMenu.php";
    }
    $templateParams["notifiche"] = $dbh->getUserNotifications($_SESSION['email']);
    $notifiche = count($templateParams["notifiche"]);
} else {
    $templateParams["menu"] = "notUserMenu.php";
}

?>