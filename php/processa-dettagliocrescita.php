<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !isset($_POST["action"]) || $dbh->isUserRole($_SESSION["email"])) {
    header("location: login.php");
}

if ($_POST["action"] == 1) {
    //Inserisco
    $data_rilevazione = $_POST["data_rilevazione"];
    $altezza = $_POST["altezza"];
    $albero_piantato = $_POST["albero_piantato"];

    $dettaglio_crescita = $dbh->insertDettaglioCrescita($data_rilevazione, $altezza, $albero_piantato);

    list($result, $nome_immagine) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);

    if ($result) {
        $codice = $dbh->insertImmagineDettaglioCrescita($nome_immagine, $dettaglio_crescita);

        if (!$codice) {
            $dbh->deleteDettaglioCrescita($dettaglio_crescita); // elimina dettaglio crescita inserito precedentemente
            $msg = "Errore inserimento immagine!";
        } else {
            $msg = "Inserimento effettuato correttamente!";
        }
    } else {
        $msg = "Errore in inserimento!" . $nome_immagine;
    }

    $templateParams["albero_piantato"] = $dbh->getTreePlantedByCode($_POST["albero_piantato"])[0];
    $email_utente = $templateParams["albero_piantato"]["email_utente"];
    $dbh->addNotification(6, $email_utente, date("Y-m-d"));
    header("location: dettaglio-crescitavenditore.php?formmsg=" . $msg . "&codice=" . $albero_piantato);
}

if ($_POST["action"] == 2) {
    //modifico
    $data_rilevazione = $_POST["data_rilevazione"];
    $altezza = $_POST["altezza"];
    $codice = $_POST["codice"];
    $albero_piantato = $_POST["albero_piantato"];

    $codice = $dbh->updateDettaglioCrescita($codice, $data_rilevazione, $altezza);

    if ($codice) {
        $msg = "Modifica completata correttamente!";
    } else {
        $msg = "Errore di modifica!";
    }


    header("location: dettaglio-crescitavenditore.php?formmsg=" . $msg . "&codice=" . $albero_piantato);
}

if ($_POST["action"] == 3) {
    //cancello
    $dettaglio_crescita = $_POST["codice"];
    $albero_piantato = $_POST["albero_piantato"];

    $codice = $dbh->deleteImmagineCrescita($dettaglio_crescita);

    if ($codice) {
        $codice = $dbh->deleteDettaglioCrescita($dettaglio_crescita);

        if ($codice) {
            $msg = "Cancellazione completata corretamente!";
        } else {
            $msg = "Errore nella cancellazione cancellazione";
        }
    } else {
        $msg = "Errore nella cancellazione dell'immagine!";
    }

    header("location: dettaglio-crescitavenditore.php?formmsg=" . $msg . "&codice=" . $albero_piantato);
}

if ($_POST["action"] == 4) {
    //cancello
    $dettaglio_crescita = $_POST["codice"];
    $albero_piantato = $_POST["albero_piantato"];

    list($result, $nome_immagine) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);

    if ($result) {
        $codice = $dbh->deleteImmagineCrescita($dettaglio_crescita);

        if ($codice) {
            $codice = $dbh->insertImmagineDettaglioCrescita($nome_immagine, $dettaglio_crescita);
        }
    }
}
?>