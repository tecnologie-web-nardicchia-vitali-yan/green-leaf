<?php
class DatabaseHelper
{
    private $db;

    public function __construct($servername, $username, $dbname, $port)
    {
        $this->db = new mysqli($servername, $username, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    public function getRandomTrees()
    {
        $stmt = $this->db->prepare("SELECT nome_albero, costo, nome_immagine 
        FROM albero A, immagine I, immagine_albero IA 
        WHERE A.nome = IA.nome_albero AND I.nome = IA.nome_immagine 
        GROUP BY nome_albero
        ORDER BY RAND()");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //Avere la descrizione dell'albero
    public function getTreeByName($name)
    {
        $query = "SELECT nome, descrizione, costo, consumoCO2 FROM albero WHERE nome=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $name);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllTrees()
    {
        $query = "SELECT * FROM albero";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //Avere tutte le immagini dellalbero
    public function getTreeImgByName($name)
    {
        $stmt = $this->db->prepare("SELECT nome_immagine FROM immagine_albero WHERE nome_albero=?");
        $stmt->bind_param('s', $name);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllTreesWithImages()
    {
        $stmt = $this->db->prepare("SELECT nome_albero, descrizione, costo, consumoCO2, GROUP_CONCAT(nome_immagine) AS immagini
                                    FROM immagine i, immagine_albero ia, albero a
                                    WHERE i.nome = ia.nome_immagine AND a.nome = ia.nome_albero
                                    AND dettaglio_crescita IS NULL
                                    GROUP BY nome_albero");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAlberiPiantati()
    {
        $stmt = $this->db->prepare("SELECT * FROM alberopiantato");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDettaglioCrescitaAlbero($codice)
    {
        $stmt = $this->db->prepare("SELECT codice FROM dettagliocrescita WHERE albero_piantato=?");
        $stmt->bind_param('i', $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDettagliCrescitaWithImagesAlbero($codice)
    {
        $stmt = $this->db->prepare("SELECT d.codice, d.data_rilevazione, d.altezza, d.albero_piantato, i.nome
                                    FROM dettagliocrescita d, immagine i 
                                    WHERE i.dettaglio_crescita = d.codice AND d.albero_piantato=?");
        $stmt->bind_param('i', $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDettaglioCrescita($codice)
    {
        $stmt = $this->db->prepare("SELECT * FROM dettagliocrescita WHERE codice=?");
        $stmt->bind_param('i', $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getImmagineDettaglioCrescita($codice)
    {
        $stmt = $this->db->prepare("SELECT nome FROM immagine WHERE dettaglio_crescita=?");
        $stmt->bind_param('i', $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getDatiSpedizioneByCodiceAndEmail($codice, $email)
    {
        $query = "SELECT * FROM datispedizione WHERE email=? AND codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $email, $codice);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDatiPagamentoByNumeroAndEmail($numero, $email)
    {
        $query = "SELECT * FROM modalitàdipagamento WHERE email=? AND numero = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ss", $email, $numero);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDatiSpedizioneByUserEmail($email)
    {
        $query = "SELECT * FROM datispedizione d, utente u WHERE u.email = d.email AND u.email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDatiPagamentoByUserEmail($email)
    {
        $query = "SELECT * FROM modalitàdipagamento m, utente u WHERE u.email = m.email AND u.email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDatiPersonaliByUserEmail($email){
        $query = "SELECT * FROM utente WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAlberiPiantatiNonRegalatiByUserEmail($email)
    {
        $query = "SELECT ap.codice, ap.data_piantagione, ap.nome_albero, i.nome, p.luogo
                    FROM alberopiantato ap, immagine i, immagine_albero ia, albero a, posizione p
                    WHERE ap.nome_albero = a.nome AND a.nome = ia.nome_albero AND i.nome = ia.nome_immagine AND p.codice = ap.posizione AND email_utente = ?
                    AND email_regalo_iscritto IS NULL AND email_regalo_noniscritto IS NULL
                    AND i.nome = (SELECT im.nome
                                    FROM immagine im, albero al, immagine_albero imal
                                    WHERE im.nome = imal.nome_immagine AND al.nome = imal.nome_albero AND al.nome = ap.nome_albero
                                    LIMIT 1)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAlberiPiantatiRegalatiByUserEmail($email)
    {
        $query = "SELECT ap.codice, ap.data_piantagione, ap.nome_albero, ap.email_utente, i.nome,  p.luogo 
                    FROM alberopiantato ap, immagine i, immagine_albero ia, albero a, posizione p
                    WHERE ap.nome_albero = a.nome AND a.nome = ia.nome_albero AND i.nome = ia.nome_immagine AND p.codice = ap.posizione 
                    AND (email_regalo_iscritto= ? OR email_regalo_noniscritto= ?)
                    AND i.nome = (SELECT im.nome
                                    FROM immagine im, albero al, immagine_albero imal
                                    WHERE im.nome = imal.nome_immagine AND al.nome = imal.nome_albero AND al.nome = ap.nome_albero
                                    LIMIT 1)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ss", $email, $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getLastDettaglioCrescita($codice)
    {
        $stmt = $this->db->prepare("SELECT d.codice, d.data_rilevazione, d.altezza, d.albero_piantato, i.nome
                                    FROM dettagliocrescita d, immagine i 
                                    WHERE i.dettaglio_crescita = d.codice AND d.albero_piantato=?
                                    LIMIT 1");
        $stmt->bind_param('i', $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrdiniPagatiByUserEmail($email)
    {
        $stmt = $this->db->prepare("SELECT *
                                    FROM ordine
                                    WHERE pagato = 1 AND email_utente = ?");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkIfOrderReferToUser($numero_ordine, $email)
    {
        $stmt = $this->db->prepare("SELECT *
                                    FROM ordine
                                    WHERE pagato = 1 AND email_utente = ? AND numero = ?");
        $stmt->bind_param('si', $email, $numero_ordine);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDettagliOrdineWithDatiSpedizione($numero_ordine)
    {
        $stmt = $this->db->prepare("SELECT d.nome_albero, d.quantità, d.regalo, d.piantare, d.codice_spedizione, s.via, s.numero, s.città, s.nome_destinatario,
                                    s.cognome_destinatario
                                    FROM dettaglioordine d, datispedizione s
                                    WHERE d.codice_spedizione = s.codice AND d.numero_ordine = ?");
        $stmt->bind_param('i', $numero_ordine);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getDettagliOrdineWithOutDatiSpedizione($numero_ordine)
    {
        $stmt = $this->db->prepare("SELECT d.nome_albero, d.quantità, d.regalo, d.piantare
                                    FROM dettaglioordine d
                                    WHERE d.codice_spedizione IS NULL AND d.numero_ordine = ?");
        $stmt->bind_param('i', $numero_ordine);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrderDetails($numero_ordine)
    {
        $stmt = $this->db->prepare("SELECT d.codice, d.nome_albero, d.quantità, d.regalo, d.piantare, d.email_regalo, s.via, s.numero, s.città, s.nome_destinatario, s.cognome_destinatario
                                    FROM dettaglioordine d LEFT OUTER JOIN datispedizione s ON d.codice_spedizione = s.codice
                                    WHERE numero_ordine = ?");
        $stmt->bind_param('i', $numero_ordine);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertDatiSpedizione($via, $numero, $città, $provincia, $CAP, $nome_destinatario, $cognome_destinatario, $email)
    {
        $query = "INSERT INTO datispedizione (codice, via, numero, città, provincia, CAP, nome_destinatario, cognome_destinatario, email) 
                    VALUES (NULL,?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ssssssss", $via, $numero, $città, $provincia, $CAP, $nome_destinatario, $cognome_destinatario, $email);

        return $stmt->execute();
    }

    public function addNewOrder($isPaid, $isShipped, $totalPrice, $email, $cardNumber)
    {
        $query = "INSERT INTO ordine (pagato, spedito, prezzo_totale, email_utente, numero_carta) VALUES (?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iidss', $isPaid, $isShipped, $totalPrice, $email, $cardNumber);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertDatiPagamento($numero, $tipo_carta, $CVV, $nome_titolare, $cognome_titolare, $data_scadenza, $email)
    {
        $query = "INSERT INTO modalitàdipagamento (numero, tipo_carta, CVV, nome_titolare, cognome_titolare, data_scadenza, email) 
                    VALUES (?, ?, ?, ?, ? ,?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssss', $numero, $tipo_carta, $CVV, $nome_titolare, $cognome_titolare, $data_scadenza, $email);

        return $stmt->execute();
    }

    public function insertDatiAlbero($nome, $descrizione, $costo, $consumoCO2)
    {
        $query = "INSERT INTO albero (nome, descrizione, costo, consumoCO2) 
                    VALUES (?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssdi', $nome, $descrizione, $costo, $consumoCO2);

        return $stmt->execute();
    }

    public function insertDatiImmagine($nome)
    {
        $query = "INSERT INTO immagine (nome, dettaglio_crescita) 
                    VALUES (?, NULL)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nome);

        return $stmt->execute();
    }

    public function insertImmagineAlberoForeignKey($nome_immagine, $nome_albero)
    {
        $query = "INSERT INTO immagine_albero (nome_immagine, nome_albero) 
                    VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $nome_immagine, $nome_albero);

        return $stmt->execute();
    }

    public function insertImmagineDettaglioCrescita($nome_immagine, $dettaglio_crescita)
    {
        $query = "INSERT INTO immagine (nome, dettaglio_crescita) 
                    VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $nome_immagine, $dettaglio_crescita);

        return $stmt->execute();
    }

    public function insertDettaglioCrescita($data_rilevazione, $altezza, $albero_piantato)
    {
        $query = "INSERT INTO dettagliocrescita (data_rilevazione, altezza, albero_piantato) 
                    VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sdi', $data_rilevazione, $altezza, $albero_piantato);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function addTreePlanted($date, $email_utente, $nome_albero, $email_regalo_noniscritto, $email_regalo_iscritto, $place)
    {	
        $query = "INSERT INTO alberopiantato (data_piantagione, email_utente, nome_albero, email_regalo_noniscritto, email_regalo_iscritto, posizione)
                    VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssss', $date, $email_utente, $nome_albero, $email_regalo_noniscritto, $email_regalo_iscritto, $place);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function addOrderDetails($treeName, $quantity, $isGift, $isToPlant, $orderNumber, $shippingCode, $email)
    {
        $query = "INSERT INTO dettaglioordine (nome_albero, quantità, regalo, piantare, numero_ordine, codice_spedizione, email_regalo) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('siiiiis', $treeName, $quantity, $isGift, $isToPlant, $orderNumber, $shippingCode, $email);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function checkIsPresentOrderDetails($treeName, $order_number, $isToPlant, $isGift, $email)
    {
        $query = "SELECT nome_albero, quantità, regalo, piantare, codice_spedizione, codice, email_regalo 
        FROM dettaglioordine WHERE nome_albero = ? AND numero_ordine=? AND piantare=? AND regalo=? AND email_regalo=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('siiis', $treeName, $order_number, $isToPlant, $isGift, $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getShippingAddressByCode($codice)
    {
        $stmt = $this->db->prepare("SELECT via, numero, città, provincia, CAP, nome_destinatario, cognome_destinatario 
        FROM datispedizione WHERE codice=?");
        $stmt->bind_param('i', $codice);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteOrderDetails($codice)
    {
        $query = "DELETE FROM dettaglioordine WHERE codice=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $codice);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function updateOrderDetails($codice, $treeName, $quantity, $isGift, $isToPlant, $shippingCode, $email)
    {
        $query = "UPDATE `dettaglioordine` 
        SET `nome_albero`=?,`quantità`=?,`regalo`=?,`piantare`=?,`codice_spedizione`=?,`email_regalo`=? 
        WHERE codice=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('siiiisi', $treeName, $quantity, $isGift, $isToPlant, $shippingCode, $email, $codice);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function updateOrder($isPaid, $isShipped, $totalPrice, $cardNumber, $number)
    {
        $query = "UPDATE `ordine` 
        SET `pagato`=?,`spedito`=?,`prezzo_totale`=?,`numero_carta`=?
        WHERE numero=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iidsi', $isPaid, $isShipped, $totalPrice, $cardNumber, $number);
        $stmt->execute();

        return $stmt->insert_id;
    }

    //Selezionare l'ordine non pagato dato l'utente
    public function getNotPairOrderByUser($email)
    {
        $query = "SELECT numero, prezzo_totale FROM ordine WHERE email_utente = ? AND pagato=0";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function countImageOfaTree($nome_albero)
    {
        $query = "SELECT COUNT(nome_immagine) AS numero_immagini FROM immagine_albero WHERE nome_albero = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $nome_albero);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateDatiSpedizione($codice, $via, $numero, $citta, $provincia, $cap, $nome_dest, $cognome_dest, $email)
    {
        $query = "UPDATE datispedizione SET via = ?, numero = ?, città = ?, provincia = ?, CAP = ?, nome_destinatario = ?, cognome_destinatario = ? WHERE codice = ? AND email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssssis', $via, $numero, $citta, $provincia, $cap, $nome_dest, $cognome_dest, $codice, $email);

        return $stmt->execute();
    }

    public function updateDatiPagamento($numero, $tipo_carta, $CVV, $nome_titolare, $cognome_titolare, $data_scadenza, $email)
    {
        $query = "UPDATE modalitàdipagamento SET tipo_carta = ?, CVV = ?, nome_titolare = ?, cognome_titolare = ?, data_scadenza = ? WHERE numero = ? AND email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssss', $tipo_carta, $CVV, $nome_titolare, $cognome_titolare, $data_scadenza, $numero, $email);

        return $stmt->execute();
    }

    public function updateDatiAlbero($nome, $descrizione, $costo, $consumoCO2)
    {
        $query = "UPDATE albero SET  descrizione = ?, costo = ?, consumoCO2 = ? WHERE nome = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sdis', $descrizione, $costo, $consumoCO2, $nome);

        return $stmt->execute();
    }

    public function updateDettaglioCrescita($codice, $data_rilevazione, $altezza)
    {
        $query = "UPDATE dettagliocrescita SET  data_rilevazione = ?, altezza = ? WHERE codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sdi', $data_rilevazione, $altezza, $codice);

        return $stmt->execute();
    }

    public function updateDatiPersonali($nome, $cognome, $telefono, $email)
    {
        $query = "UPDATE utente SET  nome = ?, cognome = ?, telefono = ? WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssss', $nome, $cognome, $telefono, $email);

        return $stmt->execute();
    }

    public function setNullCartainOrdine($numero_ordine)
    {
        $query = "UPDATE ordine SET  numero_carta = NULL WHERE numero = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $numero_ordine);

        return $stmt->execute();
    }

    public function deleteDatiSpedizioneOfUtente($email, $codice)
    {
        $query = "DELETE FROM datispedizione WHERE email = ? AND codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $email, $codice);
        var_dump($stmt->error);

        return $stmt->execute();
    }

    public function deleteDatiPagamentoOfUtente($email, $numero)
    {
        $query = "DELETE FROM modalitàdipagamento WHERE email = ? AND numero = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $email, $numero);
        var_dump($stmt->error);

        return $stmt->execute();
    }

    public function deleteDatiAlberoImmagine($nome_albero, $nome_immagine)
    {
        $query = "DELETE FROM immagine_albero WHERE nome_albero = ? AND nome_immagine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $nome_albero, $nome_immagine);
        var_dump($stmt->error);

        return $stmt->execute();
    }

    public function getOrderById($id)
    {
        $query = "SELECT numero, pagato, spedito, prezzo_totale, email_utente, numero_carta FROM ordine WHERE numero = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //Selezionare tutti i dettagli ordine dato il numero dell'ordine
    public function getOrderDetailsByOrderNumber($order_number)
    {
        $query = "SELECT D.codice, D.nome_albero, D.quantità, D.regalo, D.piantare, D.codice_spedizione, A.costo, IA.nome_immagine, D.email_regalo
        FROM dettaglioordine D, immagine I, immagine_albero IA, albero A 
        WHERE D.numero_ordine = ? AND A.nome = D.nome_albero AND D.nome_albero = IA.nome_albero AND I.nome = IA.nome_immagine
        GROUP BY D.codice ORDER BY D.nome_albero";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $order_number);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrderDetailsByCode($codice)
    {
        $query = "SELECT D.codice, D.nome_albero, D.quantità, D.regalo, D.piantare, D.codice_spedizione, A.costo, IA.nome_immagine, D.email_regalo
        FROM dettaglioordine D, immagine I, immagine_albero IA, albero A 
        WHERE D.codice = ? AND A.nome = D.nome_albero AND D.nome_albero = IA.nome_albero AND I.nome = IA.nome_immagine
        GROUP BY D.codice ORDER BY D.nome_albero";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $codice);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSpecificOrdersByOrderNumber($order_number, $isPlant, $isGift)
    {
        $query = "SELECT D.codice, D.nome_albero, D.quantità, D.regalo, D.piantare, D.codice_spedizione, D.email_regalo, A.costo, IA.nome_immagine, D.email_regalo
        FROM dettaglioordine D, immagine I, immagine_albero IA, albero A 
        WHERE D.numero_ordine = ? AND A.nome = D.nome_albero AND D.nome_albero = IA.nome_albero 
        AND I.nome = IA.nome_immagine AND D.piantare=? AND D.regalo=?
        GROUP BY D.codice ORDER BY D.nome_albero";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iii', $order_number, $isPlant, $isGift);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPlantOrdersByOrderNumber($order_number)
    {
        $query = "SELECT D.codice, D.nome_albero, D.quantità, D.regalo, D.piantare, D.codice_spedizione, A.costo, IA.nome_immagine, D.email_regalo
        FROM dettaglioordine D, immagine I, immagine_albero IA, albero A 
        WHERE D.numero_ordine = ? AND A.nome = D.nome_albero AND D.nome_albero = IA.nome_albero 
        AND I.nome = IA.nome_immagine AND D.piantare=1
        GROUP BY D.codice ORDER BY D.nome_albero";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $order_number);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTreePlantedByCode($code)
    {
        $query = "SELECT * FROM alberopiantato WHERE codice=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $code);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrdersNotSend()
    {
        $query = "SELECT numero FROM ordine WHERE pagato=1 AND spedito=0";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function setOrderSent($order_number)
    {
        $query = "UPDATE ordine SET spedito=1 WHERE numero = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $order_number);

        return $stmt->execute();
    }

    public function getUserEmailByOrder($order_number)
    {
        $query = "SELECT email_utente FROM ordine WHERE numero = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $order_number);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function isUserRole($email)
    {
        $query = "SELECT ruolo FROM utente WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC)[0]["ruolo"];
        if ($result == "utente") {
            return true;
        }
        return false;
    }

    public function getSeller() {
        $query = 'SELECT email FROM utente WHERE ruolo="venditore"';
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($email, $password)
    {
        $query = "SELECT email, nome, cognome, telefono FROM utente WHERE email = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $email, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function registerUser($email, $password, $nome, $cognome, $telefono)
    {
        $query = "INSERT INTO utente (email, password, nome, cognome, telefono, ruolo) VALUES (?, ?, ?, ?, ?, 'utente')";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssss', $email, $password, $nome, $cognome, $telefono);
        return $stmt->execute();
    }

    public function insertUnregisterUser($email)
    {
        $query = "INSERT INTO utentenoniscritto (email) VALUES (?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        return $stmt->execute();
    }

    public function isUserRegister($email)
    {
        $query = "SELECT email FROM utente WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);

        if (count($result)!=0) {
            return true;
        }
        return false;
    }

    public function isUserUnregister($email)
    {
        $query = "SELECT email FROM utentenoniscritto WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);

        if (count($result)!=0) {
            return true;
        }
        return false;
    }

    public function deleteUserUnregister($email)
    {
        $query = "DELETE FROM utentenoniscritto WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);

        return $stmt->execute();
    }

    public function setUserRegisterTreePlanted($email)
    {
        $query = "UPDATE alberopiantato SET email_regalo_iscritto = ? WHERE email_regalo_noniscritto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $email, $email);

        return $stmt->execute();
    }

    public function setNullUserUnegisterTreePlanted($email)
    {
        $query = "UPDATE alberopiantato SET email_regalo_noniscritto = NULL WHERE email_regalo_iscritto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);

        return $stmt->execute();
    }

    public function addNotification($code, $email, $date)
    {
        $query = "INSERT INTO notifica_utente (codice, email, data) VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iss', $code, $email, $date);
        
        return $stmt->execute();
    }

    public function getUserNotifications($email)
    {
        $query = "SELECT  NU.numero, NU.data, N.codice, N.messaggio FROM notifica as N, notifica_utente as NU WHERE N.codice=NU.codice AND NU.email=? ORDER BY NU.data";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$email);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPlaces()
    {
        $query = "SELECT * FROM posizione";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function addPlace($name_place, $src)
    {
        $query = "INSERT INTO posizione (codice, nome, luogo) VALUES (NULL, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $name_place, $src);
        
        return $stmt->execute();
    }

    public function deleteNotification($numero)
    {
        $query = "DELETE FROM notifica_utente WHERE numero = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $numero);

        return $stmt->execute();
    }

    public function deleteImmagine($nome)
    {
        $query = "DELETE FROM immagine WHERE nome = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nome);
        var_dump($stmt->error);

        return $stmt->execute();
    }

    public function deleteImmagineCrescita($codice)
    {
        $query = "DELETE FROM immagine WHERE dettaglio_crescita = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $codice);
        var_dump($stmt->error);

        return $stmt->execute();
    }

    public function deleteDettaglioCrescita($codice)
    {
        $query = "DELETE FROM dettagliocrescita WHERE codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $codice);
        var_dump($stmt->error);

        return $stmt->execute();
    }

    public function deleteAlberoPiantato($codice)
    {
        $query = "DELETE FROM alberopiantato WHERE codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $codice);
        var_dump($stmt->error);

        return $stmt->execute();
    }

    public function deleteDatiAlbero($nome)
    {
        $query = "DELETE FROM albero WHERE nome = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nome);
        var_dump($stmt->error);

        return $stmt->execute();
    }

    public function checkifExistOrderWithCard($numero_carta)
    {
        $query = "SELECT numero FROM ordine WHERE numero_carta = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $numero_carta);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkifExistOrderWithSpedizione($codice_spedizione)
    {
        $query = "SELECT codice FROM dettaglioordine WHERE codice_spedizione = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $codice_spedizione);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
}
