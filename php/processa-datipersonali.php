<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn()) {
    header("location: login.php");
}


$nome = $_POST["nome"];
$cognome = $_POST["cognome"];
$telefono = $_POST["telefono"];

$email = $_SESSION['email'];

$codice = $dbh->updateDatiPersonali($nome, $cognome, $telefono, $email);
if ($codice) {
    $msg = "Modifica completata correttamente! ";
} else {
    $msg = "Errore di modifica! ";
}

header("location: visualizzazione-dati.php?formmsg=" . $msg);
?>
