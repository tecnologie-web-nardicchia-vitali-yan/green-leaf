<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || $dbh->isUserRole($_SESSION["email"]) || !isset($_GET["action"]) || ($_GET["action"] != 1 && $_GET["action"] != 2 && $_GET["action"] != 3)) {
    header("location: login.php");
}


if ($_GET["action"] != 1) {
    $risultato = $dbh->getDettaglioCrescita($_GET["codice"]);

    if (count($risultato) == 0) {
        $templateParams["dettaglio"] = null;
    } else {
        $templateParams["dettaglio"] = $risultato[0];
        $templateParams["immagine"] = $dbh->getImmagineDettaglioCrescita($_GET["codice"])[0];
    }
} else {
    $templateParams["dettaglio"] = getEmptyDettaglioCrescita();
}

if(isset($_GET["formmsg"])){
    $templateParams["formmsg"] = $_GET["formmsg"];
}

$templateParams["titolo"] = "Green Leaf - Gestisci dati spedizione";
$templateParams["nome"] = "form-dettagliocrescita.php";

$templateParams["js"] = "controllo_formdettagliocrescita.js";


$templateParams["stile"] = "style_gestisci_dati.css";

$templateParams["azione"] = $_GET["action"];
$templateParams["albero_piantato"] = $_GET["albero_piantato"];

require 'template/base.php';
?>