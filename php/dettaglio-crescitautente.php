<?php
require_once 'bootstrap.php';


// controllare che sia un venditore dopo MERGE

if(!isUserLoggedIn() || !isset($_GET["codice"])){
    header("location: login.php");
}else{
    $templateParams["titolo"] = "Dettagli Crescita - Admin";
    $templateParams["nome"] = "dettaglio-crescitautente.php";
    $templateParams["stile"] = "style_descrizioneAlbero.css";
    $templateParams["dettaglio_crescita"] = $dbh->getDettagliCrescitaWithImagesAlbero($_GET["codice"]);
    $templateParams["ultima_rilevazione"] = $dbh->getLastDettaglioCrescita($_GET["codice"]);
    $templateParams["codice_albero"] = $_GET["codice"];
    
}


require 'template/base.php';
?>