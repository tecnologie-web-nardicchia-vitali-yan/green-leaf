<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !isset($_POST["action"])) {
    header("location: login.php");
}

if ($_POST["action"] == 1 || $_POST["action"] == 5) {
    //Inserisco
    $via = $_POST["via"];
    $numero = $_POST["numero"];
    $citta = $_POST["città"];
    $provincia = $_POST["provincia"];
    $cap = $_POST["cap"];
    $nome_dest = $_POST["nome_destinatario"];
    $cognome_dest = $_POST["cognome_destinatario"];

    $email = $_SESSION["email"];

    $codice = $dbh->insertDatiSpedizione($via, $numero, $citta, $provincia, $cap, $nome_dest, $cognome_dest, $email);
    if ($codice) {
        $msg = "Inserimento completato correttamente!";
    } else {
        $msg = "Errore in inserimento!";
    }

    header("location: visualizzazione-dati.php?formmsg=" . $msg);

    if ($_POST["action"] == 5) {
        header("location: riepilogoOrdine.php?action=5");
    }
}

if ($_POST["action"] == 2) {
    //modifico
    $codice = $_POST["codice"];
    $via = $_POST["via"];
    $numero = $_POST["numero"];
    $citta = $_POST["città"];
    $provincia = $_POST["provincia"];
    $cap = $_POST["cap"];
    $nome_dest = $_POST["nome_destinatario"];
    $cognome_dest = $_POST["cognome_destinatario"];

    $email = $_SESSION["email"];


    $codice = $dbh->updateDatiSpedizione($codice, $via, $numero, $citta, $provincia, $cap, $nome_dest, $cognome_dest, $email);

    if ($codice) {
        $msg = "Modifica completata correttamente!";
    } else {
        $msg = "Errore di modifica!";
    }


    header("location: visualizzazione-dati.php?formmsg=" . $msg);
}

if ($_POST["action"] == 3) {
    //cancello
    $codice = $_POST["codice"];
    $email =  $_SESSION["email"];

    $result = $dbh->checkifExistOrderWithSpedizione($codice);

    if ($result != null) {
        $msg = "Errore nella cancellazione, non è possibile cancellare l'indirizzo perchè collegato a un ordine!";
    } else {
        $codice = $dbh->deleteDatiSpedizioneOfUtente($email, $codice);

        if ($codice) {
            $msg = "Cancellazione completata correttamente!";
        } else {
            $msg = "Errore nella cancellazione!";
        }
    }
    header("location: visualizzazione-dati.php?formmsg=" . $msg);
}
?>