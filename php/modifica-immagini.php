<?php
require_once 'bootstrap.php';
/*
if(!isUserLoggedIn() || !isUserVendor() || !isset($_GET["action"]) || ($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3)){
    header("location: login.php");
}*/

if (!isUserLoggedIn() ||  !isset($_GET["nome_albero"])) {
    header("location: login.php");
}


$risultato = $dbh->getTreeImgByName($_GET["nome_albero"]);

if (count($risultato) == 0) {
    $templateParams["immagini"] = null;
} else {
    $templateParams["immagini"] = $risultato;
}

if(isset($_GET["formmsg"])){
    $templateParams["formmsg"] = $_GET["formmsg"];
}

$templateParams["nome_albero"] = $_GET["nome_albero"];

$templateParams["titolo"] = "Green Leaf - Modifica immagini";
$templateParams["nome"] = "modifica-immagini.php";

$templateParams["stile"] = "style_gestisci_dati.css";
$templateParams["js"] = "modifica_immagini.js";

require 'template/base.php';
?>