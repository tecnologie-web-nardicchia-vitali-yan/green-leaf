<?php
require_once 'bootstrap.php';



if(isUserLoggedIn() && !$dbh->isUserRole($_SESSION["email"])){
    $templateParams["titolo"] = "Alberi piantati - Admin";
    $templateParams["nome"] = "visualizzazione-venditorealberipiantati.php";
    $templateParams["stile"] = "style_gestisci_dati.css";
    $templateParams["albero"] = $dbh->getAlberiPiantati();    
    
    
    if(isset($_GET["formmsg"])){
        $templateParams["formmsg"] = $_GET["formmsg"];
    }

}else{
    header("location: login.php");
}


require 'template/base.php';
?>