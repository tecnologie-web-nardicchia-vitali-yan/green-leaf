<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !(isset($_GET["id"]) || isset($_GET["action"]))) {
    header("location: carrello.php");
}

if (isset($_GET["id"])) {
    $_SESSION["pagamentoordine"] = $_GET["id"];
    $codice = $_GET["id"];
} else if ($_GET["action"] == 5) {
    $codice = $_SESSION["pagamentoordine"];
}

$templateParams["ordininormali"] = $dbh->getSpecificOrdersByOrderNumber($codice, 0, 0);
$templateParams["ordinipianta"] = $dbh->getSpecificOrdersByOrderNumber($codice, 1, 0);
$templateParams["ordiniregala"] = $dbh->getSpecificOrdersByOrderNumber($codice, 0, 1);
$templateParams["ordinipiantaregala"] = $dbh->getSpecificOrdersByOrderNumber($codice, 1, 1);
$templateParams["datispedizione"] = $dbh->getDatiSpedizioneByUserEmail($_SESSION["email"]);
$templateParams["datipagamento"] = $dbh->getDatiPagamentoByUserEmail($_SESSION["email"]);
$templateParams["ordine"] = $dbh->getOrderById($codice);

if(isset($_GET["formmsg"])){
    $templateParams["formmsg"] = $_GET["formmsg"];
}

if(isset($_POST["paga"]) && !isset($_POST["datipagamento"])) {
    $templateParams["errore"] = "Non hai ancora inserito una carta! Aggiungi subito un metodo di pagamento!";
} else if (isset($_POST["paga"]) && isset($_POST["datipagamento"])) {
    $cardNumber = $_POST["datipagamento"];
    $dbh->updateOrder(1, 0, $templateParams["ordine"][0]["prezzo_totale"], $cardNumber, $codice);

    $dbh->addNotification(2, $_SESSION["email"],date("Y-m-d"));
    $templateParams["venditori"] = $dbh->getSeller();
    foreach($templateParams["venditori"] as $venditore) {
        $dbh->addNotification(8, $venditore["email"], date("Y-m-d"));
    }
    header("location: risultatoPagamento.php");
}

$templateParams["titolo"] = "Green Leaf - Pagamento";
$templateParams["nome"] = "pagamento.php";
$templateParams["stile"] = "style_pagamento.css";

require 'template/base.php';
?>