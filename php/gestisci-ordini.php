<?php

require_once 'bootstrap.php';

if (isUserLoggedIn() && !$dbh->isUserRole($_SESSION["email"])) {
    $templateParams["titolo"] = "Green Leaf - Gestisci ordini";
    $templateParams["nome"] = "gestisci-ordini.php";
    $templateParams["stile"] = "gestisci_ordini.css";
    $templateParams["js"] = "gestisci_ordini.js";
    $orders_not_send = $dbh->getOrdersNotSend();
    $templateParams["nome_luoghi"] = $dbh->getPlaces();

    if (isset($_POST["processa"]) && isset($_POST["order_number"])) {
        $order_number = $_POST["order_number"];
        $templateParams["dettagli_ordine"] = $dbh->getOrderDetails($order_number);
        $templateParams["ordine"] = $dbh->getOrderById($order_number);
        $email_utente = $templateParams["ordine"][0]["email_utente"];

        foreach ($templateParams["dettagli_ordine"] as $dettaglio) {

            $codice = $dettaglio["codice"];
            $nome_albero = $dettaglio["nome_albero"];
            $quantità = $dettaglio["quantità"];
            $isRegalo = $dettaglio["regalo"];
            $isPiantare = $dettaglio["piantare"];
            $email_regalo = $dettaglio["email_regalo"];
            
            //pianta e regala
            if ($isPiantare && $isRegalo) { 
                
                if (isset($_POST["place".$codice])) {

                    $place = $_POST["place".$codice];

                    if ($dbh->isUserRegister($email_regalo)) {
                        $dbh->addTreePlanted(date("Y-m-d"), $email_utente, $nome_albero, NULL, $email_regalo, $place);
                        $dbh->addNotification(7, $email_regalo, date("Y-m-d"));
                    } else {
                        //se non è registrato
                        if (!$dbh->isUserUnregister($email_regalo)) {
                            $dbh->insertUnregisterUser($email_regalo);
                        }
                        $dbh->addTreePlanted(date("Y-m-d"), $email_utente, $nome_albero, $email_regalo, NULL, $place);
                        $dbh->addNotification(3, $email_utente, date("Y-m-d"));
                    }
                }

            //pianta
            } elseif ($isPiantare && !$isRegalo) {
                if (isset($_POST["place".$codice])) {

                    $place = $_POST["place".$codice];

                    $dbh->addTreePlanted(date("Y-m-d"), $email_utente, $nome_albero, NULL, NULL, $place);
                    $dbh->addNotification(4, $email_utente, date("Y-m-d"));
                }

            //regala e spedisci
            } elseif ((!$isPiantare && $isRegalo) || (!$isPiantare && !$isRegalo)) { 
                $dbh->addNotification(3, $email_utente, date("Y-m-d"));
            }
        }
        $dbh->setOrderSent($order_number);

        header("location: gestisci-ordini.php");
    }
} else {
    header("location: login.php");
}

require 'template/base.php';

?>