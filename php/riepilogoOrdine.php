<?php
require_once 'bootstrap.php';

if (!( isset($_GET["id"]) || isset($_GET["action"]) || isset($_GET["codice"]))) {
    header("location: carrello.php");
}


if (!isUserLoggedIn()) {
    header("location: login.php?action=1");
} else {
    
    if (isset($_GET["id"])) {
        $_SESSION["ordine"] = $_GET["id"];
        $codice = $_GET["id"];
    } else if ($_GET["action"] == 5) {
        $codice = $_SESSION["ordine"];
    }

    if (isset($_GET["action"])==1 && isset($_GET["codice"]) && isset($_GET["id"])){
        $codiceOrdine = $_GET["id"];
        $codiceOrdineDettaglio = $_GET["codice"];
        $ordineDettaglio = $dbh->getOrderDetailsByCode($codiceOrdineDettaglio);

        $treeName = $ordineDettaglio[0]["nome_albero"];
        $quantità = $ordineDettaglio[0]["quantità"] - 1;
        $regalo = $ordineDettaglio[0]["regalo"];
        $piantare = $ordineDettaglio[0]["piantare"];
        $codiceSpedizione = $ordineDettaglio[0]["codice_spedizione"];
        $costo = $ordineDettaglio[0]["costo"];
        $email = $ordineDettaglio[0]["email_regalo"];

        $dbh->updateOrderDetails($codiceOrdineDettaglio, $treeName, $quantità, $regalo, $piantare, $codiceSpedizione, $email);
        $dbh->addOrderDetails($treeName, 1, $regalo, $piantare, $codiceOrdine, $codiceSpedizione, $email);
    }

    $templateParams["ordinedettaglio"] = $dbh->getOrderDetailsByOrderNumber($codice);
    $templateParams["ordininormali"] = $dbh->getSpecificOrdersByOrderNumber($codice, 0, 0);
    $templateParams["ordinipianta"] = $dbh->getSpecificOrdersByOrderNumber($codice, 1, 0);
    $templateParams["ordiniregala"] = $dbh->getSpecificOrdersByOrderNumber($codice, 0, 1);
    $templateParams["ordinipiantaregala"] = $dbh->getSpecificOrdersByOrderNumber($codice, 1, 1);
    $templateParams["datispedizione"] = $dbh->getDatiSpedizioneByUserEmail($_SESSION["email"]);
    $templateParams["ordine"] = $dbh->getOrderById($codice);
    

    if (isset($_POST["procedi"])) {

        foreach($templateParams["ordininormali"] as $orderdetails) {
            $code = $orderdetails["codice"];
            $name = $orderdetails["nome_albero"];
            $quantity = $orderdetails["quantità"];
            $isGift = $orderdetails["regalo"];
            $isPlant = $orderdetails["piantare"];
            $address = $_POST["indirizzo_ordinenormale".$code];
            $email = $orderdetails["email_regalo"];
            $dbh->updateOrderDetails($code, $name, $quantity, $isGift, $isPlant, $address, $email);
        }

        foreach($templateParams["ordiniregala"] as $orderdetails) {
            $code = $orderdetails["codice"];
            $name = $orderdetails["nome_albero"];
            $quantity = $orderdetails["quantità"];
            $isGift = $orderdetails["regalo"];
            $isPlant = $orderdetails["piantare"];
            $address = $_POST["indirizzo_ordineregala".$code];
            $email = $orderdetails["email_regalo"];
            $dbh->updateOrderDetails($code, $name, $quantity, $isGift, $isPlant, $address, $email);
        }
        
        header("location: pagamento.php?id=".$codice);
    }   

    $templateParams["ordine"] = $dbh->getOrderById($codice);
    
    if (isset($_POST["update"])) {
        $order = $templateParams["ordine"][0];
        $isPaid = $order["pagato"];
        $isShipped = $order["spedito"];
        $totalPrice = $_POST["totale"];
        $cardNumber = $order["numero_carta"];
        $dbh->updateOrder($isPaid, $isShipped, $totalPrice, $cardNumber, $codice);
        $templateParams["ordine"] = $dbh->getOrderById($codice);
    }


    $templateParams["titolo"] = "Green Leaf - Riepilogo";
    $templateParams["nome"] = "riepilogoOrdine.php";
    $templateParams["stile"] = "style_carrello.css";
    $templateParams["js"] = "riepilogoOrdine.js";


}


require 'template/base.php';
?>