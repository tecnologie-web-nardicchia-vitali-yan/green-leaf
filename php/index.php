<?php
require_once 'bootstrap.php';

$templateParams["titolo"] = "Green Leaf - Home";
$templateParams["nome"] = "alberi.php";
$templateParams["randomTrees"] = $dbh->getRandomTrees();
$templateParams["stile"] = "style_alberi.css";

require 'template/base.php';
?>