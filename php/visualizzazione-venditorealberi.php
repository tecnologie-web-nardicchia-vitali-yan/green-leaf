<?php
require_once 'bootstrap.php';



if(isUserLoggedIn() && !$dbh->isUserRole($_SESSION["email"])){
    $templateParams["titolo"] = "Alberi in vendità - Admin";
    $templateParams["nome"] = "visualizzazione-venditorealberi.php";
    $templateParams["stile"] = "style_gestisci_dati.css";
    $templateParams["albero"] = $dbh->getAllTreesWithImages();
    
    
    
    if(isset($_GET["formmsg"])){
        $templateParams["formmsg"] = $_GET["formmsg"];
    }

}else{
    header("location: login.php");
}


require 'template/base.php';
?>