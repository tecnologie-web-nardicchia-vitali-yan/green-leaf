<?php
function isActive($pagename){
    if(basename($_SERVER['PHP_SELF'])==$pagename){
        echo " active";
    }
}

function getIdFromName($name){
    return preg_replace("/[^a-z]/", '', strtolower($name));
}

function isUserLoggedIn(){
    return !empty($_SESSION['email']);
}

function registerLoggedUser($user){
    $_SESSION["email"] = $user["email"];
    $_SESSION["nome"] = $user["nome"];
    $_SESSION["cognome"] = $user["cognome"];
    $_SESSION["telefono"] = $user["telefono"];
    //$_SESSION["ruolo"] = $user["ruolo"]; 
}

function registerOrderDetails($treeName, $quantity, $isGift, $isToPlant, $orderNumber, $shippingCode, $price, $image, $email) {
    $count= isset($_COOKIE["dettaglioordine"]) ? unserialize(end($_COOKIE["dettaglioordine"]))["codice"]+1 : 0;
    $orderDetails=["codice" => $count, "nome_albero" => $treeName, "quantità" => $quantity, "regalo" => $isGift, "piantare"=>$isToPlant, 
                       "numero_ordine" => $orderNumber, "codice_spedizione" => $shippingCode, "costo" => $price, 
                       "nome_immagine" => $image, "email_regalo" => $email];
    $orders = serialize($orderDetails);
    $name_cookie = "dettaglioordine[".$count."]";
    setcookie($name_cookie, $orders, time() + (60 * 60 * 24 * 30 * 12), "/");
}

function updateOrderDetails($code, $treeName, $quantity, $isGift, $isToPlant, $shippingCode, $email) {
    $name_cookie = "dettaglioordine[".$code."]";
    $order = unserialize($_COOKIE["dettaglioordine"][$code]);
    
    $order["nome_albero"] = $treeName;
    $order["quantità"] = $quantity;
    $order["regalo"] = $isGift;
    $order["piantare"] = $isToPlant;
    $order["codice_spedizione"] = $shippingCode;
    $order["email_regalo"] = $email;
  
    $order1 = serialize($order);
    setcookie($name_cookie, $order1, time() + (60 * 60 * 24 * 30), "/");
}

function deleteOrderDetails($code) {
    $name_cookie = "dettaglioordine[".$code."]";
    setcookie($name_cookie, "", time() - 3600, "/");
}

function checkValidityPhone($phone) {
    return is_numeric($phone) && strlen($phone)==10;
}

function getEmptyDatiSpedizione(){
    return array("codice" => "", "via" => "", "numero" => "", "città" => "", "provincia" => "", "CAP" => "", "nome_destinatario" => "", "cognome_destinatario" => "");
}

function getEmptyDatiPagamento(){
    return array("numero" => "", "tipo_carta" => "", "CVV" => "", "nome_titolare" => "", "cognome_titolare" => "", "data_scadenza" => "");
}

function getEmptyAlbero(){
    return array("nome" => "", "descrizione" => "", "costo" => "", "consumoCO2" => "");
}

function getEmptyDettaglioCrescita(){
    return array("data_rilevazione" => "", "altezza" => "");
}

function getAction($action){
    $result = "";
    switch($action){
        case 1:
            $result = "Inserisci";
            break;
        case 2:
            $result = "Modifica";
            break;
        case 3:
            $result = "Cancella";
            break;
        case 4:
            $result = "Mostra Immagine";
        case 5: 
            $result = "Aggiungi";
            break;
    }

    return $result;
}

function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $fullPath = $path.$imageName;
    
    $maxKB = 2000;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = 0;
    $msg = "";
    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }
    //Controllo dimensione dell'immagine < 500KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
}