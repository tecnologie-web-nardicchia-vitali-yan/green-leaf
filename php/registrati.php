<?php
require_once 'bootstrap.php';

if (isset($_POST["submit"])) {
    if (checkValidityPhone($_POST["phone"])) {

        //se non è registrato in utente
        if (!$dbh->isUserRegister($_POST["email"])) {
            //registrati in utente
            $password = $_POST["password"];
            $password_criptata = crypt($password, '$6$rounds=5000$usesomesillystringforsalt$');
            $dbh->registerUser($_POST["email"], $password_criptata, $_POST["name"], $_POST["surname"], $_POST["phone"]);
            $dbh->addNotification(1, $_POST["email"], date("Y-m-d"));
            $login_result = $dbh->checkLogin($_POST["email"], $password_criptata);
            registerLoggedUser($login_result[0]);
            //se è registrato in utentenoniscritto
            if ($dbh->isUserUnregister($_POST["email"])) {
                //cambia utente in alberopiantato
                $dbh->setUserRegisterTreePlanted($_POST["email"]);
                $dbh->setNullUserUnegisterTreePlanted($_POST["email"]);
                $dbh->addNotification(5, $_POST["email"], date("Y-m-d"));
                //togli registrazione in utentenoniscritto
                $dbh->deleteUserUnregister($_POST["email"]);
            }
        //se è registrato
        } else {
            //errore: utente già presente
            $templateParams["registrato"] = "Errore! L'utente è già presente in memoria!";
        }
    } else {
        $templateParams["errore"] = "Errore! Numero di telefono non corretto!";
    }
}

if(isUserLoggedIn()){
    if (isset($_GET["action"]) && $_GET["action"]==1) {
        header("Location: carrello.php?action=1");
    } else if (isset($_COOKIE["dettaglioordine"]) && count($_COOKIE["dettaglioordine"])>0) {
        header("Location: carrello.php?action=2");
    } else {
        header("location: index.php");
    }
} else{
    $templateParams["titolo"] = "Green Leaf - Register";
    $templateParams["nome"] = "registrati.php";
    $templateParams["stile"] = "style_utente.css";
}

require 'template/base.php';
?>