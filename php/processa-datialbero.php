<?php
require_once 'bootstrap.php';

if (!isUserLoggedIn() || !isset($_POST["action"]) || $dbh->isUserRole($_SESSION["email"])) {
    header("location: login.php");
}

if ($_POST["action"] == 1) {
    //Inserisco
    $nome_albero = $_POST["nome"];
    $descrizione = $_POST["descrizione"];
    $costo = $_POST["costo"];
    $consumoCO2 = $_POST["consumoCO2"];

    list($result, $nome_immagine) = uploadImage(UPLOAD_DIR, $_FILES["immagine"]);

    if ($result) {
        $codice = $dbh->insertDatiImmagine($nome_immagine);

        if (!$codice) {
            $msg = "Errore inserimento immagine!";
        } else {
            $codice = $dbh->insertDatiAlbero($nome_albero, $descrizione, $costo, $consumoCO2);

            if (!$codice) {
                $dbh->deleteImmagine($nome_immagine); //toglie l'immagine inserita precedentemente
                $msg = "Errore inserimento albero!";
            } else {
                $codice = $dbh->insertImmagineAlberoForeignKey($nome_immagine, $nome_albero);

                if ($codice) {
                    $msg = "Inserimento completato correttamente!";
                } else {
                    $dbh->deleteDatiAlbero($nome_albero); //toglie l'albero inserito precedentemente
                    $msg = "Errore in inserimento!";
                }
            }
        }
    } else {
        $msg = "Errore caricamento immagine!" . $nome_immagine;
    }

    header("location: visualizzazione-venditorealberi.php?formmsg=" . $msg);
}

if ($_POST["action"] == 2) {
    //modifico
    $nome = $_POST["nome"];
    $descrizione = $_POST["descrizione"];
    $costo = $_POST["costo"];
    $consumoCO2 = $_POST["consumoCO2"];
    $nome_vecchio = $_POST["nome_vecchio"];

    $codice = $dbh->updateDatiAlbero($nome, $descrizione, $costo, $consumoCO2);

    if ($codice) {
        $msg = "Modifica completata correttamente!";
    } else {
        $msg = "Errore di modifica!";
    }


    header("location: visualizzazione-venditorealberi.php?formmsg=" . $msg);
}

if ($_POST["action"] == 3) {
    //cancello
    $nome = $_POST["nome"];

    $immagini = $dbh->getTreeImgByName($nome);

    foreach (array_slice($immagini, 0) as $img) {
        $dbh->deleteDatiAlberoImmagine($nome, $img["nome_immagine"]);
        $dbh->deleteImmagine($immagini[$i]);
    }

    $codice = $dbh->deleteDatiAlbero($nome);

    if ($codice) {
        $msg = "Cancellazione completata correttamente!";
    } else {
        $msg = "Errore nella cancellazione!";
    }

    header("location: visualizzazione-venditorealberi.php?formmsg=" . $msg);
}

?>
