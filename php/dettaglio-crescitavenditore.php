<?php
require_once 'bootstrap.php';


// controllare che sia un venditore dopo MERGE

if(!isUserLoggedIn() || $dbh->isUserRole($_SESSION["email"]) || !isset($_GET["codice"])){
    header("location: login.php");
}else{
    $templateParams["titolo"] = "Dettagli Crescita - Admin";
    $templateParams["nome"] = "dettaglio-crescitavenditore.php";
    $templateParams["stile"] = "style_gestisci_dati.css";
    $templateParams["dettaglio_crescita"] = $dbh->getDettagliCrescitaWithImagesAlbero($_GET["codice"]);
    $templateParams["codice_albero"] = $_GET["codice"];
    
    
    
    if(isset($_GET["formmsg"])){
        $templateParams["formmsg"] = $_GET["formmsg"];
    }

}


require 'template/base.php';
?>