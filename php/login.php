<?php
require_once 'bootstrap.php';

if (isset($_POST["submit"])) {
    $email = $_POST["email"];

    $password = $_POST["password"];
    $password_criptata = crypt($password, '$6$rounds=5000$usesomesillystringforsalt$');

    $login_result = $dbh->checkLogin($email, $password_criptata);
    if(count($login_result)==0){
        $templateParams["errore"] = "Errore! E-mail o password non corrette!";
    }
    else{
        registerLoggedUser($login_result[0]);
    }
}

if (isset($_GET["action"]) && $_GET["action"]==1) {
    $templateParams["action"] = 1;
}

if(isUserLoggedIn()) {
    if (isset($templateParams["action"])){
        header("Location: carrello.php?action=1");
    } else if (isset($_COOKIE["dettaglioordine"]) && count($_COOKIE["dettaglioordine"])>0) {
        header("Location: carrello.php?action=2");
    } else {
        header("Location: index.php");
    }
} else {
    $templateParams["titolo"] = "Green Leaf - Login";
    $templateParams["nome"] = "login.php";
    $templateParams["stile"] = "style_utente.css";
}

require 'template/base.php';
?>