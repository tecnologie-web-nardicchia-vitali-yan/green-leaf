$(document).ready(function() {

    $("main > form").submit(function(e) {

        e.preventDefault();

        const fields = [$("input#via"), $("input#numcivico"), $("input#città"), $("input#prov"),
            $("input#cap"), $("input#nome-dest"), $("input#cognome-dest")
        ];

        let isformOK = true;

        let i;
        let count = 0;

        $("div .text-danger").remove();

        for (i = 0; i < fields.length; i++) {
            if (fields[i].val() == "") {
                count++;
                fields[i].parent().append("<p class=\"text-danger\">Questo campo non può essere vuoto</p>");
            }
        }

        if (count > 0) {
            isformOK = false;
        } else if (!$.isNumeric(fields[4].val() || fields[4].val().length != 5)) {
            isformOK = false;
            fields[4].parent().append("<p class=\"text-danger\">CAP non valido</p>");
        }

        if (isformOK) {
            e.currentTarget.submit();
        }


    });





});