$(document).ready(function() {

    $("main > form").submit(function(e) {

        e.preventDefault();

        const fields = [$("input#nome"), $("input#cognome"), $("input#telefono")];

        let isformOK = true;

        let i;
        let count = 0;

        $("div .text-danger").remove();

        for (i = 0; i < fields.length; i++) {
            if (fields[i].val() == "") {
                count++;
                fields[i].parent().append("<p class=\"text-danger\">Questo campo non può essere vuoto</p>");
            }
        }

        if (count > 0) {
            isformOK = false;
        } else if (!$.isNumeric(fields[2].val()) || fields[2].val().length != 10) {
            isformOK = false;
            fields[2].parent().append("<p class=\"text-danger\">Telefono non valido</p>");
        }

        if (isformOK) {
            e.currentTarget.submit();
        }


    });





});