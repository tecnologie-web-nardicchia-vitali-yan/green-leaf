
$(document).ready(function () {

    $("main > form").submit(function (e) {

        e.preventDefault();

        const fields = [$("input#numero"), $("select#tipo_carta"), $("input#cvv"), $("input#nome_titolare"),
        $("input#cognome_titolare"), $("input#data_scadenza")];

        let isformOK = true;

        let i;
        let count = 0;

        $("div .text-danger").remove();

        for (i = 0; i < fields.length; i++) {
            if (fields[i].val() == "") {
                count++;
                fields[i].parent().append("<p class=\"text-danger\">Questo campo non può essere vuoto</p>");
            }
        }

        if ($("input#action").val() == 1 || $("input#action").val() == 5) {
            if (count > 0) {
                isformOK = false;
            } else if (!$.isNumeric(fields[2].val()) || fields[2].val().length != 3 || !$.isNumeric(fields[0].val()) || fields[0].val().length != 16) {
                isformOK = false;
                if (!$.isNumeric(fields[2].val()) || fields[2].val().length != 3) {
                    fields[2].parent().append("<p class=\"text-danger\">CVV non valido</p>");
                }

                if (!$.isNumeric(fields[0].val()) || fields[0].val().length != 16) {
                    fields[0].parent().append("<p class=\"text-danger\">Numero carta non valido</p>");
                }

            }
        } else {
            if (count > 0) {
                isformOK = false;
            } else if (!$.isNumeric(fields[2].val()) || fields[2].val().length != 3) {
                isformOK = false;
                fields[2].parent().append("<p class=\"text-danger\">CVV non valido</p>");
            }
        }

        if (isformOK) {
            e.currentTarget.submit();
        }



    })



});