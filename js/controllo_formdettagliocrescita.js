
$(document).ready(function () {

    $("main > form").submit(function (e) {

        e.preventDefault();

        const fields = [$("input#data_rilevazione"), $("input#altezza")];

        let isformOK = true;

        let i;
        let count = 0;

        $("div .text-danger").remove();

        for (i = 0; i < fields.length; i++) {
            if (fields[i].val() == "") {
                count++;
                fields[i].parent().append("<p class=\"text-danger\">Questo campo non può essere vuoto</p>");
            }
        }

        if ($("input#azione").val() == 1) {

            if ($("input#file_immagine").val() == "") {
                isformOK = false;
                $("input#file_immagine").parent().append("<p class=\"text-danger\">Scegliere un file</p>");
            }
        }

        if (count > 0) {
            isformOK = false;
        } 
        
        if (isformOK) {
            e.currentTarget.submit();
        }

    })

    $("input#file_immagine").change(function () {

        $(this).prev('label').text($(this)[0].files[0].name);

    });

    $("input#file_immagine_modifica").change(function () {

        $(this).prev('label').text($(this)[0].files[0].name);
  
       
        const dettaglio_crescita = $("input#codice").val();
        const albero_piantato = $("input#albero_piantato").val();
        const codice = $("input#codice").val();
        const file =  $(this)[0].files[0];
        
        formdata = new FormData();
        formdata.append('immagine', file);
        formdata.append('dettaglio_crescita', dettaglio_crescita);
        formdata.append('albero_piantato', albero_piantato);
        formdata.append('codice', codice);
        formdata.append('action', '4'); 

        
        $.ajax({
            url: 'processa-dettagliocrescita.php',
            type: 'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function(response){
                if(!response.error){
                    window.location.reload();
                }
            }

        })

       
    });

});