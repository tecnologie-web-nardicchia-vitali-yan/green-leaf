$(document).ready(function() {
    $("article > header")
    .mouseover(function() {
        $(this).children().css("background-color", "#86C4AF");
    })
    .mouseleave(function() {
        $(this).children().css("background-color", "#EEFAF2");
    });
});