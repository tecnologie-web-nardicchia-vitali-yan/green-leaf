$(document).ready(function() {
    let = "#pianta";
    let regala = "#regala";

    showRegalaPiantaEmail(pianta, regala);

    $(pianta).change(function() {
        showRegalaPiantaEmail(pianta, regala);
    });

    $(regala).change(function() {
        showRegalaPiantaEmail(pianta, regala);
    });

    function showRegalaPiantaEmail(cb1, cb2) {
        if ($(cb1).is(':checked') && $(cb2).is(':checked')) {
            $("#regala-email").show();
            document.querySelector("#regala-email>input").setAttribute("required", "");
        } else {
            $("#regala-email").hide();
            document.querySelector("#regala-email>input").removeAttribute("required");
        }
    }
});