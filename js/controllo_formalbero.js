
$(document).ready(function () {

    $("main > form").submit(function (e) {

        e.preventDefault();

        const fields = [$("input#nome"), $("input#descrizione"), $("input#costo"), $("input#consumoCO2")];

        let isformOK = true;

        let i;
        let count = 0;

        $("div .text-danger").remove();

        for (i = 0; i < fields.length; i++) {
            if (fields[i].val() == "") {
                count++;
                fields[i].parent().append("<p class=\"text-danger\">Questo campo non può essere vuoto</p>");
            }
        }

        if ($("input#azione").val() == 1) {

            if ($("input#file_immagine").val() == "") {
                isformOK = false;
                $("input#file_immagine").parent().append("<p class=\"text-danger\">Scegliere un file</p>");
            }
        }

        if (count > 0) {
            isformOK = false;
        } else if (!$.isNumeric(fields[2].val()) || !$.isNumeric(fields[3].val())) {
            isformOK = false;
            if (!$.isNumeric(fields[2].val())) {
                fields[2].parent().append("<p class=\"text-danger\">Costo non valido</p>");
            }

            if (!$.isNumeric(fields[3].val())) {
                fields[3].parent().append("<p class=\"text-danger\">ConsumoCO2 non valido</p>");
            }

        }

        if (isformOK) {
            e.currentTarget.submit();
        }

    })

    $("input#file_immagine").change(function () {

        $(this).prev('label').text($(this)[0].files[0].name);

    });

});