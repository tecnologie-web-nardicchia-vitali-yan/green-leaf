$(document).ready(function () {

    $("input#file_immagine").change(function () {

        $("p#nome-file").removeClass("text-danger").text($(this)[0].files[0].name);

    });

    $("form").submit(function (e) {

        e.preventDefault();

        isFormOk = true;

        if ($("input#file_immagine").val() == "") {
            isFormOk = false;
            $("p#nome-file").addClass("text-danger").text("Scegliere un file");
        }

        if (isFormOk) {
            e.currentTarget.submit();
        }

    });

});