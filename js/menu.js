$(document).ready(function() {
    
    let fileName = getFileName();
    if (fileName == "login.php" || fileName == "registrati.php"
    || fileName == "login.php#" || fileName == "registrati.php#") {
        $("body > div").hide();
    }

    if (fileName == "login.php" || fileName == "login.php#") {
        $("body > footer").css("bottom","0");
    }


    setInterval(function() {
        let isMobile = window.matchMedia("(max-width: 767px)").matches;

        if (!$("body > header > nav.navbar").is(":visible") && !isMobile) {
            if (fileName == "login.php" || fileName == "registrati.php"
            || fileName == "login.php#" || fileName == "registrati.php#") {
                $("body > div").hide();
                $("main").show();
            } else {
                $("body > div, main").show();
            }
            $("body > header > div > a").removeClass("navbar-toggler").addClass("dropdown-toggle");
            $("body > header > div > ul.navbar-nav").removeClass("navbar-nav").addClass("dropdown-menu");
            $("body > header > div > ul.navbar-nav > li").removeClass("nav-item").addClass("dropdown-item");
            $("body > header > nav > div").removeClass("show");
        }

    }, 10);

    $("body > header > nav > button").click(function() {
        if (!$("body > header > nav > div").is(".show")) {
            $("body > div, main").hide();
            $("body > header > nav > div > a").removeClass("dropdown-toggle").addClass("navbar-toggler"); 
            $("body > header > nav > div > ul.dropdown-menu").removeClass("dropdown-menu").addClass("navbar-nav");
            $("body > header > nav > div > ul.dropdown-menu > li").removeClass("dropdown-item").addClass("nav-item");
            /* $("body > footer").css({ "position" : "absolute", "bottom" : "0" }); */
        } else {
            if (fileName == "login.php" || fileName == "registrati.php"
            || fileName == "login.php#" || fileName == "registrati.php#") {
                $("body > div").hide();
                $("main").show();
            } else {
                $("body > div, main").show();
            }
            $("body > header > div > a").removeClass("navbar-toggler").addClass("dropdown-toggle");
            $("body > header > div > ul.navbar-nav").removeClass("navbar-nav").addClass("dropdown-menu");
            $("body > header > div > ul.navbar-nav > li").removeClass("nav-item").addClass("dropdown-item");
            /* $("body > footer").css("position", "relative"); */
        }
    });

    function getFileName() {
        return location.href.substr(location.href.lastIndexOf("/")+1);
    }
});