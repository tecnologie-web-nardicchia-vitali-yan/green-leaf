$(document).ready(function() {

    $("button.close").click(function() {
        const table = $(this).parent().parent().parent();
        const numero = table.find("td:first-child > p:first-child").text();

        formdata = new FormData();
        formdata.append('numero', numero);
        
        $.ajax({
            url: 'processa-notifiche.php',
            type: 'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function(response){
                if(!response.error){
                    window.location.reload();
                }
            }
        });

    });

});