-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 27, 2021 alle 17:51
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greenleaf`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `albero`
--

CREATE TABLE `albero` (
  `nome` varchar(50) NOT NULL,
  `descrizione` text NOT NULL,
  `costo` float NOT NULL,
  `consumoCO2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `albero`
--

INSERT INTO `albero` (`nome`, `descrizione`, `costo`, `consumoCO2`) VALUES
('Albero di Giuda', 'L’albero di Giuda è una pianta originaria delle coste mediterranee, alta mediamente 7 metri. Si tratta di un albero molto particolare per la struttura del tronco e per il tipo di fioritura. I fiori riuniti in racemi di colore rosa, lilla e bianchi fioriscono direttamente sui rami e sul tronco.', 20, 400),
('Arancia', '      Per migliaia di anni, la coltivazione delle arance si è svolta nel sud della Cina e successivamente si è diffusa in tutto il sud-est asiatico. La Via della Seta fu la causa della diffusione dell’arancia in tutto l’oriente. Fu grazie agli arabi che attraverso il sud della Spagna portarono l’arancia in Europa.      ', 15, 200),
('Avocado', 'L’albero di Avocado, è originario del Guatemala e del Messico e viene coltivato da più di 7000 anni. Fu scoperto durante le esplorazioni dell’America Centrale, dove faceva già parte dell’alimentazione degli indigeni Aztechi e Maya, prima dell’arrivo degli Spagnoli.', 17, 800),
('Caoba', 'La Caoba, o Mogano a foglia larga, è un albero alto e maestoso che sovrasta la chioma della foresta.\r\nCaoba era un termine indigeno che sta per “frutto che non si mangia”. Particolarmente apprezzato dagli ebanisti, questo albero ha una distribuzione irregolare dal Messico meridionale attraverso l’America centrale e a sud fino al Brasile e alla Bolivia. Il bellissimo legno duro ottenuto da alberi di mogano è stato molto richiesto per secoli e questa specie è classificata come vulnerabile (VU) dall’IUCN.', 16, 700),
('Cedro', 'L’albero di cedro in questione, contrariamente a quanto si possa pensare, non è l’albero dell’agrume, bensì un albero forestale tipico del Centro America.', 16, 630),
('Chicozapote', 'L’albero da cui si ricava il chicle, ingrediente delle gomme da masticare. I dolci frutti sono tipici del Centro America ed hanno un gusto che ricorda molto la mandorla e la pera.Originario delle foreste atlantiche del Nicaragua come anche del Gran Peten, che copre parte della penisola dello Yucatan, Belize e Guatemala.\r\n\r\nIl nome chicozapote deriva dalla lingua nahuatl è significa “zapote di miele”, ad indicare il sapore più dolce degli altri frutti della famiglia delle Sapotaceae. Storicamente il lattice della pianta venne usato come masticatorio (chicle), costituendo l’ingrediente base pregiato della gomma da masticare.', 18, 500),
('Lime', 'Il lime è un agrume originario del Sud-Est asiatico, da dove i portoghesi l’hanno poi importato in America. Ama il clima tropicale, infatti non viene coltivato in Europa ma in India, Messico, Caraibi e America latina.', 14, 100),
('Mango', 'Fin da tempi antichissimi (quattromila anni fa), il mango ha avuto una certa importanza in India, suo paese d’origine. L’albero di mango appare in molte leggende indiane ed è considerato sacro dagli Indù, che con le foglie di questa pianta fanno delle ghirlande per adornare i templi.', 18, 700),
('Pino', 'Il pino dei Caraibi cresce anche fino a 45 metri e ha una corteccia che va dal rosso-rossastro al grigio. L’albero cresce rapidamente e la sua crescita è direttamente influenzata dalle precipitazioni. Il pino dei Caraibi prospera su terreni acidi e ben drenati, spesso vicino alla costa. L’albero è elencato come Least Concern, e non ci sono sforzi di conservazione identificati in Guatemala.', 16, 630);

-- --------------------------------------------------------

--
-- Struttura della tabella `alberopiantato`
--

CREATE TABLE `alberopiantato` (
  `codice` int(11) NOT NULL,
  `data_piantagione` date NOT NULL,
  `email_utente` varchar(50) NOT NULL,
  `nome_albero` varchar(50) NOT NULL,
  `email_regalo_noniscritto` varchar(50) DEFAULT NULL,
  `email_regalo_iscritto` varchar(100) DEFAULT NULL,
  `posizione` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `alberopiantato`
--

INSERT INTO `alberopiantato` (`codice`, `data_piantagione`, `email_utente`, `nome_albero`, `email_regalo_noniscritto`, `email_regalo_iscritto`, `posizione`) VALUES
(1, '2021-01-27', 'maria@gmail.com', 'Chicozapote', NULL, NULL, 8),
(2, '2021-01-27', 'anna@gmail.com', 'Pino', NULL, NULL, 6),
(3, '2021-01-27', 'sara@gmail.com', 'Caoba', NULL, 'anna@gmail.com', 7),
(4, '2021-01-27', 'anna@gmail.com', 'Pino', NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `datispedizione`
--

CREATE TABLE `datispedizione` (
  `codice` int(11) NOT NULL,
  `via` varchar(50) NOT NULL,
  `numero` varchar(5) NOT NULL,
  `città` varchar(50) NOT NULL,
  `provincia` varchar(50) NOT NULL,
  `CAP` char(5) NOT NULL,
  `nome_destinatario` varchar(15) NOT NULL,
  `cognome_destinatario` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `datispedizione`
--

INSERT INTO `datispedizione` (`codice`, `via`, `numero`, `città`, `provincia`, `CAP`, `nome_destinatario`, `cognome_destinatario`, `email`) VALUES
(1, 'Firenza', '43', 'Cesena', 'FC', '47521', 'Maria', 'Mengozzi', 'maria@gmail.com'),
(2, 'Bolognesi', '51', 'Cesena', 'FC', '47521', 'Maria', 'Mengozzi', 'maria@gmail.com'),
(3, 'Ribolle', '31', 'Bologna', 'BO', '40121', 'Anna', 'Vitali', 'anna@gmail.com'),
(4, 'Emilia', '23', 'Savignano sul Rubicone', 'FC', '47039', 'Sara', 'Rossini', 'sara@gmail.com'),
(5, 'Verdi', '25', 'Cesena', 'FC', '44127', 'Elena', 'Yan', 'elena.yan@gmail.com'),
(6, 'Cristoforo Colombo', '3', 'Forlì', 'FC', '44128', 'Giulia', 'Nardicchia', 'elena.yan@gmail.com'),
(7, 'Marco Polo', '45', 'Ferrara', 'FE', '44123', 'Stefano', 'Vitali', 'giulia@gmail.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `dettagliocrescita`
--

CREATE TABLE `dettagliocrescita` (
  `codice` int(11) NOT NULL,
  `data_rilevazione` date NOT NULL,
  `altezza` float NOT NULL,
  `albero_piantato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `dettagliocrescita`
--

INSERT INTO `dettagliocrescita` (`codice`, `data_rilevazione`, `altezza`, `albero_piantato`) VALUES
(1, '2021-01-25', 0.06, 1),
(2, '2021-01-27', 1, 1),
(3, '2021-01-27', 0.05, 2),
(4, '2021-01-27', 0.1, 3),
(5, '2021-01-28', 0.13, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `dettaglioordine`
--

CREATE TABLE `dettaglioordine` (
  `codice` int(11) NOT NULL,
  `nome_albero` varchar(50) NOT NULL,
  `quantità` int(11) NOT NULL,
  `regalo` tinyint(4) NOT NULL,
  `piantare` tinyint(4) NOT NULL,
  `numero_ordine` int(11) NOT NULL,
  `codice_spedizione` int(11) DEFAULT NULL,
  `email_regalo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `dettaglioordine`
--

INSERT INTO `dettaglioordine` (`codice`, `nome_albero`, `quantità`, `regalo`, `piantare`, `numero_ordine`, `codice_spedizione`, `email_regalo`) VALUES
(2, 'Chicozapote', 1, 0, 1, 2, NULL, NULL),
(3, 'Pino', 1, 1, 0, 2, 1, NULL),
(4, 'Pino', 1, 0, 1, 5, NULL, NULL),
(5, 'Caoba', 1, 1, 1, 6, NULL, 'anna@gmail.com'),
(6, 'Lime', 1, 0, 0, 8, NULL, NULL),
(7, 'Mango', 1, 1, 0, 10, 6, NULL),
(8, 'Cedro', 1, 1, 1, 10, NULL, 'giulia@gmail.com'),
(9, 'Avocado', 1, 1, 0, 10, 5, NULL),
(10, 'Albero di Giuda', 1, 1, 1, 11, NULL, 'anna@gmail.com'),
(11, 'Lime', 1, 0, 0, 11, 7, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagine`
--

CREATE TABLE `immagine` (
  `nome` varchar(100) NOT NULL,
  `dettaglio_crescita` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `immagine`
--

INSERT INTO `immagine` (`nome`, `dettaglio_crescita`) VALUES
('AlberoGiuda-01_2.png', NULL),
('AlberoGiuda-02_2.jpg', NULL),
('AlberoGiuda-03_2.jpg', NULL),
('Arancia-01.png', NULL),
('Arancia-02.jpg', NULL),
('Arancia-03.jpg', NULL),
('Avocado-01.png', NULL),
('Avocado-02.jpg', NULL),
('Avocado-03.jpg', NULL),
('Caoba-01.png', NULL),
('Caoba-02_2.jpg', NULL),
('Caoba-03.jpg', NULL),
('Cedro-01.png', NULL),
('Cedro-02.jpeg', NULL),
('Cedro-03.jpg', NULL),
('Chico-zapote-01.png', NULL),
('Chico-zapote-01_3.png', NULL),
('Chicozapote-02.jpeg', NULL),
('Chicozapote-03.jpeg', NULL),
('Lime-01.png', NULL),
('Lime-02.jpg', NULL),
('Lime-03.jpg', NULL),
('Mango-01.png', NULL),
('Mango-02.jpg', NULL),
('Mango-03.jpg', NULL),
('Pino-01.png', NULL),
('Pino-02.jpg', NULL),
('Pino-03.jpg', NULL),
('Chicozapote-04.jpg', 1),
('Chicozapote-05.jpeg', 2),
('Pino-04_2.jpg', 3),
('Caoba-05_2.jpg', 4),
('Pino-05.jpg', 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagine_albero`
--

CREATE TABLE `immagine_albero` (
  `nome_immagine` varchar(100) NOT NULL,
  `nome_albero` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `immagine_albero`
--

INSERT INTO `immagine_albero` (`nome_immagine`, `nome_albero`) VALUES
('Arancia-01.png', 'Arancia'),
('Avocado-01.png', 'Avocado'),
('Lime-01.png', 'Lime'),
('Mango-01.png', 'Mango'),
('Arancia-02.jpg', 'Arancia'),
('Arancia-03.jpg', 'Arancia'),
('Avocado-02.jpg', 'Avocado'),
('Avocado-03.jpg', 'Avocado'),
('Lime-02.jpg', 'Lime'),
('Lime-03.jpg', 'Lime'),
('Mango-02.jpg', 'Mango'),
('Mango-03.jpg', 'Mango'),
('Chico-zapote-01.png', 'Chicozapote'),
('Chicozapote-02.jpeg', 'Chicozapote'),
('Chicozapote-03.jpeg', 'Chicozapote'),
('Caoba-01.png', 'Caoba'),
('Caoba-03.jpg', 'Caoba'),
('Caoba-02_2.jpg', 'Caoba'),
('Cedro-01.png', 'Cedro'),
('Cedro-02.jpeg', 'Cedro'),
('Cedro-03.jpg', 'Cedro'),
('Pino-01.png', 'Pino'),
('Pino-02.jpg', 'Pino'),
('Pino-03.jpg', 'Pino'),
('AlberoGiuda-01_2.png', 'Albero di Giuda'),
('AlberoGiuda-02_2.jpg', 'Albero di Giuda'),
('AlberoGiuda-03_2.jpg', 'Albero di Giuda');

-- --------------------------------------------------------

--
-- Struttura della tabella `modalitàdipagamento`
--

CREATE TABLE `modalitàdipagamento` (
  `numero` varchar(20) NOT NULL,
  `tipo_carta` varchar(20) NOT NULL,
  `CVV` char(3) NOT NULL,
  `nome_titolare` varchar(15) NOT NULL,
  `cognome_titolare` varchar(15) NOT NULL,
  `data_scadenza` date NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `modalitàdipagamento`
--

INSERT INTO `modalitàdipagamento` (`numero`, `tipo_carta`, `CVV`, `nome_titolare`, `cognome_titolare`, `data_scadenza`, `email`) VALUES
('1234567890122455', 'Visa', '195', 'Anna', 'Vitali', '2021-03-19', 'anna@gmail.com'),
('1234567890123451', 'Visa', '365', 'Maria', 'Mengozzi', '2021-02-07', 'maria@gmail.com'),
('3645936168774396', 'Maestro', '243', 'Sara', 'Rossini', '2022-02-25', 'sara@gmail.com'),
('4564735869564738', 'MasterCard', '564', 'Elena', 'Yan', '2023-03-15', 'elena.yan@gmail.com'),
('6758496758763456', 'Maestro', '543', 'Giulia', 'Nardicchia', '2023-03-25', 'giulia@gmail.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifica`
--

CREATE TABLE `notifica` (
  `codice` int(11) NOT NULL,
  `messaggio` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `notifica`
--

INSERT INTO `notifica` (`codice`, `messaggio`) VALUES
(1, 'Benvenuto su Green Leaf!'),
(2, 'Pagamento effettuato con successo!'),
(3, 'Il tuo albero è stato spedito!'),
(4, 'Il tuo albero è stato piantato!'),
(5, 'Hai ricevuto un regalo!'),
(6, 'E\' stato aggiunto un dettaglio di crescita all\'albero che hai piantato!'),
(7, 'Hai ricevuto un regalo e il tuo albero è stato piantato!'),
(8, 'Hai ricevuto un nuovo ordine!');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifica_utente`
--

CREATE TABLE `notifica_utente` (
  `numero` int(11) NOT NULL,
  `codice` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `notifica_utente`
--

INSERT INTO `notifica_utente` (`numero`, `codice`, `email`, `data`) VALUES
(1, 1, 'elena.yan@gmail.com', '2021-01-16'),
(2, 1, 'antionio.verdi@gmail.com', '2021-01-16'),
(3, 1, 'bianchi.mauro@gmail.com', '2021-01-20'),
(4, 1, 'franco@libero.it', '2021-01-23'),
(5, 1, 'luigi.verdi@gmail.com', '2021-01-23'),
(6, 1, 'maria@gmail.com', '2021-01-24'),
(7, 1, 'sara@gmail.com', '2021-01-25'),
(8, 2, 'sara@gmail.com', '2021-01-27'),
(9, 8, 'green.leaf@gmail.com', '2021-01-27'),
(10, 8, 'mario.rossi@gmail.com', '2021-01-27'),
(11, 3, 'sara@gmail.com', '2021-01-27'),
(12, 2, 'maria@gmail.com', '2021-01-24'),
(13, 8, 'green.leaf@gmail.com', '2021-01-27'),
(14, 8, 'mario.rossi@gmail.com', '2021-01-27'),
(15, 4, 'maria@gmail.com', '2021-01-24'),
(16, 3, 'maria@gmail.com', '2021-01-24'),
(17, 2, 'anna@gmail.com', '2021-01-27'),
(18, 8, 'green.leaf@gmail.com', '2021-01-27'),
(19, 8, 'mario.rossi@gmail.com', '2021-01-27'),
(20, 4, 'anna@gmail.com', '2021-01-27'),
(21, 6, 'anna@gmail.com', '2021-01-27'),
(22, 2, 'sara@gmail.com', '2021-01-27'),
(23, 8, 'green.leaf@gmail.com', '2021-01-27'),
(24, 8, 'mario.rossi@gmail.com', '2021-01-27'),
(25, 7, 'anna@gmail.com', '2021-01-27'),
(26, 6, 'sara@gmail.com', '2021-01-27'),
(27, 6, 'anna@gmail.com', '2021-01-27'),
(28, 4, 'anna@gmail.com', '2021-01-27'),
(29, 2, 'elena.yan@gmail.com', '2021-01-27'),
(30, 8, 'green.leaf@gmail.com', '2021-01-27'),
(31, 8, 'mario.rossi@gmail.com', '2021-01-27'),
(32, 2, 'giulia@gmail.com', '2021-01-27'),
(33, 8, 'green.leaf@gmail.com', '2021-01-27'),
(34, 8, 'mario.rossi@gmail.com', '2021-01-27');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `numero` int(11) NOT NULL,
  `pagato` tinyint(4) NOT NULL,
  `spedito` tinyint(4) NOT NULL,
  `prezzo_totale` float NOT NULL,
  `email_utente` varchar(50) NOT NULL,
  `numero_carta` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`numero`, `pagato`, `spedito`, `prezzo_totale`, `email_utente`, `numero_carta`) VALUES
(1, 1, 1, 20, 'sara@gmail.com', '3645936168774396'),
(2, 1, 1, 34, 'maria@gmail.com', '1234567890123451'),
(3, 0, 0, 0, 'maria@gmail.com', NULL),
(4, 0, 0, 0, 'green.leaf@gmail.com', NULL),
(5, 1, 1, 16, 'anna@gmail.com', '1234567890122455'),
(6, 1, 1, 16, 'sara@gmail.com', '3645936168774396'),
(7, 0, 0, 0, 'sara@gmail.com', NULL),
(8, 0, 0, 0, 'anna@gmail.com', NULL),
(9, 0, 0, 0, 'mario.rossi@gmail.com', NULL),
(10, 1, 0, 51, 'elena.yan@gmail.com', '4564735869564738'),
(11, 1, 0, 34, 'giulia@gmail.com', '6758496758763456');

-- --------------------------------------------------------

--
-- Struttura della tabella `posizione`
--

CREATE TABLE `posizione` (
  `codice` int(11) NOT NULL,
  `nome` varchar(50) COLLATE utf8_bin NOT NULL,
  `luogo` varchar(500) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `posizione`
--

INSERT INTO `posizione` (`codice`, `nome`, `luogo`) VALUES
(6, 'Birmania', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d590509.9504248623!2d97.15140501777083!3d23.40175590692934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3732439c36633b67%3A0xb1f9b0ba23077ceb!2sMan%20Sak%2C%20Myanmar%20(Birmania)!5e0!3m2!1sit!2sit!4v1611759623956!5m2!1sit!2sit'),
(7, 'Guayapo', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d509182.4495712956!2d-67.57807058351523!3d4.4133539839124944!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8de01ec59eb1eebd%3A0x15c3855ecc135fe5!2sGuayapo%2C%207101%2C%20Amazonas%2C%20Venezuela!5e0!3m2!1sit!2sit!4v1611759990707!5m2!1sit!2sit'),
(8, 'Pasacuc', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d195064.83446614674!2d-90.9264575579342!3d15.745932785911789!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x858b07f34101eb33%3A0x52c35a213d885672!2sPasacuc%2C%20Guatemala!5e0!3m2!1sit!2sit!4v1611760333089!5m2!1sit!2sit');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `email` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `nome` varchar(15) NOT NULL,
  `cognome` varchar(15) NOT NULL,
  `telefono` varchar(11) NOT NULL,
  `ruolo` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`email`, `password`, `nome`, `cognome`, `telefono`, `ruolo`) VALUES
('anna@gmail.com', '$6$rounds=5000$usesomesillystri$vkrbHgm58gM4NQEM29.2/hw8/Pza3BStTjXOHX3bjf3GuHcnpzQkxkkFlSX05y/1UzMHv6W1ATbuhWBKD5lyP/', 'Anna', 'Vitali', '1234567890', 'utente'),
('antionio.verdi@gmail.com', '$6$rounds=5000$usesomesillystri$CpvEXSIqDwQKq.Aio8HPCM7sTc.lF/7cytc4Qx30m.EySlZzUYrfvhtQC5FBQGYn8NOAEwqD29N7r9nSIYkxI0', 'Antonio', 'Verdi', '3333333333', 'utente'),
('bianchi.mauro@gmail.com', '$6$rounds=5000$usesomesillystri$M6t/eVaDN9gYseQH0rXlobIzxAphnFNWfBBOWCxQ1G51lYes1fclVBHH5rbVdGXgX4sG6nfHTdk8mOtcGde14/', 'Mauro', 'Bianchi', '32456789564', 'utente'),
('elena.yan@gmail.com', '$6$rounds=5000$usesomesillystri$Cw2Ns7lD7ONCEpLByMYuPzy68Sp2gzLpFzuCYf2dCiEs16lEyNsJ44ARqdE43dsBBQmp6MU63iigpTT6h6RP70', 'Elena', 'Yan', '3457689564', 'utente'),
('franco@libero.it', '$6$rounds=5000$usesomesillystri$JNFTwaj0c/Bk7JSHtYFm.GJMyIgYyDbRMQpEMddGCoMO33VGclQ25oKh7WNHQtlLc0bFRmGXKewTr9rVt2UcF1', 'Franco', 'Savoia', '3333333333', 'utente'),
('giulia@gmail.com', '$6$rounds=5000$usesomesillystri$BrW3XF5/72sWKsn1ajM85CILlrDL6wEBVFsSl9TW0CmInpHgNeNGBFKcsy5XZhWqR73NAomjq/1E7.GV3rsvV.', 'Giulia', 'Nardicchia', '1234567890', 'utente'),
('green.leaf@gmail.com', '$6$rounds=5000$usesomesillystri$CLkEvedkKewwlGyzeHofdaseYCMJe.VztV5EFdwzCm7U279W0b7Jiw/WCKydeDJuYzWpOenPVsRaQIIt06rKi/', 'Green', 'Leaf', '3450567834', 'venditore'),
('luigi.verdi@gmail.com', '$6$rounds=5000$usesomesillystri$f/DrLNesQAH8Kp2oNiuZWj1ssjIyytQqgS5K0eepDG44EWE8hgYn.O.x/aAxD/a3QzUVCcwqRaC8mnTVHiuSW0', 'Luigi', 'Verdi', '3458769867', 'utente'),
('maria@gmail.com', '$6$rounds=5000$usesomesillystri$ptCHykfMBr1Jb7uPEQEJTJjFQRPdXpRm1eJERcR.VUhhXqJGgJn3n1Zb/Oyii5x0KJHU.cF9d85Nw.dWx3pkj1', 'Maria', 'Mengozzi', '3343547854', 'utente'),
('mario.rossi@gmail.com', '$6$rounds=5000$usesomesillystri$qfg.QgA.Cbv1Va2S3vaEgEPvssRzxNRNcChJzZqpZ.vsKb1FquS6VbbMQmQp5eMKQRHd2h0fgocx6xA2oommb0', 'Mario', 'Rossi', '3450567834', 'venditore'),
('sara@gmail.com', '$6$rounds=5000$usesomesillystri$tpwCF5c6iTEkm3QbNazZRyYtLErYmfFFvjBoTIiM3OycBOQXCtY8mJ67a64hkXhWyvB2oGoVEmm03Qttfl89z.', 'Sara', 'Rossini', '1234567890', 'utente');

-- --------------------------------------------------------

--
-- Struttura della tabella `utentenoniscritto`
--

CREATE TABLE `utentenoniscritto` (
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utentenoniscritto`
--

INSERT INTO `utentenoniscritto` (`email`) VALUES
('francesca@gmail.com');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `albero`
--
ALTER TABLE `albero`
  ADD PRIMARY KEY (`nome`);

--
-- Indici per le tabelle `alberopiantato`
--
ALTER TABLE `alberopiantato`
  ADD PRIMARY KEY (`codice`),
  ADD KEY `FKtipologia` (`nome_albero`),
  ADD KEY `FKacquista` (`email_utente`),
  ADD KEY `FKriceve_regalo` (`email_regalo_noniscritto`),
  ADD KEY `FKiscritto` (`email_regalo_iscritto`),
  ADD KEY `FKappartenenza` (`posizione`);

--
-- Indici per le tabelle `datispedizione`
--
ALTER TABLE `datispedizione`
  ADD PRIMARY KEY (`codice`),
  ADD KEY `FKinserisce` (`email`);

--
-- Indici per le tabelle `dettagliocrescita`
--
ALTER TABLE `dettagliocrescita`
  ADD PRIMARY KEY (`codice`),
  ADD KEY `FKcrescita` (`albero_piantato`);

--
-- Indici per le tabelle `dettaglioordine`
--
ALTER TABLE `dettaglioordine`
  ADD PRIMARY KEY (`codice`),
  ADD KEY `FKriferito` (`numero_ordine`),
  ADD KEY `FKspedito` (`codice_spedizione`),
  ADD KEY `FKcompare` (`nome_albero`);

--
-- Indici per le tabelle `immagine`
--
ALTER TABLE `immagine`
  ADD PRIMARY KEY (`nome`),
  ADD KEY `FK_associaImmagine` (`dettaglio_crescita`);

--
-- Indici per le tabelle `immagine_albero`
--
ALTER TABLE `immagine_albero`
  ADD KEY `FKimg_albero` (`nome_immagine`),
  ADD KEY `FKalbero_img` (`nome_albero`);

--
-- Indici per le tabelle `modalitàdipagamento`
--
ALTER TABLE `modalitàdipagamento`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `FKpossiede` (`email`);

--
-- Indici per le tabelle `notifica`
--
ALTER TABLE `notifica`
  ADD PRIMARY KEY (`codice`);

--
-- Indici per le tabelle `notifica_utente`
--
ALTER TABLE `notifica_utente`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `FKinvia` (`codice`),
  ADD KEY `FKriceve` (`email`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `FKpaga` (`numero_carta`),
  ADD KEY `FKeffettua` (`email_utente`);

--
-- Indici per le tabelle `posizione`
--
ALTER TABLE `posizione`
  ADD PRIMARY KEY (`codice`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`email`);

--
-- Indici per le tabelle `utentenoniscritto`
--
ALTER TABLE `utentenoniscritto`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `alberopiantato`
--
ALTER TABLE `alberopiantato`
  MODIFY `codice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `datispedizione`
--
ALTER TABLE `datispedizione`
  MODIFY `codice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `dettagliocrescita`
--
ALTER TABLE `dettagliocrescita`
  MODIFY `codice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `dettaglioordine`
--
ALTER TABLE `dettaglioordine`
  MODIFY `codice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT per la tabella `notifica`
--
ALTER TABLE `notifica`
  MODIFY `codice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `notifica_utente`
--
ALTER TABLE `notifica_utente`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT per la tabella `posizione`
--
ALTER TABLE `posizione`
  MODIFY `codice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `alberopiantato`
--
ALTER TABLE `alberopiantato`
  ADD CONSTRAINT `FKacquista` FOREIGN KEY (`email_utente`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKappartenenza` FOREIGN KEY (`posizione`) REFERENCES `posizione` (`codice`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKiscritto` FOREIGN KEY (`email_regalo_iscritto`) REFERENCES `utente` (`email`),
  ADD CONSTRAINT `FKriceve_regalo` FOREIGN KEY (`email_regalo_noniscritto`) REFERENCES `utentenoniscritto` (`email`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKtipologia` FOREIGN KEY (`nome_albero`) REFERENCES `albero` (`nome`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `datispedizione`
--
ALTER TABLE `datispedizione`
  ADD CONSTRAINT `FKinserisce` FOREIGN KEY (`email`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `dettagliocrescita`
--
ALTER TABLE `dettagliocrescita`
  ADD CONSTRAINT `FKcrescita` FOREIGN KEY (`albero_piantato`) REFERENCES `alberopiantato` (`codice`);

--
-- Limiti per la tabella `dettaglioordine`
--
ALTER TABLE `dettaglioordine`
  ADD CONSTRAINT `FKcompare` FOREIGN KEY (`nome_albero`) REFERENCES `albero` (`nome`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKriferito` FOREIGN KEY (`numero_ordine`) REFERENCES `ordine` (`numero`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKspedito` FOREIGN KEY (`codice_spedizione`) REFERENCES `datispedizione` (`codice`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `immagine`
--
ALTER TABLE `immagine`
  ADD CONSTRAINT `FK_associaImmagine` FOREIGN KEY (`dettaglio_crescita`) REFERENCES `dettagliocrescita` (`codice`);

--
-- Limiti per la tabella `immagine_albero`
--
ALTER TABLE `immagine_albero`
  ADD CONSTRAINT `FKalbero_img` FOREIGN KEY (`nome_albero`) REFERENCES `albero` (`nome`),
  ADD CONSTRAINT `FKimg_albero` FOREIGN KEY (`nome_immagine`) REFERENCES `immagine` (`nome`);

--
-- Limiti per la tabella `modalitàdipagamento`
--
ALTER TABLE `modalitàdipagamento`
  ADD CONSTRAINT `FKpossiede` FOREIGN KEY (`email`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `notifica_utente`
--
ALTER TABLE `notifica_utente`
  ADD CONSTRAINT `FKinvia` FOREIGN KEY (`codice`) REFERENCES `notifica` (`codice`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKriceve` FOREIGN KEY (`email`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `FKeffettua` FOREIGN KEY (`email_utente`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FKpaga` FOREIGN KEY (`numero_carta`) REFERENCES `modalitàdipagamento` (`numero`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
